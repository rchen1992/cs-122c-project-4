#include "ixpage.h"

// ------------------- IX PAGE BASE CLASS ------------------- //
int IXPage::getKeyCount() {
	return key_count;
}

int IXPage::getFreeSpace() {
	return free_space;
}

PageType IXPage::getLevel() {
	return level;
}

PID IXPage::getPID() {
	return pid;
}

bool IXPage::hasSpace(AttrType type, const void* key) {
	switch(type) {
		case TypeVarChar: {
			std::string varchar = getVarcharKey(key);
			return getFreeSpace() >= (12 + varchar.length());
		}		
		case TypeInt: case TypeReal: {
			return getFreeSpace() >= 12;
		}		
	}
}



// ------------------- IX CHAR PAGE ------------------- //

IXCharPage::IXCharPage(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 12;
	level = Nonleaf;
	pid = page_num;
}

IXCharPage::~IXCharPage() {
}

RC IXCharPage::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);
	free_space = *(int*)c; 			// save free space
	c += sizeof(int);	
	level = *(PageType*)c; 	 		// save level's page type
	c += sizeof(PageType);

	for (int i = 0; i < key_count; i++) {
		int varchar_length = *(int*)c;						// get varchar length
		c += sizeof(int);
		std::string varchar(c, varchar_length);
		c += varchar_length;
		PID left_child = *(PID*)c;							// get left page number
		c += sizeof(PID);
		PID right_child = *(PID*)c;							// get right page number
		c += sizeof(PID);

		// Add key to list
		Key<std::string> k(varchar_length, varchar, left_child, right_child);
		keys.push_back(k);
	}


	return 0;
}

RC IXCharPage::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count;
	c += sizeof(key_count);
	*(int*)c = free_space;
	c += sizeof(free_space);	
	*(PageType*)c = level;
	c += sizeof(PageType);


	for (auto it = keys.begin(); it != keys.end(); it++) {
		*(int*)c = it->length;				// flush varchar length
		c += sizeof(int);
		memcpy(c, it->data.c_str(), it->length);	// flush varchar data
		c += it->length;
		*(PID*)c = it->left_child;			// flush left page number
		c += sizeof(PID);		
		*(PID*)c = it->right_child;			// flush right page number
		c += sizeof(PID);		
	}

	return 0;
}

RC IXCharPage::findNextPID(const void* key, PID& next_pid) {
	if (keys.empty())
		return error;

	std::string varchar = getVarcharKey(key);

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (varchar < it->data) {
			next_pid = it->left_child;
			return successful;
		}
	}

	// If it's not less than any key,
	// it must be greater than or equal to the last key
	auto it = keys.end();
	--it;
	if (varchar >= it->data) {
		next_pid = it->right_child;
		return successful;
	}
	else {
		return error;
	}

}

RC IXCharPage::insertKey(void* key) {
	Key<std::string>* k = static_cast<Key<std::string>*>(key);

	// Update metadata
	key_count++;
	free_space -= (12 + k->length);

	// If page is empty, simply insert key
	if (keys.size() == 0) {
		keys.push_back(*k);
		return successful;
	}

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (k->data < it->data) {
			// iterator is now at the right neighbor of new key
			// Set right neighbors left child to be new key's right child
			it->left_child = k->right_child; 
			// If there is a left neighbor
			// Set left neighbors right child to be new key's left child
			if (it != keys.begin()) {
				auto left = --it;
				left->right_child = k->left_child;
				++it;
			}

			keys.insert(it, *k);
			return successful;
		}
	}

	// Key must be greater than all other keys, 
	// and there must be at least one other existing key to the left
	keys.push_back(*k);
	auto left = keys.end();
	--left;
	--left; // seek to correct position of left neighbor
	if (k->data >= left->data) {
		left->right_child = k->left_child;
		return successful;
	}
	else {
		return error;
	}
}

RC IXCharPage::split(IXPage* new_child, IXPage* parent) {
	int mid = keys.size()/2;
	int i = 0;
    for (auto it = keys.begin(); it != keys.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<std::string> k(*it);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
            keys.erase(it++);

            // Update metadata
            key_count--;
            free_space += (k.length + 12);
        }
        else if (i > mid) {
        	// insert entry into new child
            new_child->insertKey((void*)&(*it)); 

            // Update metadata
            key_count--;
            free_space += (it->length + 12);

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;
}

RC IXCharPage::splitRoot(IXPage* new_left, IXPage* new_right) {
	int mid = keys.size()/2;
	int i = 0;


    for (auto it = keys.begin(); it != keys.end();) {
		// Change mid key's left and right and right child to the new pages
    	if (i == mid) {
    		it->left_child = new_left->getPID();
    		it->right_child = new_right->getPID();
    		++it;
    	}
        else if (i < mid) {
	    	// insert entry into left child
	        new_left->insertKey((void*)&(*it)); 

	        // Update metadata
            key_count--;
            free_space += (it->length + 12);

            // erase entry from this child while advancing iterator
	        keys.erase(it++);
        }
        else if (i > mid) {
        	new_right->insertKey((void*)&(*it));

            // Update metadata
            key_count--;
            free_space += (it->length + 12);

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;	
}


void IXCharPage::printDebug() {
	std::cout << "// --- Debug Information for Char Nonleaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = keys.begin(); it != keys.end(); it++) {
		std::cout << "\t\tKey value: " << it->data<< std::endl;
		std::cout << "\t\t\tKey length: " << it->length << std::endl;
		std::cout << "\t\t\tLeft child: " << it->left_child << std::endl;
		std::cout << "\t\t\tRight child: " << it->right_child << std::endl;
	}
}

std::list<Key<std::string>> IXCharPage::getKeys() {
	return this->keys;
}

std::list<Key<int>> IXIntPage::getKeys() {
	return this->keys;
}

std::list<Key<float>> IXFloatPage::getKeys() {
	return this->keys;
}


std::vector<PID> IXCharPage::getChildPIDs() {
    vector<PID> childPIDs;
    for (auto it = keys.begin(); it != keys.end(); it++) {
        PID pid = it->left_child;
        if (pid != -1) childPIDs.push_back(pid);

        if (it == --keys.end()) {
            pid = it->right_child;
            if (pid != -1) childPIDs.push_back(pid);
        }
    }
    return childPIDs;
}

std::vector<PID> IXIntPage::getChildPIDs() {
	vector<PID> childPIDs;
    for (auto it = keys.begin(); it != keys.end(); it++) {
        PID pid = it->left_child;
        if (pid != -1) childPIDs.push_back(pid);

        if (it == --keys.end()) {
            pid = it->right_child;
            if (pid != -1) childPIDs.push_back(pid);
        }
    }
	return childPIDs;
}

std::vector<PID> IXFloatPage::getChildPIDs() {
    vector<PID> childPIDs;
    for (auto it = keys.begin(); it != keys.end(); it++) {
        PID pid = it->left_child;
        if (pid != -1) childPIDs.push_back(pid);

        if (it == --keys.end()) {
            pid = it->right_child;
            if (pid != -1) childPIDs.push_back(pid);
        }
    }
    return childPIDs;
}

void IXCharPage::printKeys() {
    for (auto it = keys.begin(); it != keys.end(); it++) {
        cout << "\"" << it->data << "\"";
        if (it != --keys.end()) cout << ", ";
    }
}

void IXIntPage::printKeys() {
    for (auto it = keys.begin(); it != keys.end(); it++) {
        cout << "\"" << it->data << "\"";
        if (it != --keys.end()) cout << ", ";
    }
}

void IXFloatPage::printKeys() {
    for (auto it = keys.begin(); it != keys.end(); it++) {
        cout << "\"" << it->data << "\"";
        if (it != --keys.end()) cout << ", ";
    }
}

// ------------------- IX INT PAGE ------------------- //
IXIntPage::IXIntPage(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 12;
	level = Nonleaf;
	pid = page_num;
}

IXIntPage::~IXIntPage() {
}

RC IXIntPage::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);	
	free_space = *(int*)c;			// save free space
	c += sizeof(int);
	level = *(PageType*)c; 			// save level's page type
	c += sizeof(PageType);

	for (int i = 0; i < key_count; i++) {
		int value = *(int*)c;
		c += sizeof(int);
		PID left_child = *(PID*)c;							// get left page number
		c += sizeof(PID);		
		PID right_child = *(PID*)c;							// get right page number
		c += sizeof(PID);		

		// Add key to list
		Key<int> k(4, value, left_child, right_child);
		keys.push_back(k);
	}

	return 0;
}

RC IXIntPage::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count; 		// flush key count
	c += sizeof(key_count);
	*(int*)c = free_space;		// flush free space
	c += sizeof(free_space);
	*(PageType*)c = level; // flush next level page type
	c += sizeof(PageType);

	for (auto it = keys.begin(); it != keys.end(); it++) {
		*(int*)c = it->data;
		c += sizeof(int);
		*(PID*)c = it->left_child;			// flush left page number
		c += sizeof(PID);		
		*(PID*)c = it->right_child;			// flush right page number
		c += sizeof(PID);		
	}

	return 0;
}

RC IXIntPage::findNextPID(const void* key, PID& next_pid) {
	if (keys.empty())
		return error;

	int key_val = *(int*)key;

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (key_val < it->data) {
			next_pid = it->left_child;

			return successful;
		}
	}

	// If it's not less than or equal to any key,
	// it must be greater than the last key
	auto it = keys.end();
	--it;
	if (key_val >= it->data) {
		next_pid = it->right_child;
		return successful;
	}
	else
		return error;
}

RC IXIntPage::insertKey(void* key) {
	Key<int>* k = (Key<int>*)key;

	// Update metadata
	key_count++;
	free_space -= 12;

	// If page is empty, simply insert key
	if (keys.size() == 0) {
		keys.push_back(*k);
		return successful;
	}

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (k->data < it->data) {
			// iterator is now at the right neighbor of new key
			// Set right neighbors left child to be new key's right child
			it->left_child = k->right_child; 
			// If there is a left neighbor
			// Set left neighbors right child to be new key's left child
			if (it != keys.begin()) {
				auto left = --it; 
				left->right_child = k->left_child;
				++it;
			}

			keys.insert(it, *k);
			return successful;
		}
	}

	// Key must be greater than all other keys, 
	// and there must be at least one other existing key to the left
	keys.push_back(*k);
	auto left = keys.end();
	--left;
	--left; // seek to correct position of left neighbor
	if (k->data >= left->data) {
		left->right_child = k->left_child;
		return successful;
	}
	else {
		return error;
	}
}

RC IXIntPage::split(IXPage* new_child, IXPage* parent) {
	int mid = keys.size()/2;
	int i = 0;
    for (auto it = keys.begin(); it != keys.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<int> k(*it);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
            keys.erase(it++);

            // Update metadata
            key_count--;
            free_space += 12;
        }
        else if (i > mid) {
        	// insert entry into new child
            new_child->insertKey((void*)&(*it)); 

            // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;
}

RC IXIntPage::splitRoot(IXPage* new_left, IXPage* new_right) {
	int mid = keys.size()/2;
	int i = 0;


    for (auto it = keys.begin(); it != keys.end();) {
		// Change mid key's left and right and right child to the new pages
    	if (i == mid) {
    		it->left_child = new_left->getPID();
    		it->right_child = new_right->getPID();
    		++it;
    	}
        else if (i < mid) {
	    	// insert entry into left child
	        new_left->insertKey((void*)&(*it)); 

	        // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
	        keys.erase(it++);
        }
        else if (i > mid) {
        	new_right->insertKey((void*)&(*it));

            // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;	
}

void IXIntPage::printDebug() {
	std::cout << "// --- Debug Information for Int Nonleaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = keys.begin(); it != keys.end(); it++) {
		std::cout << "\t\tKey value: " << it->data<< std::endl;
		std::cout << "\t\t\tLeft child: " << it->left_child << std::endl;
		std::cout << "\t\t\tRight child: " << it->right_child << std::endl;
	}
}



// ------------------- IX FLOAT PAGE ------------------- //
IXFloatPage::IXFloatPage(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 12;
	level = Nonleaf;
	pid = page_num;
}

IXFloatPage::~IXFloatPage() {
}

RC IXFloatPage::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);	
	free_space = *(int*)c;			// save free space
	c += sizeof(int);
	level = *(PageType*)c; 			// save level's page type
	c += sizeof(PageType);

	for (float i = 0; i < key_count; i++) {
		float value = *(float*)c;
		c += sizeof(float);
		PID left_child = *(PID*)c;							// get left page number
		c += sizeof(PID);		
		PID right_child = *(PID*)c;							// get right page number
		c += sizeof(PID);		

		// Add key to list
		Key<float> k(4, value, left_child, right_child);
		keys.push_back(k);
	}

	return 0;
}

RC IXFloatPage::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count; 		// flush key count
	c += sizeof(key_count);
	*(int*)c = free_space;		// flush free space
	c += sizeof(free_space);
	*(PageType*)c = level; // flush next level page type
	c += sizeof(PageType);

	for (auto it = keys.begin(); it != keys.end(); it++) {
		*(float*)c = it->data;
		c += sizeof(float);
		*(PID*)c = it->left_child;			// flush left page number
		c += sizeof(PID);		
		*(PID*)c = it->right_child;			// flush right page number
		c += sizeof(PID);		
	}

	return 0;
}

RC IXFloatPage::findNextPID(const void* key, PID& next_pid) {
	if (keys.empty())
		return error;

	float key_val = *(float*)key;

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (key_val < it->data) {
			next_pid = it->left_child;

			return successful;
		}
	}

	// If it's not less than or equal to any key,
	// it must be greater than the last key
	auto it = keys.end();
	--it;
	if (key_val >= it->data) {
		next_pid = it->right_child;
		return successful;
	}
	else
		return error;
}

RC IXFloatPage::insertKey(void* key) {
	Key<float>* k = (Key<float>*)key;

	// Update metadata
	key_count++;
	free_space -= 12;

	// If page is empty, simply insert key
	if (keys.size() == 0) {
		keys.push_back(*k);
		return successful;
	}

	for (auto it = keys.begin(); it != keys.end(); it++) {
		if (k->data < it->data) {
			// iterator is now at the right neighbor of new key
			// Set right neighbors left child to be new key's right child
			it->left_child = k->right_child; 
			// If there is a left neighbor
			// Set left neighbors right child to be new key's left child
			if (it != keys.begin()) {
				auto left = --it; 
				left->right_child = k->left_child;
				++it;
			}

			keys.insert(it, *k);
			return successful;
		}
	}

	// Key must be greater than or equal to all other keys, 
	// and there must be at least one other existing key to the left
	keys.push_back(*k);
	auto left = keys.end();
	--left;
	--left; // seek to correct position of left neighbor
	if (k->data >= left->data) {
		left->right_child = k->left_child;
		return successful;
	}
	else {
		return error;
	}
}

RC IXFloatPage::split(IXPage* new_child, IXPage* parent) {
	int mid = keys.size()/2;
	int i = 0;
    for (auto it = keys.begin(); it != keys.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<float> k(*it);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
            keys.erase(it++);

            // Update metadata
            key_count--;
            free_space += 12;
        }
        else if (i > mid) {
        	// insert entry into new child
            new_child->insertKey((void*)&(*it)); 

            // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;
}

RC IXFloatPage::splitRoot(IXPage* new_left, IXPage* new_right) {
	int mid = keys.size()/2;
	int i = 0;

	// Skip mid key because we want to leave it in this page
    for (auto it = keys.begin(); it != keys.end();) {
		// Change mid key's left and right and right child to the new pages
    	if (i == mid) {
    		it->left_child = new_left->getPID();
    		it->right_child = new_right->getPID();
    		++it;
    	}
        else if (i < mid) {
	    	// insert entry into left child
	        new_left->insertKey((void*)&(*it)); 

	        // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
	        keys.erase(it++);
        }
        else if (i > mid) {
        	new_right->insertKey((void*)&(*it));
        	
            // Update metadata
            key_count--;
            free_space += 12;

            // erase entry from this child while advancing iterator
            keys.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;	
}

void IXFloatPage::printDebug() {
	std::cout << "// --- Debug Information for Float Nonleaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = keys.begin(); it != keys.end(); it++) {
		std::cout << "\t\tKey value: " << it->data<< std::endl;
		std::cout << "\t\t\tLeft child: " << it->left_child << std::endl;
		std::cout << "\t\t\tRight child: " << it->right_child << std::endl;
	}
}


