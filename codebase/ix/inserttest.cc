#include <iostream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "ix.h"
#include "ixtest_util.h"

IndexManager *indexManager;

int testCase_1(const string &indexFileName)
{
    // Functions tested
    // insertEntry() with no splits
    cerr << endl << "****In simple insertEntry() with no splits test case****" << endl;

    // create index file
    assertCreateIndexFile(success, indexManager, indexFileName);

    // open index file
    IXFileHandle fileHandle;
    assertOpenIndexFile(success, indexManager, indexFileName, fileHandle);

    Attribute a = createAttribute(TypeVarChar);
    RID rid = createRID(1,0);
    int varchar_length = 11;
    char* key = new char[varchar_length+4];
    memset(key, 0, varchar_length+4);
    *(int*)key = varchar_length;
    char* c = key;
    c += 4;
    char* s = "helloworld";
    memcpy(c, s, varchar_length);
    RC rc = indexManager->insertEntry(fileHandle, a, key, rid);    
    rid = createRID(3,2);
    s = "appleapple";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);      
    s = "appleapple";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);        
    s = "bobbybobby";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);      
    s = "bobbybobby";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);    
    s = "bobbybobby";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);    
    s = "bobbybobby";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);    
    s = "bobbybobby";
    memset(c, 0, varchar_length);
    memcpy(c, s, varchar_length);
    rc = indexManager->insertEntry(fileHandle, a, key, rid);    

    delete key;


    indexManager->printBtree(fileHandle, a);
    // close index file
    assertCloseIndexFile(success, indexManager, fileHandle);

    // destroy index file
    assertDestroyIndexFile(success, indexManager, indexFileName);

    return success;
}

int main()
{
    //Global Initializations
    indexManager = IndexManager::instance();

    const string indexFileName = "age_idx";

    testCase_1(indexFileName);
    cerr << "IX_Test Case 1 passed" << endl;
}

