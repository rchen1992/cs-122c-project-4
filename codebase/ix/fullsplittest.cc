#include <iostream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "ix.h"
#include "ixtest_util.h"
#include "../rbf/types.h"

IndexManager *indexManager;

int testCase_1(const string &indexFileName)
{
    // Functions tested
    // insertEntry() with no splits
    cerr << endl << "****In simple insertEntry() with splits test case****" << endl;

    // create index file
    assertCreateIndexFile(success, indexManager, indexFileName);

    // open index file
    IXFileHandle fileHandle;
    assertOpenIndexFile(success, indexManager, indexFileName, fileHandle);


    Attribute a = createAttribute(TypeInt);
    RID rid = createRID(1,0);
    int* key = new int;
    for (int i = 0; i < 30; i++) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }
    for (int i = 0; i < 30; i++) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }
    // for (float i = 0; i < 18; i++) {
    //     *key = i;
    //     indexManager->insertEntry(fileHandle, a, key, rid);
    // }
    // *key = 1;
    // indexManager->insertEntry(fileHandle, a, key, rid);


    delete key;

    indexManager->printBtree(fileHandle, a);
    cout << endl << endl;

    int k = 40;
    RID dRid;
    dRid.pageNum = 1;
    dRid.slotNum = 0;


    // Test deleteEntry
    indexManager->deleteEntry(fileHandle, a, &k, dRid);
    indexManager->deleteEntry(fileHandle, a, &k, dRid);

    // close index file
    assertCloseIndexFile(success, indexManager, fileHandle);

    // destroy index file
    assertDestroyIndexFile(success, indexManager, indexFileName);

    return success;
}

int main()
{
    //Global Initializations
    indexManager = IndexManager::instance();

    const string indexFileName = "age_idx";

    testCase_1(indexFileName);
    cerr << "IX_Test Case 1 passed" << endl;
}

