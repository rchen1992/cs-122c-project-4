#include "ixleaf.h"
#include "../rbf/types.h"


// ------------------- IX LEAF BASE CLASS ------------------- //
int IXLeaf::getKeyCount() {
	return key_count;
}
int IXLeaf::getFreeSpace() {
	return free_space;
}
PageType IXLeaf::getLevel() {
	return level;
}
PID IXLeaf::getNextPID() {
	return next_pid;
}
PID IXLeaf::getPID() {
	return pid;
}
void IXLeaf::setNextPID(PID new_pid) {
	next_pid = new_pid;
}

// ------------------- IX CHAR LEAF ------------------- //

IXCharLeaf::IXCharLeaf(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 16;
	level = Leaf;
	next_pid = -1;
	pid = page_num;
}

IXCharLeaf::~IXCharLeaf() {
}

RC IXCharLeaf::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);
	free_space = *(int*)c; 			// save free space
	c += sizeof(int);		
	level = *(PageType*)c; 			// save level
	c += sizeof(PageType);	
	next_pid = *(PID*)c; 			// save next leaf's page number
	c += sizeof(PID);		

	for (int i = 0; i < key_count; i++) {
		int varchar_length = *(int*)c;						// get varchar length
		c += sizeof(int);
		std::string varchar(c, varchar_length);
		c += varchar_length;

		// Create key
		Key<std::string> k(varchar_length, varchar, -1, -1);

		RIDlist rid_list;
		int rid_count = *(int*)c; 	// get length of RID list
		c += sizeof(int);

		// Fill in RID list for key
		for (int j = 0; j < rid_count; j++) {
			RID rid;
			rid.pageNum = *(unsigned*)c; // get RID page num
			c += sizeof(unsigned);
			rid.slotNum = *(unsigned*)c; // get RID slot num
			c += sizeof(unsigned);

			rid_list.push_back(rid);
		}

		// Enter key:RIDlist pair into ordered map
		entries[k] = rid_list;
	}


	return successful;
}

RC IXCharLeaf::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count; 		// flush key count
	c += sizeof(key_count);
	*(int*)c = free_space;  	// flush free space
	c += sizeof(free_space);		
	*(PageType*)c = level;  	// flush level
	c += sizeof(PageType);	
	*(PID*)c = next_pid;  	    // flush next leaf's pid
	c += sizeof(PID);		

	for (auto it = entries.begin(); it != entries.end(); it++) {
		*(int*)c = it->first.length;					// flush varchar length
		c += sizeof(int);
		memcpy(c, it->first.data.c_str(), it->first.length);	// flush varchar data
		c += it->first.length;

		*(int*)c = it->second.size(); 					// flush RIDlist length
		c += sizeof(int);

		// Flush RIDlist
		for (int i = 0; i < it->second.size(); i++) {
			*(unsigned*)c = it->second[i].pageNum; 		// flush RID page num
			c += sizeof(unsigned);
			*(unsigned*)c = it->second[i].slotNum; 		// flush RID slot num
			c += sizeof(unsigned);
		}
	}

	return successful;
}

RC IXCharLeaf::insertEntry(const void* key, const RID& rid) {
	std::string varchar = getVarcharKey(key); // read in varchar

	Key<std::string> k(varchar.length(), varchar, -1, -1); 	// create key
	if (entries.find(k) != entries.end()) {  	// if key already exists, add to RID list
		entries[k].push_back(rid);
		free_space -= sizeof(RID);
	}
	else { // otherwise, create RID list and add key
		RIDlist rid_list;
		rid_list.push_back(rid);
		entries[k] = rid_list;
		free_space -= (8 + varchar.length() + sizeof(RID));
		key_count++;
	}

	return successful;
}

bool IXCharLeaf::hasSpace(AttrType type, const void* key) {
	std::string varchar = getVarcharKey(key);
	Key<std::string> k(varchar.length(), varchar, -1, -1);

	// If key is already in leaf
	if (entries.find(k) != entries.end()) {
		return getFreeSpace() >= sizeof(RID);
	}
	else {
		return (getFreeSpace() >= (varchar.length() + 8 + sizeof(RID)));
	}
}

RC IXCharLeaf::insertFromAnotherLeaf(void* key, RIDlist& rid_list) {
	Key<std::string>* k = static_cast<Key<std::string>*>(key);	
	entries[*k] = rid_list;

	// Update metadata
	free_space -= (sizeof(RID) * rid_list.size() + k->length + 8);
	key_count++;

	return successful;
}

int IXCharLeaf::getMid() {
	return entries.size()/2;
}

RC IXCharLeaf::split(IXLeaf* new_child, IXPage* parent) {
    // Set new child's next PID to point to this child's next PID
    new_child->setNextPID(getNextPID());
    // Now point this child's next to the new child
    setNextPID(new_child->getPID());

	int mid = getMid();
	int i = 0;
    for (auto it = entries.begin(); it != entries.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<std::string> k(it->first);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
        }
        if (i >= mid) {
        	// insert entry into new child
            new_child->insertFromAnotherLeaf((void*)&(it->first), it->second); 

            // Update metadata
            key_count--;
            free_space += (sizeof(RID) * it->second.size() + it->first.length + 8);

            // erase entry from this child while advancing iterator
            entries.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

    return successful;
}


RC IXCharLeaf::prepForScan(const void* key, bool keyInclusive) {
	std::string varchar = getVarcharKey(key);

	// Delete all keys that are less than low key from map
	for (auto it = entries.begin(); it != entries.end();) {
		// If key inclusive, keep keys that are equal to low key
		if (keyInclusive) {
			if (varchar > it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8 + varchar.length());
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
		else {
			if (varchar >= it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8 + varchar.length());
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
	}

	return successful;
}

RC IXCharLeaf::popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey) {
	// If no upper limit, simply pop next entry
	if (highKey == 0) 
		return pop(rid, returnKey);

	std::string varchar = getVarcharKey(highKey);
	auto it = entries.begin();

	if (highKeyInclusive) {
		if (varchar >= it->first.data) {
			return pop(rid, returnKey);
		}
		else 
			return error;
	}
	else {
		if (varchar > it->first.data) 
			return pop(rid, returnKey);
		else 
			return error;
	}
}

RC IXCharLeaf::pop(RID& rid, void* returnKey) {
	auto it = entries.begin();

	// Save RID information
	rid.pageNum = it->second[0].pageNum;
	rid.slotNum = it->second[0].slotNum;

	// Save key information
	char* c = (char*)returnKey;
	c += 4;
	*(int*)returnKey = it->first.length;
	memcpy(c, it->first.data.c_str(), it->first.length);

	// If there is only one RID, delete the entire entry from map
	// Otherwise, only delete the RID
	if (it->second.size() == 1) {
		entries.erase(it);
		free_space -= (sizeof(RID) + 8 + it->first.length);
		key_count--;
	}
	else {
		it->second.erase(it->second.begin());
		free_space -= sizeof(RID);
	}
	return successful;	
}


void IXCharLeaf::printDebug() {
	std::cout << "// --- Debug Information for Char Leaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tNext PID: " << next_pid << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = entries.begin(); it != entries.end(); it++) {
		std::cout << "\t\tKey value: " << it->first.data << std::endl;
		std::cout << "\t\tRID List: { ";
		for (int i = 0; i < it->second.size(); i++) {
			std::cout << "(" << it->second[i].pageNum << "," << it->second[i].slotNum << ") ";
		}
		std::cout << "}" << std::endl;
	}
}



// ------------------- IX INT LEAF ------------------- //

IXIntLeaf::IXIntLeaf(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 16;
	level = Leaf;
	next_pid = -1;
	pid = page_num;
}

IXIntLeaf::~IXIntLeaf() {
}

RC IXIntLeaf::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);
	free_space = *(int*)c; 			// save free space
	c += sizeof(int);		
	level = *(PageType*)c; 			// save level
	c += sizeof(PageType);	
	next_pid = *(PID*)c; 			// save next leaf's page number
	c += sizeof(PID);		

	for (int i = 0; i < key_count; i++) {
		int data = *(int*)c;
		c += sizeof(int);

		// Create key
		Key<int> k(4, data, -1, -1);

		RIDlist rid_list;
		int rid_count = *(int*)c; 	// get length of RID list
		c += sizeof(int);

		// Fill in RID list for key
		for (int j = 0; j < rid_count; j++) {
			RID rid;
			rid.pageNum = *(unsigned*)c; // get RID page num
			c += sizeof(unsigned);
			rid.slotNum = *(unsigned*)c; // get RID slot num
			c += sizeof(unsigned);

			rid_list.push_back(rid);
		}

		// Enter key:RIDlist pair into ordered map
		entries[k] = rid_list;
	}


	return successful;
}

RC IXIntLeaf::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count; 		// flush key count
	c += sizeof(key_count);
	*(int*)c = free_space;  	// flush free space
	c += sizeof(free_space);		
	*(PageType*)c = level;  	// flush level
	c += sizeof(PageType);	
	*(PID*)c = next_pid;  	    // flush next leaf's pid
	c += sizeof(PID);		

	for (auto it = entries.begin(); it != entries.end(); it++) {
		*(int*)c = it->first.data;						// flush key value
		c += sizeof(int);

		*(int*)c = it->second.size(); 					// flush RIDlist length
		c += sizeof(int);

		// Flush RIDlist
		for (int i = 0; i < it->second.size(); i++) {
			*(unsigned*)c = it->second[i].pageNum; 		// flush RID page num
			c += sizeof(unsigned);
			*(unsigned*)c = it->second[i].slotNum; 		// flush RID slot num
			c += sizeof(unsigned);
		}
	}

	return successful;
}

RC IXIntLeaf::insertEntry(const void* key, const RID& rid) {
	int data = *(int*)key;	// get value of int key
	Key<int> k(4, data, -1, -1); 	// create key

	if (entries.find(k) != entries.end()) {  	// if key already exists, add to RID list
		entries[k].push_back(rid);
		free_space -= sizeof(RID);
	}
	else { // otherwise, create RID list and add key
		RIDlist rid_list;
		rid_list.push_back(rid);
		entries[k] = rid_list;
		free_space -= (8 + sizeof(RID));
		key_count++;
	}


	return successful;
}

bool IXIntLeaf::hasSpace(AttrType type, const void* key) {
	int key_val = *(int*)key;
	Key<int> k(4, key_val, -1, -1);

	// If key is already in leaf
	if (entries.find(k) != entries.end()) 
		return getFreeSpace() >= sizeof(RID);
	else 
		return (getFreeSpace() >= (8 + sizeof(RID)));
}

RC IXIntLeaf::insertFromAnotherLeaf(void* key, RIDlist& rid_list) {
	Key<int>* k = (Key<int>*)key;	
	entries[*k] = rid_list;

	// Update metadata
	free_space -= (sizeof(RID) * rid_list.size() + 8);
	key_count++;
	
	return successful;
}

int IXIntLeaf::getMid() {
	return entries.size()/2;
}

RC IXIntLeaf::split(IXLeaf* new_child, IXPage* parent) {
    // Set new child's next PID to point to this child's next PID
    new_child->setNextPID(getNextPID());
    // Now point this child's next to the new child
    setNextPID(new_child->getPID());

	int mid = getMid();
	int i = 0;
    for (auto it = entries.begin(); it != entries.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<int> k(it->first);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
        }
        if (i >= mid) {
        	// insert entry into new child
            new_child->insertFromAnotherLeaf((void*)&(it->first), it->second); 

            // Update metadata
            key_count--;
            free_space += (sizeof(RID) * it->second.size() + 8);

            // erase entry from this child while advancing iterator
            entries.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;
}

// Assumes low key passed in
RC IXIntLeaf::prepForScan(const void* key, bool keyInclusive) {
	int key_val = *(int*)key;

	// Delete all keys that are less than low key from map
	for (auto it = entries.begin(); it != entries.end();) {
		// If key inclusive, keep keys that are equal to low key
		if (keyInclusive) {
			if (key_val > it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8);
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
		else {
			if (key_val >= it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8);
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
	}

	return successful;
}

RC IXIntLeaf::popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey) {
	// If no upper limit, simply pop next entry
	if (highKey == 0) 
		return pop(rid, returnKey);

	int key_val = *(int*)highKey;
	auto it = entries.begin();

	if (highKeyInclusive) {
		if (key_val >= it->first.data) {
			return pop(rid, returnKey);
		}
		else 
			return error;
	}
	else {
		if (key_val > it->first.data) 
			return pop(rid, returnKey);
		else 
			return error;
	}
}

RC IXIntLeaf::pop(RID& rid, void* returnKey) {
	auto it = entries.begin();

	// Save RID information
	rid.pageNum = it->second[0].pageNum;
	rid.slotNum = it->second[0].slotNum;

	// Save key information
	*(int*)returnKey = it->first.data;

	// If there is only one RID, delete the entire entry from map
	// Otherwise, only delete the RID
	if (it->second.size() == 1) {
		entries.erase(it);
		free_space -= (sizeof(RID) + 8);
		key_count--;
	}
	else {
		it->second.erase(it->second.begin());
		free_space -= sizeof(RID);
	}
	return successful;	
}


void IXIntLeaf::printDebug() {
	std::cout << "// --- Debug Information for Int Leaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tNext PID: " << next_pid << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = entries.begin(); it != entries.end(); it++) {
		std::cout << "\t\tKey value: " << it->first.data << std::endl;
		std::cout << "\t\tRID List: { ";
		for (int i = 0; i < it->second.size(); i++) {
			std::cout << "(" << it->second[i].pageNum << "," << it->second[i].slotNum << ") ";
		}
		std::cout << "}" << std::endl;
	}
}



// ------------------- IX FLOAT LEAF ------------------- //

IXFloatLeaf::IXFloatLeaf(PID page_num) {
	key_count = 0;
	free_space = PAGE_SIZE - 16;
	level = Leaf;
	next_pid = -1;
	pid = page_num;
}

IXFloatLeaf::~IXFloatLeaf() {
}

RC IXFloatLeaf::loadPage(void* data) {
	char* c = (char*) data;
	key_count = *(int*)c;			// save key count
	c += sizeof(int);
	free_space = *(int*)c; 			// save free space
	c += sizeof(int);		
	level = *(PageType*)c; 			// save level
	c += sizeof(PageType);	
	next_pid = *(PID*)c; 			// save next leaf's page number
	c += sizeof(PID);		

	for (int i = 0; i < key_count; i++) {
		float data = *(float*)c;
		c += sizeof(float);

		// Create key
		Key<float> k(4, data, -1, -1);

		RIDlist rid_list;
		int rid_count = *(int*)c; 	// get length of RID list
		c += sizeof(int);

		// Fill in RID list for key
		for (int j = 0; j < rid_count; j++) {
			RID rid;
			rid.pageNum = *(unsigned*)c; // get RID page num
			c += sizeof(unsigned);
			rid.slotNum = *(unsigned*)c; // get RID slot num
			c += sizeof(unsigned);

			rid_list.push_back(rid);
		}

		// Enter key:RIDlist pair into ordered map
		entries[k] = rid_list;
	}


	return successful;
}

RC IXFloatLeaf::flush(void* data) {
	char* c = (char*) data;
	*(int*)c = key_count; 		// flush key count
	c += sizeof(key_count);
	*(int*)c = free_space;  	// flush free space
	c += sizeof(free_space);		
	*(PageType*)c = level;  	    // flush level
	c += sizeof(PageType);	
	*(PID*)c = next_pid;  	    // flush next leaf's pid
	c += sizeof(PID);		

	for (auto it = entries.begin(); it != entries.end(); it++) {
		*(float*)c = it->first.data;					// flush key value
		c += sizeof(float);

		*(int*)c = it->second.size(); 					// flush RIDlist length
		c += sizeof(int);

		// Flush RIDlist
		for (int i = 0; i < it->second.size(); i++) {
			*(unsigned*)c = it->second[i].pageNum; 		// flush RID page num
			c += sizeof(unsigned);
			*(unsigned*)c = it->second[i].slotNum; 		// flush RID slot num
			c += sizeof(unsigned);
		}
	}

	return successful;
}

RC IXFloatLeaf::insertEntry(const void* key, const RID& rid) {
	float data = *(float*)key;	// get value of float key
	Key<float> k(4, data, -1, -1); 	// create key

	if (entries.find(k) != entries.end()) {  	// if key already exists, add to RID list
		entries[k].push_back(rid);
		free_space -= sizeof(RID);
	}
	else { // otherwise, create RID list and add key
		RIDlist rid_list;
		rid_list.push_back(rid);
		entries[k] = rid_list;
		free_space -= (8 + sizeof(RID));
		key_count++;
	}


	return successful;
}

bool IXFloatLeaf::hasSpace(AttrType type, const void* key) {
	float key_val = *(float*)key;
	Key<float> k(4, key_val, -1, -1);

	// If key is already in leaf
	if (entries.find(k) != entries.end()) 
		return getFreeSpace() >= sizeof(RID);
	else 
		return (getFreeSpace() >= (8 + sizeof(RID)));
}

RC IXFloatLeaf::insertFromAnotherLeaf(void* key, RIDlist& rid_list) {
	Key<float>* k = (Key<float>*)key;	
	entries[*k] = rid_list;

	// Update metadata
	free_space -= (sizeof(RID) * rid_list.size() + 8);
	key_count++;
	
	return successful;
}

int IXFloatLeaf::getMid() {
	return entries.size()/2;
}

RC IXFloatLeaf::split(IXLeaf* new_child, IXPage* parent) {
    // Set new child's next PID to point to this child's next PID
    new_child->setNextPID(getNextPID());
    // Now point this child's next to the new child
    setNextPID(new_child->getPID());

	int mid = getMid();
	int i = 0;
    for (auto it = entries.begin(); it != entries.end();) {
        // Create key with mid
        // left child points to this child, right child points to new child
        // Push key to parent
        if (i == mid) {
        	Key<float> k(it->first);
        	k.left_child = getPID();
        	k.right_child = new_child->getPID();
            parent->insertKey((void*)&k);
        }
        if (i >= mid) {
        	// insert entry into new child
            new_child->insertFromAnotherLeaf((void*)&(it->first), it->second); 

            // Update metadata
            key_count--;
            free_space += (sizeof(RID) * it->second.size() + 8);

            // erase entry from this child while advancing iterator
            entries.erase(it++); 
        }
        else {
	        ++it;
        }
        i++;
    }

	return successful;
}

// Assumes low key passed in
RC IXFloatLeaf::prepForScan(const void* key, bool keyInclusive) {
	float key_val = *(float*)key;

	// Delete all keys that are less than low key from map
	for (auto it = entries.begin(); it != entries.end();) {
		// If key inclusive, keep keys that are equal to low key
		if (keyInclusive) {
			if (key_val > it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8);
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
		else {
			if (key_val >= it->first.data) {
				key_count--;
				free_space -= (sizeof(RID) * it->second.size() + 8);
				entries.erase(it++);
			}
			else {
				return successful;
			}
		}
	}

	return successful;
}

RC IXFloatLeaf::popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey) {
	// If no upper limit, simply pop next entry
	if (highKey == 0) 
		return pop(rid, returnKey);

	float key_val = *(float*)highKey;
	auto it = entries.begin();

	if (highKeyInclusive) {
		if (key_val >= it->first.data) 
			return pop(rid, returnKey);
		else 
			return error;
	}
	else {
		if (key_val > it->first.data) 
			return pop(rid, returnKey);
		else 
			return error;
	}
}

RC IXFloatLeaf::pop(RID& rid, void* returnKey) {
	auto it = entries.begin();

	// Save RID information
	rid.pageNum = it->second[0].pageNum;
	rid.slotNum = it->second[0].slotNum;

	// Save key information
	*(float*)returnKey = it->first.data;

	// If there is only one RID, delete the entire entry from map
	// Otherwise, only delete the RID
	if (it->second.size() == 1) {
		entries.erase(it);
		free_space -= (sizeof(RID) + 8);
		key_count--;
	}
	else {
		it->second.erase(it->second.begin());
		free_space -= sizeof(RID);
	}
	return successful;	
}


void IXFloatLeaf::printDebug() {
	std::cout << "// --- Debug Information for Float Leaf Page --- //" << std::endl;
	std::cout << "\tPID: " << pid << std::endl;
	std::cout << "\tKey count: " << key_count << std::endl;
	std::cout << "\tFree Space: " << free_space << std::endl;
	std::cout << "\tNext PID: " << next_pid << std::endl;
	std::cout << "\tKey Info: " << std::endl;
	for (auto it = entries.begin(); it != entries.end(); it++) {
		std::cout << "\t\tKey value: " << it->first.data << std::endl;
		std::cout << "\t\tRID List: { ";
		for (int i = 0; i < it->second.size(); i++) {
			std::cout << "(" << it->second[i].pageNum << "," << it->second[i].slotNum << ") ";
		}
		std::cout << "}" << std::endl;
	}
}



void IXCharLeaf::printKeys() {
    for (auto it = entries.begin(); it != entries.end(); it++) {
        std::cout << "\"" << it->first.data << ":";
        RIDlist rl = it->second;
        for (unsigned i = 0; i < rl.size(); i++) {
            RID rid = rl[i];
            std::cout << "(" << rid.pageNum << "," << rid.slotNum << ")";
            if (i < rl.size() - 1) std::cout << ",";
        }
        std::cout << "\"";
        if (it != (--entries.end())) cout << ",";
    }
}

void IXIntLeaf::printKeys() {
    for (auto it = entries.begin(); it != entries.end(); it++) {
        std::cout << "\"" << it->first.data << ":";
        RIDlist rl = it->second;
        for (unsigned i = 0; i < rl.size(); i++) {
            RID rid = rl[i];
            std::cout << "(" << rid.pageNum << "," << rid.slotNum << ")";
            if (i < rl.size() - 1) std::cout << ",";
        }
        std::cout << "\"";
        if (it != (--entries.end())) cout << ",";
    }
}

void IXFloatLeaf::printKeys() {
    for (auto it = entries.begin(); it != entries.end(); it++) {
        std::cout << "\"" << it->first.data << ":";
        RIDlist rl = it->second;
        for (unsigned i = 0; i < rl.size(); i++) {
            RID rid = rl[i];
            std::cout << "(" << rid.pageNum << "," << rid.slotNum << ")";
            if (i < rl.size() - 1) std::cout << ",";
        }
        std::cout << "\"";
        if (it != (--entries.end())) cout << ",";
    }
}


RC IXCharLeaf::deleteEntry(const void* key, const RID& rid)
{
    std::string varchar = getVarcharKey(key); // read in varchar and fill in varchar length

    Key<std::string> k(varchar.length(), varchar, -1, -1); 	    // create key
    if (entries.find(k) == entries.end()) return -1;    // <Key, RIDList> doesn't exist, return err

    // <Key, RIDList> does exist, Search the RIDList
    int idxToDelete = -1;
    RIDlist& rl = entries[k];
    for (unsigned i = 0; i < rl.size(); i++) {
        RID curRID = rl.at(i);
        if (curRID.pageNum == rid.pageNum && curRID.slotNum == rid.slotNum) {
            idxToDelete = i;
            break;
        }
    }
    if (idxToDelete == -1) return -1;
    rl.erase(rl.begin() + idxToDelete);
    free_space += sizeof(RID);

    // If size of entries == 0, delete <Key, RIDList>
    if (rl.size() == 0) {
        entries.erase(k);
        free_space += 8;
        key_count--;
    }
    return successful;
}

RC IXIntLeaf::deleteEntry(const void *key, const RID &rid) {
    int data = *(int*)key;	// get value of int key
    Key<int> k(4, data, -1, -1); 	// create key

    if (entries.find(k) == entries.end()) return -1;

    // <Key, RIDList> does exist, Search the RIDList
    int idxToDelete = -1;
    RIDlist& rl = entries[k];
    for (unsigned i = 0; i < rl.size(); i++) {
        RID curRID = rl.at(i);
        if (curRID.pageNum == rid.pageNum && curRID.slotNum == rid.slotNum) {
            idxToDelete = i;
            break;
        }
    }
    if (idxToDelete == -1) return -1;
    rl.erase(rl.begin() + idxToDelete);
    free_space += sizeof(RID);

    // If size of entries == 0, delete <Key, RIDList>
    if (rl.size() == 0) {
        entries.erase(k);
        free_space += 8;
        key_count--;
    }
    return successful;
}

RC IXFloatLeaf::deleteEntry(const void* key, const RID& rid) {
    float data = *(float*)key;	// get value of float key

    Key<float> k(4, data, -1, -1); 	// create key

    // <Key, RIDList> does exist, Search the RIDList
    int idxToDelete = -1;
    RIDlist& rl = entries[k];
    for (unsigned i = 0; i < rl.size(); i++) {
        RID curRID = rl.at(i);
        if (curRID.pageNum == rid.pageNum && curRID.slotNum == rid.slotNum) {
            idxToDelete = i;
            break;
        }
    }
    if (idxToDelete == -1) return -1;
    rl.erase(rl.begin() + idxToDelete);
    free_space += sizeof(RID);

    // If size of entries == 0, delete <Key, RIDList>
    if (rl.size() == 0) {
        entries.erase(k);
        free_space += 8;
        key_count--;
    }
    return successful;
}
