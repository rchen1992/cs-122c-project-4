#ifndef IXTYPES_H
#define IXTYPES_H

#include "../rbf/types.h"
#include <list>

#define DEBUGMODE false

typedef enum { Leaf = 0, Nonleaf } PageType;
typedef int PID;
typedef std::vector<RID> RIDlist;

typedef enum { successful = 0, pagefull, error } RCType;

template <typename T>
struct Key {
	int length;
	T data;
	PID left_child;
	PID right_child;

	Key(int l, T d, PID left, PID right) 
		: length(l), data(d), left_child(left), right_child(right) {}

	// Varchar < Comparator
	bool operator<(const Key<char*>& rhs) const {
		return (strcmp(data, rhs.data) < 0);
	}	
	// Varchar < Comparator
	bool operator<(const Key<std::string>& rhs) const {
		return (data < rhs.data);
	}	

	// Int < Comparator
	bool operator<(const Key<int>& rhs) const {
		return data < rhs.data;
	}	
	// Float < Comparator
	bool operator<(const Key<float>& rhs) const {
		return data < rhs.data;
	}		

	// Varchar > Comparator
	bool operator>(const Key<char*>& rhs) const {
		return (strcmp(data, rhs.data) < 0);
	}	
	// Varchar > Comparator
	bool operator>(const Key<std::string>& rhs) const {
		return (data > rhs.data);
	}	
	// Int > Comparator
	bool operator>(const Key<int>& rhs) const {
		return data > rhs.data;
	}	
	// Float > Comparator
	bool operator>(const Key<float>& rhs) const {
		return data > rhs.data;
	}	

	// Varchar == Comparator
	bool operator==(const Key<char*>& rhs) const {
		return (strcmp(data, rhs.data) == 0);
	}		
	bool operator==(const Key<std::string>& rhs) const {
		return (data == rhs.data);
	}	
	// Int == Comparator
	bool operator==(const Key<int>& rhs) const {
		return data == rhs.data;
	}	
	// Float == Comparator
	bool operator==(const Key<float>& rhs) const {
		return data == rhs.data;
	}
};

// Returns a memset char array equal to the size of a page
inline char* getPageBlock() {
	char* data = new char[PAGE_SIZE];
	memset(data, 0, PAGE_SIZE);
	return data;
}

inline char* getVarcharKey(const void* key, int& varchar_length) {
	char* c = (char*)key;
	varchar_length = *(int*)key; 			// get varchar length
	c += 4;
	char* varchar = new char[varchar_length];   // get actual varchar
	memset(varchar, 0, varchar_length);
	memcpy(varchar, c, varchar_length);

	return varchar;
}

inline std::string getVarcharKey(const void* key) {
	char* c = (char*)key;
	int varchar_length = *(int*)key; 			// get varchar length
	c += 4;
	std::string varchar(c, varchar_length);

	return varchar;
}

#endif