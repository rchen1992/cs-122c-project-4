#include <iostream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "ix.h"
#include "ixtest_util.h"

IndexManager *indexManager;

int testCase_1(const string &indexFileName)
{
    // Functions tested
    // insertEntry() with no splits
    cerr << endl << "****In simple insertEntry() with splits test case****" << endl;

    // create index file
    assertCreateIndexFile(success, indexManager, indexFileName);

    // open index file
    IXFileHandle fileHandle;
    assertOpenIndexFile(success, indexManager, indexFileName, fileHandle);

    // Attribute a = createAttribute(TypeVarChar);
    // RID rid = createRID(1,0);
    // int varchar_length = 10;
    // char* key = new char[varchar_length+4];
    // memset(key, 0, 4+varchar_length);
    // *(int*)key = varchar_length;
    // char* c = key;
    // c += 4;
    // char* s = "helloworld";
    // memcpy(c, s, varchar_length);
    // RC rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);  

    // s = "helloworld";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);       
    // s = "helloworld";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);       
    // s = "jumpingjak";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "appleapple";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "coolbeansa";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "jumpingjak";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "wasdfjhwef";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "whywhywhya";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);     
    // s = "awesomeyaa";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "gooooooooo";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);    
    // s = "wooooooooo";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "asdfefwuhd";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "weufhskufh";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "efbfjduenf";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "askfjhwefh";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "nveuhfnskj";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      
    // s = "ewheffkjdj";
    // memset(c, 0, varchar_length);
    // memcpy(c, s, varchar_length);
    // rc = indexManager->insertEntry(fileHandle, a, (void*)key, rid);      

    // delete key;

    // indexManager->printBtree(fileHandle, a);

    Attribute a = createAttribute(TypeInt);
    RID rid = createRID(1,0);
    int* key = new int;
    for (int i = 0; i < 20; i=i+2) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }    
    for (int i = 30; i > 1; i=i-3) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }
    for (int i = 50; i > 2; i=i-4) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }
    for (int i = 100; i > 2; i=i-3) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }
    for (int i = 200; i > 2; i=i-7) {
        *key = i;
        indexManager->insertEntry(fileHandle, a, key, rid);
    }

    indexManager->printBtree(fileHandle, a);

    delete key;


    // close index file
    assertCloseIndexFile(success, indexManager, fileHandle);

    // destroy index file
    assertDestroyIndexFile(success, indexManager, indexFileName);

    return success;
}

int main()
{
    //Global Initializations
    indexManager = IndexManager::instance();

    const string indexFileName = "age_idx";

    testCase_1(indexFileName);
    cerr << "IX_Test Case 1 passed" << endl;
}

