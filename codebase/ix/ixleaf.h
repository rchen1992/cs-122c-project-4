#ifndef IXLEAF_H
#define IXLEAF_H

#include "ixtypes.h"
#include "ixpage.h"

// BASE CLASS
class IXLeaf {
public:
	virtual ~IXLeaf() {};
	virtual RC loadPage(void* data) = 0;
	virtual RC flush(void* data) = 0;
	virtual RC insertEntry(const void* key, const RID& rid) = 0;
	virtual RC insertFromAnotherLeaf(void* key, RIDlist& rid_list) = 0;
	virtual int getMid() = 0;
	virtual RC split(IXLeaf* new_child, IXPage* parent) = 0;
	virtual RC prepForScan(const void* key, bool keyInclusive) = 0;
	virtual RC popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey) = 0;
	virtual RC pop(RID& rid, void* returnKey) = 0;
	virtual void printDebug() = 0;

	virtual int getKeyCount();
	virtual int getFreeSpace();
	virtual bool hasSpace(AttrType type, const void* key) = 0;
	virtual PID getNextPID();
	virtual PID getPID();
	virtual PageType getLevel();
	virtual void printKeys() = 0;

	virtual void setNextPID(PID new_pid);

    virtual RC deleteEntry(const void *key, const RID &rid) = 0;
protected:
	int key_count;
	int free_space;
	PageType level;
	PID next_pid;
	PID pid;
};

// VARCHARS
class IXCharLeaf : public IXLeaf {
public:
	IXCharLeaf(PID page_num);
	~IXCharLeaf();

	RC loadPage(void* data);
	RC flush(void* data);
	RC insertEntry(const void* key, const RID& rid);
	bool hasSpace(AttrType type, const void* key);
	RC insertFromAnotherLeaf(void* key, RIDlist& rid_list);
	int getMid();
	RC split(IXLeaf* new_child, IXPage* parent);
	RC prepForScan(const void* key, bool keyInclusive);
	RC popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey);
	RC pop(RID& rid, void* returnKey);
	void printDebug();
	void printKeys();
    RC deleteEntry(const void* key, const RID& rid);
private:
	std::map<Key<std::string>, RIDlist> entries;
};


// INTS
class IXIntLeaf : public IXLeaf {
public:
	IXIntLeaf(PID page_num);
	~IXIntLeaf();

	RC loadPage(void* data);
	RC flush(void* data);
	RC insertEntry(const void* key, const RID& rid);
	bool hasSpace(AttrType type, const void* key);
	RC insertFromAnotherLeaf(void* key, RIDlist& rid_list);
	int getMid();
	RC split(IXLeaf* new_child, IXPage* parent);
	RC prepForScan(const void* key, bool keyInclusive);
	RC popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey);
	RC pop(RID& rid, void* returnKey);
	void printDebug();
	void printKeys();
    RC deleteEntry(const void* key, const RID& rid);
private:
	std::map<Key<int>, RIDlist> entries;
};

// FLOATS
class IXFloatLeaf : public IXLeaf {
public:
	IXFloatLeaf(PID page_num);
	~IXFloatLeaf();

	RC loadPage(void* data);
	RC flush(void* data);
	RC insertEntry(const void* key, const RID& rid);
	bool hasSpace(AttrType type, const void* key);
	RC insertFromAnotherLeaf(void* key, RIDlist& rid_list);
	int getMid();
	RC split(IXLeaf* new_child, IXPage* parent);
	RC prepForScan(const void* key, bool keyInclusive);
	RC popNextEntry(const void* highKey, bool highKeyInclusive, RID& rid, void* returnKey);
	RC pop(RID& rid, void* returnKey);
	void printDebug();
	void printKeys();
    RC deleteEntry(const void* key, const RID& rid);
private:
	std::map<Key<float>, RIDlist> entries;
};

#endif