#include "ix.h"
#include "../rbf/types.h"

IndexManager* IndexManager::_index_manager = 0;

IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

IndexManager::IndexManager()
{
}

IndexManager::~IndexManager()
{
}

RC IndexManager::createFile(const string &fileName)
{
    return PagedFileManager::instance()->createFile(fileName);
}

RC IndexManager::destroyFile(const string &fileName)
{
    return PagedFileManager::instance()->destroyFile(fileName);
}

RC IndexManager::openFile(const string &fileName, IXFileHandle &ixFileHandle)
{
    RC opened = PagedFileManager::instance()->openFile(fileName, ixFileHandle.getFileHandle());
    if (opened == 0) { 
        // If there are no pages in file, create the root and first leaf page
        if (ixFileHandle.getNumberOfPages() == 0) {
            // Append root
            IXPage* root = ixFileHandle.newPage(TypeInt);
            delete root;

            // Append first leaf
            IXLeaf* first_leaf = ixFileHandle.newLeaf(TypeInt);
            delete first_leaf;
        } 
        return 0;
    }

    return -1;
}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
    return PagedFileManager::instance()->closeFile(ixfileHandle.getFileHandle());
}

RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    IXPage* root = ixfileHandle.getIXPage(attribute.type, 0);
    // If root is empty, insert directly into first leaf
    if (root->getKeyCount() == 0) {
        IXLeaf* first_leaf = ixfileHandle.getIXLeaf(attribute.type, 1);
        return insertEntryIntoLeaf(first_leaf, root, ixfileHandle, attribute, key, rid);
    }
    // if root is nonempty but has space, begin traversal
    else if (root->hasSpace(attribute.type, key)) {
        return insertEntryIntoPage(root, 0, ixfileHandle, attribute, key, rid);
    }
    // Otherwise, root is full, so split and create new level
    else {
        return splitRoot(root, ixfileHandle, attribute, key, rid);
    }
}

RC IndexManager::insertEntryIntoPage(IXPage* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    // If child has space for another key
    // Find the page number of next page to traverse
    // Pull in page depending on page type and attribute type
    if (child->hasSpace(attribute.type, key)) {
        PID next_pid = -1;
        RC rc = child->findNextPID(key, next_pid);

        // Error in finding key in child
        if (rc == error) {
            if (parent)
                delete parent;
            delete child;
            return error;
        }

        if (rc == successful) {
            // Parent no longer necessary
            if (parent) {
                ixfileHandle.flushIXPage(parent);
                delete parent;
            }

            char* data = getPageBlock();
            PageType type = ixfileHandle.getPageType(data, next_pid);
            switch(type) {
                case Leaf: {
                    IXLeaf* next_child = ixfileHandle.getIXLeaf(attribute.type, next_pid, data);
                    return insertEntryIntoLeaf(next_child, child, ixfileHandle, attribute, key, rid);
                }
                case Nonleaf: {
                    IXPage* next_child = ixfileHandle.getIXPage(attribute.type, next_pid, data);
                    return insertEntryIntoPage(next_child, child, ixfileHandle, attribute, key, rid);
                }
            }
        }
    }
    // Otherwise, split page
    else { 
        return splitPage(child, parent, ixfileHandle, attribute, key, rid);
    }
}

RC IndexManager::insertEntryIntoLeaf(IXLeaf* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    // If child has space for another key
    // do insert
    if (child->hasSpace(attribute.type, key)) {
        child->insertEntry(key, rid);
        ixfileHandle.flushIXPage(parent);
        ixfileHandle.flushIXLeaf(child);

        // int pid = child->getPID();
        // delete child;
        // IXLeaf* test = ixfileHandle.getIXLeaf(attribute.type, pid);
        // test->printDebug();

        if (DEBUGMODE) {
            child->printDebug();
        }
        
        delete parent;
        delete child;
        return successful;
    }
    // Otherwise, split the leaf
    else { 
        return splitLeaf(child, parent, ixfileHandle, attribute, key, rid);
    }   
}

RC IndexManager::splitRoot(IXPage* root, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    // Add 2 new pages to end of file for left and right child
    IXPage* new_left = ixfileHandle.newPage(attribute.type);
    IXPage* new_right = ixfileHandle.newPage(attribute.type);

    // Copy over and then delete all entries greater or equal to mid from current child to new child
    // Push mid key up to parent
    RC split = root->splitRoot(new_left, new_right);
    if (split != successful) {
        return error;
    }

    if (DEBUGMODE) {
        std::cout << "********* AFTER ROOT SPLIT *********" << std::endl;
        root->printDebug();
        new_left->printDebug();
        new_right->printDebug();
        std::cout << "********* END ROOT SPLIT *********" << std::endl;
    }

    // Finish insert by looking at parent and seeing which child is correct leaf to insert
    PID target_child = -1;
    RC find = root->findNextPID(key, target_child);
    if (find == successful) {
        if (target_child == new_left->getPID()) {
            // Flush unused new child and complete insert
            ixfileHandle.flushIXPage(new_right);
            delete new_right; 
            return insertEntryIntoPage(new_left, root, ixfileHandle, attribute, key, rid);
        }
        else if (target_child == new_right->getPID()) {
            // Flush unused current child and complete insert
            ixfileHandle.flushIXPage(new_left); 
            delete new_left; 
            return insertEntryIntoPage(new_right, root, ixfileHandle, attribute, key, rid);
        }
        else {
            std::cout << "In splitRoot: Target child PID does not match either child." << std::endl;
            return error;
        }
    }
    else {
        std::cout << "In splitRoot: target child PID not found." << std::endl;
        return error;
    }
}

RC IndexManager::splitPage(IXPage* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    // Add new page to end of file
    IXPage* new_child = ixfileHandle.newPage(attribute.type);

    // Copy over and then delete all entries greater than mid from current child to new child
    // Push mid key up to parent
    RC split = child->split(new_child, parent);
    if (split != successful) {
        return error;
    }

    if (DEBUGMODE) {
        std::cout << "********* AFTER NONLEAF SPLIT *********" << std::endl;
        child->printDebug();
        new_child->printDebug();
        parent->printDebug();
        std::cout << "********* END NONLEAF SPLIT *********" << std::endl;
    }

    // Continue insert by looking at parent and seeing which child is correct page to traverse
    PID target_child = -1;
    RC find = parent->findNextPID(key, target_child);
    if (find == successful) {
        if (target_child == child->getPID()) {
            // Flush unused new child and continue insert
            ixfileHandle.flushIXPage(new_child);
            delete new_child; 
            return insertEntryIntoPage(child, parent, ixfileHandle, attribute, key, rid);
        }
        else if (target_child == new_child->getPID()) {
            // Flush unused current child and continue insert
            ixfileHandle.flushIXPage(child); 
            delete child; 
            return insertEntryIntoPage(new_child, parent, ixfileHandle, attribute, key, rid);
        }
        else {
            std::cout << "In splitPage: Target child PID does not match either child." << std::endl;
            return error;
        }
    }
    else {
        std::cout << "In splitPage: target child PID not found." << std::endl;
        return error;
    }
}

RC IndexManager::splitLeaf(IXLeaf* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    // Add new leaf to end of file
    IXLeaf* new_child = ixfileHandle.newLeaf(attribute.type);


    // Adjust next pointers of child and new_child
    // Copy over and then delete all entries greater or equal to mid from current child to new child
    // Push mid key up to parent
    RC split = child->split(new_child, parent);
    if (split != successful) {
        return error;
    }

    if (DEBUGMODE) {
        std::cout << "********* AFTER SPLIT *********" << std::endl;
        child->printDebug();
        new_child->printDebug();
        parent->printDebug();
    }

    // Finish insert by looking at parent and seeing which child is correct leaf to insert
    PID target_child = -1;
    RC find = parent->findNextPID(key, target_child);
    if (find == successful) {
        if (target_child == child->getPID()) {
            // Flush unused new child and complete insert
            ixfileHandle.flushIXLeaf(new_child);
            delete new_child; 
            return insertEntryIntoLeaf(child, parent, ixfileHandle, attribute, key, rid);
        }
        else if (target_child == new_child->getPID()) {
            // Flush unused current child and complete insert
            ixfileHandle.flushIXLeaf(child); 
            delete child; 
            return insertEntryIntoLeaf(new_child, parent, ixfileHandle, attribute, key, rid);
        }
        else {
            std::cout << "In splitLeaf: Target child PID does not match either child." << std::endl;
            return error;
        }
    }
    else {
        std::cout << "In splitLeaf: target child PID not found." << std::endl;
        return error;
    }
}

RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
//    cout << "The query key: " << *(int*)key << endl;
//    cout << "RID = (pageNum = " << rid.pageNum << ", slotNum = " << rid.slotNum << ")" << endl;
    IXLeaf* leaf = ixfileHandle.findLeaf(attribute, key, true);

//    cout << endl <<  "\tBEFORE DELETE Print keys: " << endl;
//    leaf->printKeys();
//    cout << endl;

    RC rc = leaf->deleteEntry(key, rid);
    if (rc == -1) {
        cout << "<Key, RID> pair does not exist." << endl;
        return rc;
    }
    ixfileHandle.flushIXLeaf(leaf);

//    cout << endl << "\tAFTER DELETE Print keys: " << endl;
//    leaf->printKeys();
//    cout << endl;

    delete leaf;
    return successful;
}

RC IndexManager::scan(IXFileHandle &ixfileHandle,
        const Attribute &attribute,
        const void      *lowKey,
        const void      *highKey,
        bool			lowKeyInclusive,
        bool        	highKeyInclusive,
        IX_ScanIterator &ix_ScanIterator)
{
    return ix_ScanIterator.init(ixfileHandle, attribute, lowKey, highKey, lowKeyInclusive, highKeyInclusive);
}


void IndexManager::printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const {
    int level = 0;
    int pid = 0;

    IXPage* root = ixfileHandle.getIXPage(attribute.type, 0);
    if (root->getKeyCount() == 0) pid = 1;

    IXLeaf* leaf = ixfileHandle.getIXLeaf(attribute.type, 1);
    if (leaf->getKeyCount() == 0) {
        cout << "BTree is empty." << endl;
        return;
    }
    preorderPrint(ixfileHandle, attribute, pid, level);
    cout << "\n";
}

void IndexManager::preorderPrint(IXFileHandle &ixfileHandle, const Attribute &attribute, PID pid, int level) const {
    char* data = getPageBlock();
    PageType type = ixfileHandle.getPageType(data, pid);

    string indent = "";
    for (int i = 0; i < level; i++) indent += "    ";

    if (type == Nonleaf) {
        // Print Keys and Children
        cout << indent << "{\n";
        cout << indent << "\"keys\":[";
        IXPage* page = ixfileHandle.getIXPage(attribute.type, pid, data);
        page->printKeys();
        cout << "],\n";

        cout << indent << "\"children\": [\n";
        std::vector<PID> childPIDs = page->getChildPIDs();
        for (unsigned i = 0; i < childPIDs.size(); i++) {
            preorderPrint(ixfileHandle, attribute, childPIDs[i], level + 1);
            if (i != childPIDs.size() -1) cout << ",\n";
        }
        cout << "\n";
        cout << indent << "  ]\n";
        cout << indent << "}";
    } else if (type == Leaf) {
        // Print Keys and RIDList
        IXLeaf* leaf = ixfileHandle.getIXLeaf(attribute.type, pid, data);
        cout << indent << "{\"keys\": [";
        leaf->printKeys();
        cout << "]}";
    }
}



IX_ScanIterator::IX_ScanIterator()
{
    attribute.type = TypeInt;
    attribute.length = 4;
    attribute.name = "None";

}

IX_ScanIterator::~IX_ScanIterator()
{
}

RC IX_ScanIterator::init(IXFileHandle &ixfh,
        const Attribute &attr,
        const void      *lkey,
        const void      *hkey,
        bool            lkeyInc,
        bool            hkeyInc) 
{
    attribute.type = attr.type;
    attribute.name = attr.name;
    attribute.length = attr.length;

    ixfileHandle = &ixfh;
    lowKey = lkey;
    highKey = hkey;
    lowKeyInclusive = lkeyInc;
    highKeyInclusive = hkeyInc;


    // If low key is null, grab first leaf
    if (lowKey == 0) {
        current_leaf = ixfileHandle->getIXLeaf(attribute.type, 1);
        if (current_leaf->getKeyCount() == 0) {
            return error;
        }
//        std::cout << "lowkey null: found current_leaf with PID #" << current_leaf->getPID() << std::endl;
    }
    else {
        current_leaf = ixfileHandle->findLeaf(attribute, lowKey, lowKeyInclusive);
//        std::cout << "found current_leaf with PID #" << current_leaf->getPID() << std::endl;
        current_leaf->prepForScan(lowKey, lowKeyInclusive);
//        std::cout << "prep for scan complete" << std::endl;
    }

    return successful;
}

RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
    // If no more entries in leaf
    if (current_leaf->getKeyCount() == 0) {
        // std::cout << "current leaf has no entries" << std::endl;
        // If no more leaves after this, return EOF
        PID next_pid = current_leaf->getNextPID();
        if (next_pid == -1) {
            delete current_leaf;
            return IX_EOF;
        }
        else {
            delete current_leaf;
            current_leaf = ixfileHandle->getIXLeaf(attribute.type, next_pid);
            return getNextEntry(rid, key);
        }
    }

    // std::cout << "current leaf has entries" << std::endl;

    RC found = current_leaf->popNextEntry(highKey, highKeyInclusive, rid, key);  
    if (found != successful) {
        delete current_leaf;
        return IX_EOF;
    }


    return found;
}

RC IX_ScanIterator::close()
{
    return successful;
}


void IX_PrintError (RC rc)
{
}
