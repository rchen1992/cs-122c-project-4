#include "ixfh.h"

// --------------- IX FILE HANDLE ---------------- //
IXFileHandle::IXFileHandle()
{
}

IXFileHandle::~IXFileHandle()
{
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    readPageCount   = fileHandle.readPageCounter;
    writePageCount  = fileHandle.writePageCounter;
    appendPageCount = fileHandle.appendPageCounter;
    return 0;
}

RC IXFileHandle::readPage(PageNum pageNum, void *data) {
    return fileHandle.readPage(pageNum, data);
}

RC IXFileHandle::writePage(PageNum pageNum, const void *data) {
    return fileHandle.writePage(pageNum, data);
}    

RC IXFileHandle::appendPage(const void *data) {
    return fileHandle.appendPage(data);
}                                

unsigned IXFileHandle::getNumberOfPages() {
    return fileHandle.getNumberOfPages();
}

FileHandle& IXFileHandle::getFileHandle() {
    return fileHandle;
}

IXPage* IXFileHandle::getIXPage(AttrType type, PID pid) {
    char* data = getPageBlock();
    readPage(pid, (void*)data);
    IXPage* page;

    switch(type) {
        case TypeVarChar: {
            page = new IXCharPage(pid);
            break;
        }        
        case TypeInt: {
            page = new IXIntPage(pid);
            break;
        }        
        case TypeReal: {
            page = new IXFloatPage(pid);
            break;
        }        
    }

    page->loadPage(data);

    delete[] data;

    return page;
}

IXPage* IXFileHandle::getIXPage(AttrType type, PID pid, char* data) {
    IXPage* page;

    switch(type) {
        case TypeVarChar: {
            page = new IXCharPage(pid);
            break;
        }        
        case TypeInt: {
            page = new IXIntPage(pid);
            break;
        }        
        case TypeReal: {
            page = new IXFloatPage(pid);
            break;
        }        
    }

    page->loadPage(data);

    delete[] data;

    return page;
}

IXLeaf* IXFileHandle::getIXLeaf(AttrType type, PID pid) {
    char* data = getPageBlock();
    readPage(pid, (void*)data);
    IXLeaf* leaf;

    switch(type) {
        case TypeVarChar: {
            leaf = new IXCharLeaf(pid);
            break;
        }        
        case TypeInt: {
            leaf = new IXIntLeaf(pid);
            break;
        }        
        case TypeReal: {
            leaf = new IXFloatLeaf(pid);
            break;
        }
    }
    
    leaf->loadPage(data);

    delete[] data;

    return leaf;
}

IXLeaf* IXFileHandle::getIXLeaf(AttrType type, PID pid, char* data) {
    IXLeaf* leaf;

    switch(type) {
        case TypeVarChar: {
            leaf = new IXCharLeaf(pid);
            break;
        }        
        case TypeInt: {
            leaf = new IXIntLeaf(pid);
            break;
        }        
        case TypeReal: {
            leaf = new IXFloatLeaf(pid);
            break;
        }
    }
    
    leaf->loadPage(data);

    delete[] data;

    return leaf;
}

PageType IXFileHandle::getPageType(char* data, PID pid) {
    readPage(pid, (void*)data);
    char* c = data + 8;
    PageType type = *(PageType*)c;
    return type;
}

void IXFileHandle::flushIXPage(IXPage* page) {
    char* data = getPageBlock();
    page->flush(data);
    writePage(page->getPID(), data);
    delete[] data;
}

void IXFileHandle::flushIXLeaf(IXLeaf* leaf) {
    char* data = getPageBlock();
    leaf->flush(data);
    writePage(leaf->getPID(), data);
    delete[] data;
}


IXPage* IXFileHandle::newPage(AttrType type) {
    IXPage* page;
    switch(type) {
        case TypeVarChar: {
            page = new IXCharPage(getNumberOfPages());
            break;
        }        
        case TypeInt: {
            page = new IXIntPage(getNumberOfPages());
            break;
        }        
        case TypeReal: {
            page = new IXFloatPage(getNumberOfPages());
            break;
        }
    }

    char* data = getPageBlock();
    page->flush((void*)data);
    appendPage(data);
    delete[] data;

    return page;
}

IXLeaf* IXFileHandle::newLeaf(AttrType type) {
    IXLeaf* leaf;
    switch(type) {
        case TypeVarChar: {
            leaf = new IXCharLeaf(getNumberOfPages());
            break;
        }        
        case TypeInt: {
            leaf = new IXIntLeaf(getNumberOfPages());
            break;
        }        
        case TypeReal: {
            leaf = new IXFloatLeaf(getNumberOfPages());
            break;
        }
    }

    char* data = getPageBlock();
    leaf->flush((void*)data);
    appendPage(data);
    delete[] data;

    return leaf;
}

IXLeaf* IXFileHandle::findLeaf(const Attribute& attribute, const void* key, bool keyInclusive) {
    IXPage* root = getIXPage(attribute.type, 0);

    // If root is empty, then first leaf is the only page
    if (root->getKeyCount() == 0) {
        return getIXLeaf(attribute.type, 1);
    }
    else {
        return findLeafRecursive(root, attribute, key, keyInclusive);
    }
}

IXLeaf* IXFileHandle::findLeafRecursive(IXPage* page, const Attribute& attribute, const void* key, bool keyInclusive) {
    PID next_pid = -1;
    page->findNextPID(key, next_pid);
    delete page;

    char* data = getPageBlock();
    PageType type = getPageType(data, next_pid);
    switch(type) {
        case Leaf: {
            return getIXLeaf(attribute.type, next_pid, data);
        }
        case Nonleaf: {
            IXPage* child = getIXPage(attribute.type, next_pid, data);
            return findLeafRecursive(child, attribute, key, keyInclusive);
        }
    }
}
