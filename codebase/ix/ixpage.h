#ifndef IXPAGE_H
#define IXPAGE_H

#include "ixtypes.h"

// BASE CLASS
class IXPage {
public:
	virtual ~IXPage() {};
	virtual RC loadPage(void* data) = 0;
	virtual RC flush(void* data) = 0;
	virtual RC findNextPID(const void* key, PID& pid) = 0;
	virtual RC insertKey(void* key) = 0;
	virtual RC split(IXPage* new_child, IXPage* parent) = 0;
	virtual RC splitRoot(IXPage* new_left, IXPage* new_right) = 0;

	virtual int getKeyCount();
	virtual int getFreeSpace();
	virtual bool hasSpace(AttrType type, const void* key);
	virtual PageType getLevel();
	virtual PID getPID();
    virtual void printKeys() = 0;
    virtual std::vector<PID> getChildPIDs() = 0;
	virtual void printDebug() = 0;
protected:
	int key_count;
	int free_space;
	PageType level;
	PID pid;
};


// VARCHARS
class IXCharPage : public IXPage {
public:
	IXCharPage(PID page_num);
	~IXCharPage();
	RC loadPage(void* data);
	RC flush(void* data);
	RC findNextPID(const void* key, PID& next_pid);
	void printKeys();
	std::vector<PID> getChildPIDs();
	std::list<Key<std::string>> getKeys();
	RC insertKey(void* key);
	RC split(IXPage* new_child, IXPage* parent);
	RC splitRoot(IXPage* new_left, IXPage* new_right);

	void printDebug();
private:
	std::list<Key<std::string>> keys;
};


// INTS
class IXIntPage : public IXPage {
public:
	IXIntPage(PID page_num);
	~IXIntPage();
	RC loadPage(void* data);
	RC flush(void* data);
	RC findNextPID(const void* key, PID& next_pid);
	void printKeys();
	std::vector<PID> getChildPIDs();
	std::list<Key<int>> getKeys();
	RC insertKey(void* key);
	RC split(IXPage* new_child, IXPage* parent);
	RC splitRoot(IXPage* new_left, IXPage* new_right);

	void printDebug();
private:
	std::list<Key<int>> keys;
};

// FLOATS
class IXFloatPage : public IXPage {
public:
	IXFloatPage(PID page_num);
	~IXFloatPage();
	RC loadPage(void* data);
	RC flush(void* data);
	RC findNextPID(const void* key, PID& next_pid);
	void printKeys();
	std::vector<PID> getChildPIDs();
	std::list<Key<float>> getKeys();
	RC insertKey(void* key);
	RC split(IXPage* new_child, IXPage* parent);
	RC splitRoot(IXPage* new_left, IXPage* new_right);

	void printDebug();
private:
	std::list<Key<float>> keys;
};


#endif