#ifndef IXFH_H
#define IXFH_H

#include "../rbf/rbfm.h"
#include "ixpage.h"
#include "ixleaf.h"

class IXFileHandle {
    public:
        // Put the current counter values of associated PF FileHandles into variables
        RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);

        unsigned getNumberOfPages();
        FileHandle& getFileHandle();

        IXPage* getIXPage(AttrType type, PID pid);                  // Returns nonleaf object by reading in data block
        IXPage* getIXPage(AttrType type, PID pid, char* data);      // Returns nonleaf object by using data block
        IXLeaf* getIXLeaf(AttrType type, PID pid);                  // Returns leaf object by reading in data block 
        IXLeaf* getIXLeaf(AttrType type, PID pid, char* data);      // Returns leaf object by using data block
        PageType getPageType(char* data, PID pid);

        void flushIXPage(IXPage* page);
        void flushIXLeaf(IXLeaf* leaf);

        IXPage* newPage(AttrType type);
        IXLeaf* newLeaf(AttrType type);

        IXLeaf* findLeaf(const Attribute& attribute, const void* key, bool keyInclusive);
        IXLeaf* findLeafRecursive(IXPage* page, const Attribute& attribute, const void* key, bool keyInclusive);

        RC readPage(PageNum pageNum, void *data);                  // Read a specific page
        RC writePage(PageNum pageNum, const void *data);           // Write a specific page
        RC appendPage(const void *data);                           // Append a specific page

        IXFileHandle();  							// Constructor
        ~IXFileHandle(); 							// Destructor

    private:
        FileHandle fileHandle;
};

#endif