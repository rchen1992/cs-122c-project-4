#ifndef _ix_h_
#define _ix_h_

#include "../rbf/rbfm.h"
#include "ixtypes.h"
#include "ixfh.h"

# define IX_EOF (-1)  // end of the index scan

class IX_ScanIterator;
class IXFileHandle;

class IndexManager {

    public:
        static IndexManager* instance();

        // Create an index file
        RC createFile(const string &fileName);

        // Delete an index file
        RC destroyFile(const string &fileName);

        // Open an index and return a file handle
        RC openFile(const string &fileName, IXFileHandle &ixFileHandle);

        // Close a file handle for an index. 
        RC closeFile(IXFileHandle &ixfileHandle);

        // Insert an entry into the given index that is indicated by the given ixfileHandle
        RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        RC insertEntryIntoPage(IXPage* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);
        
        RC insertEntryIntoLeaf(IXLeaf* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        RC splitRoot(IXPage* root, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        RC splitPage(IXPage* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        RC splitLeaf(IXLeaf* child, IXPage* parent, IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Delete an entry from the given index that is indicated by the given fileHandle
        RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Initialize and IX_ScanIterator to supports a range search
        RC scan(IXFileHandle &ixfileHandle,
                const Attribute &attribute,
                const void *lowKey,
                const void *highKey,
                bool lowKeyInclusive,
                bool highKeyInclusive,
                IX_ScanIterator &ix_ScanIterator);

        // Print the B+ tree JSON record in pre-order
        void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const;
        void preorderPrint(IXFileHandle &ixfileHandle, const Attribute &attribute, PID pid, int level) const;
    protected:
        IndexManager();
        ~IndexManager();

    private:
        static IndexManager *_index_manager;
};

class IX_ScanIterator {
    public:
        IX_ScanIterator();  							// Constructor
        ~IX_ScanIterator(); 							// Destructor

        RC getNextEntry(RID &rid, void *key);  		    // Get next matching entry
        RC close();             						// Terminate index scan

        RC init(IXFileHandle &ixfh,
                const Attribute &attr,
                const void      *lkey,
                const void      *hkey,
                bool            lkeyInc,
                bool            hkeyInc);
    private:
        IXFileHandle* ixfileHandle;
        Attribute attribute;
        const void      *lowKey;
        const void      *highKey;
        bool            lowKeyInclusive;
        bool            highKeyInclusive;

        IXLeaf* current_leaf;
};



// print out the error message for a given return code
void IX_PrintError (RC rc);

#endif
