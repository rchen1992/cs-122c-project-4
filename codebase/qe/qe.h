#ifndef _qe_h_
#define _qe_h_

#include <vector>

#include "../rbf/rbfm.h"
#include "../rm/rm.h"
#include "../ix/ix.h"

#define QE_EOF (-1)  // end of the index scan

using namespace std;

typedef enum{ COUNT=0, SUM, AVG, MIN, MAX } AggregateOp;
// typedef enum { pass=0, fail } ScanCode; 

// The following functions use the following
// format for the passed data.
//    For INT and REAL: use 4 bytes
//    For VARCHAR: use 4 bytes for the length followed by
//                 the characters

struct Value {
    AttrType type;          // type of value
    void     *data;         // value
};


struct Condition {
    string  lhsAttr;        // left-hand side attribute
    CompOp  op;             // comparison operator
    bool    bRhsIsAttr;     // TRUE if right-hand side is an attribute and not a value; FALSE, otherwise.
    string  rhsAttr;        // right-hand side attribute if bRhsIsAttr = TRUE
    Value   rhsValue;       // right-hand side value if bRhsIsAttr = FALSE
};


class Iterator {
    // All the relational operators and access methods are iterators.
    public:
        virtual RC getNextTuple(void *data) = 0;
        virtual void getAttributes(vector<Attribute> &attrs) const = 0;
        virtual ~Iterator() {};
};


class TableScan : public Iterator
{
    // A wrapper inheriting Iterator over RM_ScanIterator
    public:
        RelationManager &rm;
        RM_ScanIterator *iter;
        string tableName;
        vector<Attribute> attrs;
        vector<string> attrNames;
        RID rid;

        TableScan(RelationManager &rm, const string &tableName, const char *alias = NULL):rm(rm)
        {
        	//Set members
        	this->tableName = tableName;

            // Get Attributes from RM
            rm.getAttributes(tableName, attrs);

            // Get Attribute Names from RM
            unsigned i;
            for(i = 0; i < attrs.size(); ++i)
            {
                // convert to char *
                attrNames.push_back(attrs.at(i).name);
            }

            // Call rm scan to get iterator
            iter = new RM_ScanIterator();
            rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);

            // Set alias
            if(alias) this->tableName = alias;
        };

        // Start a new iterator given the new compOp and value
        void setIterator()
        {
            iter->close();
            delete iter;
            iter = new RM_ScanIterator();
            rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);
        };

        RC getNextTuple(void *data)
        {
            return iter->getNextTuple(rid, data);
        };

        void getAttributes(vector<Attribute> &attrs) const
        {
            attrs.clear();
            attrs = this->attrs;
            unsigned i;

            // For attribute in vector<Attribute>, name it as rel.attr
            for(i = 0; i < attrs.size(); ++i)
            {
                string tmp = tableName;
                tmp += ".";
                tmp += attrs.at(i).name;
                attrs.at(i).name = tmp;
            }
        };

        ~TableScan()
        {
        	iter->close();
        };
};


class IndexScan : public Iterator
{
    // A wrapper inheriting Iterator over IX_IndexScan
    public:
        RelationManager &rm;
        RM_IndexScanIterator *iter;
        string tableName;
        string attrName;
        vector<Attribute> attrs;
        char key[PAGE_SIZE];
        RID rid;

        IndexScan(RelationManager &rm, const string &tableName, const string &attrName, const char *alias = NULL):rm(rm)
        {
        	// Set members
        	this->tableName = tableName;
        	this->attrName = attrName;


            // Get Attributes from RM
            rm.getAttributes(tableName, attrs);

            // Call rm indexScan to get iterator
            iter = new RM_IndexScanIterator();
            rm.indexScan(tableName, attrName, NULL, NULL, true, true, *iter);

            // Set alias
            if(alias) this->tableName = alias;
        };

        // Start a new iterator given the new key range
        void setIterator(void* lowKey,
                         void* highKey,
                         bool lowKeyInclusive,
                         bool highKeyInclusive)
        {
            iter->close();
            delete iter;
            iter = new RM_IndexScanIterator();
            rm.indexScan(tableName, attrName, lowKey, highKey, lowKeyInclusive,
                           highKeyInclusive, *iter);
        };

        RC getNextTuple(void *data)
        {
            int rc = iter->getNextEntry(rid, key);
            if(rc == 0)
            {
                rc = rm.readTuple(tableName.c_str(), rid, data);
            }
            return rc;
        };

        void getAttributes(vector<Attribute> &attrs) const
        {
            attrs.clear();
            attrs = this->attrs;
            unsigned i;

            // For attribute in vector<Attribute>, name it as rel.attr
            for(i = 0; i < attrs.size(); ++i)
            {
                string tmp = tableName;
                tmp += ".";
                tmp += attrs.at(i).name;
                attrs.at(i).name = tmp;
            }
        };

        ~IndexScan()
        {
            iter->close();
        };
};


class Filter : public Iterator {
    // Filter operator
    public:
        Filter(Iterator *input,               // Iterator of input R
               const Condition &condition     // Selection condition
        );
        ~Filter(){};

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const {};
        RC applyFilter(void* tuple, vector<Attribute>& attrs);
        bool intPassesCondition(int value);
        bool floatPassesCondition(float value);
        bool varcharPassesCondition(char* str, int length);
    private:
        Iterator* iter;
        Condition cond;
        vector<Attribute> iter_attrs;
        int buffer_size;
};


class Project : public Iterator {
    // Projection operator
    public:
        Project(Iterator *input,                    // Iterator of input R
              const vector<string> &attrNames);   // vector containing attribute names
        ~Project(){};

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const;

        void applyProjection(void* data, void* tuple, vector<Attribute>& attrs);
        void copyAttributeToOutput(void* tuple, char*& data_location, int offset, Attribute a);

        bool isProjectedAttribute(string name) const;
    private:
        Iterator* iter;
        const vector<string> attr_names;
        vector<Attribute> iter_attrs;
        int buffer_size;
};

// Optional for the undergraduate solo teams. 5 extra-credit points
class BNLJoin : public Iterator {
    // Block nested-loop join operator
    public:
        BNLJoin(Iterator *leftIn,            // Iterator of input R
               TableScan *rightIn,           // TableScan Iterator of input S
               const Condition &condition,   // Join condition
               const unsigned numRecords     // # of records can be loaded into memory, i.e., memory block size (decided by the optimizer)
        );
        ~BNLJoin(){};

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const;
        void readNextBlock();
        void moveBlockPosition();
        bool matchesCondition(void* left_tuple, void* right_tuple);
        void concatenateTuples(void* left_tuple, void* right_tuple, void* data);
    private:
        Iterator* left;
        TableScan* right;
        const Condition cond;
        const unsigned num_records;

        vector<void*> block;
        int block_pos;
        vector<Attribute> left_attrs;
        int left_buffer_size;
        vector<Attribute> right_attrs;
        int right_buffer_size;
};


// Helper
//
//void* getFieldValue2(const string &attributeName, const vector<Attribute> &attrs, const void *data) {
//    int offset = div_ceil(attrs.size(), 8); // number of null bytes is offset
//    for (unsigned i = 0; i < attrs.size(); i++) {
//        Attribute attr = attrs.at(i);
//        if (attr.type == TypeInt) {
//            int n;
//            memcpy(&n, (byte *) data + offset, sizeof(int));
//            offset += sizeof(int);
//
//            if (attr.name == attributeName) {
//                int* nx = new int;
//                *nx = n;
//                return (void*)nx;
//            }
//
//        } else if (attr.type == TypeReal) {
//            float f;
//            memcpy(&f, (byte *) data + offset, sizeof(float));
//            offset += sizeof(float);
//
//            if (attr.name == attributeName) {
//                float* fx = new float;
//                *fx = f;
//                return (void*)fx;
//            }
//
//        } else if (attr.type == TypeVarChar) {
//            int nameLen;
//            memcpy(&nameLen, (byte *) data + offset, sizeof(int));
//            offset += sizeof(int);
//            byte *buffer = (byte *) calloc(nameLen + 1, sizeof(byte));
//            memcpy(buffer, (byte *) data + offset, nameLen);
//            offset += nameLen;
//
////            cout << attr.name << " : " << buffer << "    ";
//            if (attr.name == attributeName) {
//                byte* wBuffer = (byte*) calloc(4 + nameLen + 1, sizeof(byte));
//                memcpy(wBuffer, &nameLen, sizeof(int));
//                memcpy(wBuffer + 4, buffer, nameLen);
//                return wBuffer; // This varcharLen and varchar recombined to use as key.
//            }
//            free(buffer);
//            buffer = NULL;
//        }
//    }
//}

class INLJoin : public Iterator {
// Index nested-loop join operator
public:
    INLJoin(Iterator *leftIn,           // Iterator of input R
           IndexScan *rightIn,          // IndexScan Iterator of input S
           const Condition &condition   // Join condition
    );
    ~INLJoin();
    RC getNextTuple(void *data);
    // For attribute in vector<Attribute>, name it as rel.attr
    void getAttributes(vector<Attribute> &attrs) const;
    bool matchCondition(void* leftTuple, void* rightTuple);
    void concatenateTuples(void* leftTuple, void* rightTuple, void* data);
private:
    Iterator *leftIter;
    IndexScan *rightIXIter;
    const Condition &cond;

    vector<Attribute> leftAttrs;
    vector<Attribute> rightAttrs;
    vector<Attribute> combinedAttrs;

    byte* left;
    byte* right;
};

// Optional for everyone. 10 extra-credit points
class GHJoin : public Iterator {
    // Grace hash join operator
    public:
      GHJoin(Iterator *leftIn,               // Iterator of input R
            Iterator *rightIn,               // Iterator of input S
            const Condition &condition,      // Join condition (CompOp is always EQ)
            const unsigned numPartitions     // # of partitions for each relation (decided by the optimizer)
      ){};
      ~GHJoin(){};

      RC getNextTuple(void *data){return QE_EOF;};
      // For attribute in vector<Attribute>, name it as rel.attr
      void getAttributes(vector<Attribute> &attrs) const{};
};

class Aggregate : public Iterator {
    // Aggregation operator
    public:
        // Mandatory for graduate teams only
        // Basic aggregation
        Aggregate(Iterator *input,          // Iterator of input R
                  Attribute aggAttr,        // The attribute over which we are computing an aggregate
                  AggregateOp op            // Aggregate operation
        ){};

        // Optional for everyone. 5 extra-credit points
        // Group-based hash aggregation
        Aggregate(Iterator *input,             // Iterator of input R
                  Attribute aggAttr,           // The attribute over which we are computing an aggregate
                  Attribute groupAttr,         // The attribute over which we are grouping the tuples
                  AggregateOp op              // Aggregate operation
        ){};
        ~Aggregate(){};

        RC getNextTuple(void *data){return QE_EOF;};
        // Please name the output attribute as aggregateOp(aggAttr)
        // E.g. Relation=rel, attribute=attr, aggregateOp=MAX
        // output attrname = "MAX(rel.attr)"
        void getAttributes(vector<Attribute> &attrs) const{};
};

inline int getBufferSize(vector<Attribute>& attrs) {
    // Start with buffer size = # null bytes
    int buffer_size = attrs.size()/8 + 1;
    // Increase buffer size by length of each attribute
    for (auto it = attrs.begin(); it != attrs.end(); it++) {
        buffer_size += it->length;
        buffer_size += 4; // 4 bytes for each attribute metadata
    }
    return buffer_size;
}

inline void printAttributes(vector<Attribute>& attrs) {
    std::cout << "attrs: ";
    for (auto it = attrs.begin(); it != attrs.end(); it++) {
        std::cout << it->name << " ";
    }
    std::cout << std::endl;
}

inline int getAttributeIndex(vector<Attribute>& attrs, string name) {
    for (int i = 0; i < attrs.size(); i++) {
        if (name == attrs[i].name) {
            return i;
        }
    }
    cout << "[getAttributeIndex] Attribute Index Not Found" << endl;
    return -1;
}

inline int findAttributeOffset(int null_byte_count, int attr_index, vector<Attribute>& attrs, void* tuple) {
    int offset = null_byte_count; // add null bytes to offset value

    char* c = (char*)tuple + null_byte_count; // ptr to beginning of actual data

    for (int i = 0; i < attr_index; i++) {
        switch (attrs[i].type) {
            case TypeInt: case TypeReal: {
                offset += 4;
                c += 4;
                break;
            }
            case TypeVarChar: {
                int varchar_length = *(int*)c;
                c += (4 + varchar_length);
                offset += (4 + varchar_length);
                break;
            }
        }
    }

    return offset;

}

inline int getNullBit(void* tuple, int attr_index) {
    // Null byte information for tuple
    int null_byte_number = attr_index/8; // which null byte
    int null_bit_number = attr_index%8;  // which bit in the null byte

    return getBit(*((char*)tuple + null_byte_number), null_bit_number);
}


inline int findTupleSize(int null_byte_count, vector<Attribute>& attrs, void* tuple) {
    int size = null_byte_count; // add null bytes to size value

    char* c = (char*)tuple + null_byte_count; // ptr to beginning of actual data

    for (int i = 0; i < attrs.size(); i++) {
        switch (attrs[i].type) {
            case TypeInt: case TypeReal: {
                size += 4;
                c += 4;
                break;
            }
            case TypeVarChar: {
                int varchar_length = *(int*)c;
                c += (4 + varchar_length);
                size += (4 + varchar_length);
                break;
            }
        }
    }

    return size;

}

inline int findTupleSizeWithoutNullBytes(int null_byte_count, vector<Attribute>& attrs, void* tuple) {
    int size = 0;

    char* c = (char*)tuple + null_byte_count; // ptr to beginning of actual data

    for (int i = 0; i < attrs.size(); i++) {
        switch (attrs[i].type) {
            case TypeInt: case TypeReal: {
                size += 4;
                c += 4;
                break;
            }
            case TypeVarChar: {
                int varchar_length = *(int*)c;
                c += (4 + varchar_length);
                size += (4 + varchar_length);
                break;
            }
        }
    }

    return size;

}


#endif
