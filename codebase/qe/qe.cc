#include "qe.h"

// ---------------- FILTER ---------------- //

Filter::Filter(Iterator* input, const Condition &condition) 
	: iter(input), cond(condition)
{
	iter->getAttributes(iter_attrs);
	buffer_size = getBufferSize(iter_attrs);
}

RC Filter::getNextTuple(void* data) {
	// Get tuple from input iterator
	void* tuple = malloc(buffer_size);
	RC rc = iter->getNextTuple(tuple);
	if (rc != 0) {
		free(tuple);
		return QE_EOF;
	}

	RC filter_rc = applyFilter(tuple, iter_attrs);

	// Passes filter condition
	if (filter_rc == successful) {
		memcpy(data, tuple, buffer_size);
		free(tuple);
		return 0;
	}
	// Fails filter condition
	else if (filter_rc == error) {
		free(tuple);
		return getNextTuple(data);
	}


}

RC Filter::applyFilter(void* tuple, vector<Attribute>& attrs) {
	int null_byte_count = div_ceil(attrs.size(), 8); // # of null bytes

	char* beginning = (char*)tuple;
	char* seek = beginning + null_byte_count; // seek past null bytes

	// Seek to target LHS attribute
	for (auto it = attrs.begin(); it != attrs.end(); it++) {
		// If we found target LHS attribute, stop seeking
		if (it->name == cond.lhsAttr) {
			break;
		}
		else {
			// Skip attribute
			switch (it->type) {
				case TypeInt: case TypeReal: {
					seek += 4;
					break;
				}
				case TypeVarChar: {
					seek += (4 + it->length);
					break;
				}
			}
		}
	}

	// Check if the tuple passes filter condition
	bool pass_filter;
	switch(cond.rhsValue.type) {
		case TypeInt: {
			int value = *(int*)seek;
			pass_filter = intPassesCondition(value);
			break;
		}
		case TypeReal: {
			float value = *(float*)seek;
			pass_filter = floatPassesCondition(value);
			break;
		}
		case TypeVarChar: {
			int varchar_length = *(int*)seek;
			char* str = seek + 4;
			pass_filter = varcharPassesCondition(str, varchar_length);
			break;
		}
		default: {
			std::cout << "read attribute type unrecognized" << std::endl;
			return error;
		}
	}
	
	if (pass_filter)
		return successful;
	else
		return error;



}

bool Filter::intPassesCondition(int value) {
	int comp_val = *(int*)cond.rhsValue.data;

	switch(cond.op) {
		case EQ_OP: {
			return value == comp_val;
		}
		case LT_OP: {
			return value < comp_val;
		}
		case GT_OP: {
			return value > comp_val;
		}
		case LE_OP: {
			return value <= comp_val;
		}
		case GE_OP: {
			return value >= comp_val;
		}
		case NE_OP: {
			return value != comp_val;
		}
		default: {
			return false;
		}
	}
}

bool Filter::floatPassesCondition(float value) {
	float comp_val = *(float*)cond.rhsValue.data;
	
	switch(cond.op) {
		case EQ_OP: {
			return value == comp_val;
		}
		case LT_OP: {
			return value < comp_val;
		}
		case GT_OP: {
			return value > comp_val;
		}
		case LE_OP: {
			return value <= comp_val;
		}
		case GE_OP: {
			return value >= comp_val;
		}
		case NE_OP: {
			return value != comp_val;
		}
		default: {
			return false;
		}
	}
}

// bool Filter::varcharPassesCondition(char* str, int length) {
// 	char* cval = (char*)cond.rhsValue.data;
// 	char cstr[length + 1];
// 	memset(cstr, 0, sizeof(cstr));	// Zero out the junk (put a null at the end);
// 	strncpy(cstr, str, length);

// 	switch(cond.op) {
// 		case EQ_OP: { return strcmp(cstr, cval) == 0; }
// 		case NE_OP: { return strcmp(cstr, cval) != 0; }
// 		case LT_OP: { return strcmp(cstr, cval) < 0;  }
// 		case GT_OP: { return strcmp(cstr, cval) > 0;  }
// 		case LE_OP: { return strcmp(cstr, cval) <= 0; }
// 		case GE_OP: { return strcmp(cstr, cval) >= 0; }
// 		default: {
// 			return false;
// 		}
// 	}
// }

bool Filter::varcharPassesCondition(char* str, int length) {
	std::string tuple_val(str, length);
	std::string comp_val((char*)cond.rhsValue.data + 4, *(int*)cond.rhsValue.data);

	switch(cond.op) {
		case EQ_OP: { return tuple_val == comp_val; }
		case NE_OP: { return tuple_val != comp_val; }
		case LT_OP: { return tuple_val < comp_val; }
		case GT_OP: { return tuple_val > comp_val; }
		case LE_OP: { return tuple_val <= comp_val; }
		case GE_OP: { return tuple_val >= comp_val; }
		default: {
			return false;
		}
	}
}


// ---------------- PROJECT ---------------- //
Project::Project(Iterator *input, const vector<string> &attrNames) 
	: iter(input), attr_names(attrNames)
{
	iter->getAttributes(iter_attrs);
	buffer_size = getBufferSize(iter_attrs);
}   

RC Project::getNextTuple(void *data) {
	// Get tuple from input iterator
	void* tuple = malloc(buffer_size);
	RC rc = iter->getNextTuple(tuple);
	if (rc != 0) {
		free(tuple);
		return QE_EOF;
	}

	applyProjection(data, tuple, iter_attrs);

	return successful;
};

void Project::applyProjection(void* data, void* tuple, vector<Attribute>& attrs) {
	int null_byte_count = div_ceil(attr_names.size(), 8); // # of null bytes needed for projection
	memset(data, 0, null_byte_count); // set the beginning of projection to null bytes

	char* data_location = (char*)data + null_byte_count; 	// current position in output data

	for (int i = 0; i < attr_names.size(); i++) {
		int attr_index = getAttributeIndex(attrs, attr_names[i]);
		if (attr_index == -1) {
			std::cout << "Error in getAttributeIndex: no attribute found" << std::endl;
			return;
		}

		int nullbit = getNullBit(tuple, attr_index);

		// Null byte information for output data
		int output_null_byte_number = i/8;
		int output_null_bit_number = i*8;

		// If data in tuple was null, set the nullbit in output data
		if (nullbit) {
			setBit(*((char*)data + output_null_byte_number), output_null_bit_number);
		}

		// Find offset in tuple for this attribute
		int offset = findAttributeOffset(null_byte_count, attr_index, attrs, tuple);
		// Copy tuple data for attribute into next output data location
		copyAttributeToOutput(tuple, data_location, offset, attrs[attr_index]);
	}
}


// For attribute in vector<Attribute>, name it as rel.attr
void Project::getAttributes(vector<Attribute> &attrs) const {
	// Erase all attributes that are not projected attributes
	for (auto it = attrs.begin(); it != attrs.end();) {
		if (isProjectedAttribute(it->name)) {
			attrs.erase(it++);
		}
		else {
			++it;
		}
	}
	// string prefix = iter->tableName + ".";
	// for (auto it = attr_names.begin(); it != attr_names.end(); it++) {
	// 	attrs.push_back()
	// }
}


void Project::copyAttributeToOutput(void* tuple, char*& data_location, int offset, Attribute a) {
	char* input = (char*)tuple + offset;
	switch (a.type) {
		case TypeInt: case TypeReal: {
			memcpy(data_location, input, 4);
			data_location += 4;
			break;
		}
		case TypeVarChar: {
			int varchar_length = *(int*)input;
			memcpy(data_location, input, 4 + varchar_length);
			data_location += (4 + varchar_length);
			break;
		}
	}
}


bool Project::isProjectedAttribute(string name) const {
	for (int i = 0; i < attr_names.size(); i++) {
		if (attr_names[i] == name) {
			return true;
		}
	}

	return false;
}



// ---------------- BLOCK NESTED LOOP JOIN ---------------- //
BNLJoin::BNLJoin(Iterator *leftIn, TableScan *rightIn, const Condition &condition, const unsigned numRecords)
	: left(leftIn), right(rightIn), cond(condition), num_records(numRecords), block_pos(0)
{
	left->getAttributes(left_attrs);
	left_buffer_size = getBufferSize(left_attrs);	
	right->getAttributes(right_attrs);
	right_buffer_size = getBufferSize(right_attrs);

	readNextBlock();
}

RC BNLJoin::getNextTuple(void *data) {
	void* right_tuple = malloc(right_buffer_size);
	RC rc = right->getNextTuple(right_tuple);
	// If no more tuples on the right iterator,
	// read in new block from left and reset right
	// Then recurse
	if (rc != 0) {
		readNextBlock();
		if (block.empty())
			return QE_EOF;
		right->setIterator();
		return getNextTuple(data);
	}
	else {
		if (matchesCondition(block[block_pos], right_tuple)) {
			concatenateTuples(block[block_pos], right_tuple, data);
			moveBlockPosition();
			return successful;
		}
		else {
			moveBlockPosition();
			return getNextTuple(data);
		}
	}
}

// For attribute in vector<Attribute>, name it as rel.attr
void BNLJoin::getAttributes(vector<Attribute> &attrs) const {
	attrs.clear();
	for (auto it = left_attrs.begin(); it != left_attrs.end(); it++) {
		attrs.push_back(*it);
	}
	for (auto it = right_attrs.begin(); it != right_attrs.end(); it++) {
		attrs.push_back(*it);
	}
}

void BNLJoin::readNextBlock() {
	block.clear();
	for (int i = 0; i < num_records; i++) {
		// Get tuple from left iterator
		void* tuple = malloc(left_buffer_size);
		RC rc = left->getNextTuple(tuple);
		if (rc != 0) {
			return;
		}

		// Add tuple to block
		block.push_back(tuple);
	}
}

void BNLJoin::moveBlockPosition() {
	block_pos++;
	// If we moved out of range of the block, reset back to beginning of block
	if (block_pos >= block.size()) {
		block_pos = 0;
	}
}

bool BNLJoin::matchesCondition(void* left_tuple, void* right_tuple) {
	// Number of null bytes in left and right tuples
	int left_null_byte_count = div_ceil(left_attrs.size(), 8); 
	int right_null_byte_count = div_ceil(right_attrs.size(), 8);

	// Index of condition attribute in left and right tuples
	int left_attr_index = getAttributeIndex(left_attrs, cond.lhsAttr);
	int right_attr_index = getAttributeIndex(right_attrs, cond.rhsAttr);

	// Offsets for condition attribute in left and right tuples
	int left_offset = findAttributeOffset(left_null_byte_count, left_attr_index, left_attrs, left_tuple);
	int right_offset = findAttributeOffset(right_null_byte_count, right_attr_index, right_attrs, right_tuple);

	// The nullbits for the condition attribute in left and right tuples
	int left_nullbit = getNullBit(left_tuple, left_attr_index);
	int right_nullbit = getNullBit(right_tuple, right_attr_index);

	// Check if left and right conditions match
	if (left_attrs[left_attr_index].type != right_attrs[right_attr_index].type) {
		cout << "Left attribute type does not match right attribute type" << std::endl;
		return false;
	}

	// Check if either left or right tuple is null
	if (left_nullbit && right_nullbit)
		return true;
	else if (left_nullbit)
		return false;
	else if (right_nullbit)
		return false;


	char* left_c = (char*)left_tuple + left_offset;
	char* right_c = (char*)right_tuple + right_offset;
	// Check for equality between both left and right tuple values
	// Assumes CompOp is EQ_OP
	switch(left_attrs[left_attr_index].type) {
		case TypeInt: {
			return *(int*)left_c == *(int*)right_c;
		}
		case TypeReal: {
			return *(float*)left_c == *(float*)right_c;
		}
		case TypeVarChar: {
			int left_varchar_length = *(int*)left_c;
			int right_varchar_length = *(int*)right_c;
			string left_varchar(left_c+4, left_varchar_length);
			string right_varchar(right_c+4, right_varchar_length);
			return left_varchar == right_varchar;
		}
	}
}

void BNLJoin::concatenateTuples(void* left_tuple, void* right_tuple, void* data) {
	int null_byte_count = div_ceil(left_attrs.size() + right_attrs.size(), 8);
	memset(data, 0, null_byte_count); // set null bytes for left + right attributes

	// Number of null bytes in left and right tuples
	int left_null_byte_count = div_ceil(left_attrs.size(), 8); 
	int right_null_byte_count = div_ceil(right_attrs.size(), 8);

	// Total size of left and right tuples
	int left_tuple_size = findTupleSizeWithoutNullBytes(left_null_byte_count, left_attrs, left_tuple);
	int right_tuple_size = findTupleSizeWithoutNullBytes(right_null_byte_count, right_attrs, right_tuple);

	char* output = (char*)data + null_byte_count; 				
	char* left_seek = (char*)left_tuple + left_null_byte_count;
	char* right_seek = (char*)right_tuple + right_null_byte_count;


	memcpy(output, left_seek, left_tuple_size);
	output += left_tuple_size;
	memcpy(output, right_seek, right_tuple_size);
}




// INLJOIN
INLJoin::INLJoin(Iterator *leftIn,           // Iterator of input R
                IndexScan *rightIn,          // IndexScan Iterator of input S
                const Condition &condition   // Join condition
    ) : leftIter(leftIn), rightIXIter(rightIn), cond(condition) {

    leftIter->getAttributes(leftAttrs);
    rightIXIter->getAttributes(rightAttrs);
    combinedAttrs.insert(combinedAttrs.end(), leftAttrs.begin(), leftAttrs.end());
    combinedAttrs.insert(combinedAttrs.end(), rightAttrs.begin(), rightAttrs.end());

    left = (byte*) calloc(getBufferSize(leftAttrs), sizeof(byte));
    right = (byte*) calloc(getBufferSize(rightAttrs), sizeof(byte));

//    for (unsigned i = 0; i < combinedAttrs.size(); i++) {
//        cout << combinedAttrs.at(i).name << endl;
//    }

    int rc = leftIter->getNextTuple(left);  // Load left side
}

INLJoin::~INLJoin() {
    free(left);
    free(right);
}

RC INLJoin::getNextTuple(void *data) {
    int rc = rightIXIter->getNextTuple(right);

    if (rc == QE_EOF) {                                     // If reached end, load next Left tuple
        int lc = leftIter->getNextTuple(left);
        if (lc == QE_EOF) return QE_EOF;

        rightIXIter->setIterator(NULL, NULL, false, false); // Reset right iterator to begin.
        return getNextTuple(data);
    }
    // Found some pairs
    // Now find if they match on attributes

    if (matchCondition(left, right)) {
        concatenateTuples(left, right, data);
        return SUCCESS;
    }
    return getNextTuple(data);
}

// For attribute in vector<Attribute>, name it as rel.attr
void INLJoin::getAttributes(vector<Attribute> &attrs) const {
    attrs = this->combinedAttrs;
}

inline void* getFieldValueQE(const string &attributeName, const vector<Attribute> &attrs, const void *data) {
    // Given tuple data and attributes, extract value (project) attributeName
    int offset = div_ceil(attrs.size(), 8); // number of null bytes is offset

    for (unsigned i = 0; i < attrs.size(); i++) {
        Attribute attr = attrs[i];

        // Go to next if attribute is NULL
        int nullbit = getBit(((char*)data)[i / 8], i % 8);
        if (nullbit == 1) {
            if (attr.name == attributeName) return NULL;
            continue;
        }

        if (attr.type == TypeInt) {
            int n;
            memcpy(&n, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);

//            cout << attr.name << " : " << n << "    ";
            if (attr.name == attributeName) {
                int* nx = new int;
                *nx = n;
                return (void*)nx;
            }

        } else if (attr.type == TypeReal) {
            float f;
            memcpy(&f, (byte *) data + offset, sizeof(float));
            offset += sizeof(float);

//            cout << attr.name << " : " << f << "    ";
            if (attr.name == attributeName) {
                float* fx = new float;
                *fx = f;
                return (void*)fx;
            }

        } else if (attr.type == TypeVarChar) {
            int nameLen;
            memcpy(&nameLen, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            byte *buffer = (byte *) calloc(nameLen + 1, sizeof(byte));
            memcpy(buffer, (byte *) data + offset, nameLen);
            offset += nameLen;

//            cout << attr.name << " : " << buffer << "    ";
            if (attr.name == attributeName) {
                byte* wBuffer = (byte*) calloc(4 + nameLen + 1, sizeof(byte));
                memcpy(wBuffer, &nameLen, sizeof(int));
                memcpy(wBuffer + 4, buffer, nameLen);
                return wBuffer; // This varcharLen and varchar recombined to use as key.
            }
            free(buffer);
            buffer = NULL;
        }
    }
    return NULL;
}

bool INLJoin::matchCondition(void* leftTuple, void* rightTuple) {
//    void* lV = getFieldValueQE(cond.lhsAttr, leftAttrs, leftTuple);
//    void* rV = getFieldValueQE(cond.rhsAttr, rightAttrs, rightTuple);
//
//    // Need to get type of the attribute.
//    int aI = getAttributeIndex(leftAttrs, cond.lhsAttr);
////    cout << "A vector Index: " << aI << endl;
//
//    if (aI == -1) return false;
//    Attribute a = leftAttrs.at(aI);
//
//
//    int bI = getAttributeIndex(rightAttrs, cond.rhsAttr);
//    if (bI == -1) return false;
////    cout << "B vector Index: " << bI << endl;
//    Attribute b = rightAttrs.at(bI);
//
//    if (a.type == b.type) {
//        if (a.type == TypeInt) {
//            return *(int*)lV == *(int*)rV;
//        } else if (a.type == TypeReal) {
//            return *(float*)lV == *(float*)rV;
//        } else if (a.type == TypeVarChar) {
//            int left_varchar_length = *(int*)lV;
//            int right_varchar_length = *(int*)rV;
//            string left_varchar((char*)lV+4, left_varchar_length);
//            string right_varchar((char*)rV+4, right_varchar_length);
//            return left_varchar == right_varchar;
//        }
//
//    } else {
//        cout << "Type mismatch  on INLJoin" << endl;
//    }
//    return false;

    // Number of null bytes in left and right tuples
    int left_null_byte_count = div_ceil(leftAttrs.size(), 8);
    int right_null_byte_count = div_ceil(rightAttrs.size(), 8);

    // Index of condition attribute in left and right tuples
    int left_attr_index = getAttributeIndex(leftAttrs, cond.lhsAttr);
    int right_attr_index = getAttributeIndex(rightAttrs, cond.rhsAttr);

    if (left_attr_index == -1 || right_attr_index == -1) return false;

    // Offsets for condition attribute in left and right tuples
    int left_offset = findAttributeOffset(left_null_byte_count, left_attr_index, leftAttrs, leftTuple);
    int right_offset = findAttributeOffset(right_null_byte_count, right_attr_index, rightAttrs, rightTuple);

    // The nullbits for the condition attribute in left and right tuples
    int left_nullbit = getNullBit(leftTuple, left_attr_index);
    int right_nullbit = getNullBit(rightTuple, right_attr_index);

    // Check if left and right conditions match
    if (leftAttrs[left_attr_index].type != rightAttrs[right_attr_index].type) {
        cout << "Left attribute type does not match right attribute type" << std::endl;
        return false;
    }

    // Check if either left or right tuple is null
    if (left_nullbit && right_nullbit)
        return true;
    else if (left_nullbit)
        return false;
    else if (right_nullbit)
        return false;


    char* left_c = (char*)leftTuple + left_offset;
    char* right_c = (char*)rightTuple + right_offset;
    // Check for equality between both left and right tuple values
    // Assumes CompOp is EQ_OP
    switch(leftAttrs[left_attr_index].type) {
        case TypeInt: {
            return *(int*)left_c == *(int*)right_c;
        }
        case TypeReal: {
            return *(float*)left_c == *(float*)right_c;
        }
        case TypeVarChar: {
            int left_varchar_length = *(int*)left_c;
            int right_varchar_length = *(int*)right_c;
            string left_varchar(left_c+4, left_varchar_length);
            string right_varchar(right_c+4, right_varchar_length);
            return left_varchar == right_varchar;
        }
    }
}

void INLJoin::concatenateTuples(void* leftTuple, void* rightTuple, void* data) {
    int null_byte_count = div_ceil(leftAttrs.size() + rightAttrs.size(), 8);
    memset(data, 0, null_byte_count); // set null bytes for left + right attributes

    // Number of null bytes in left and right tuples
    int left_null_byte_count = div_ceil(leftAttrs.size(), 8);
    int right_null_byte_count = div_ceil(rightAttrs.size(), 8);

    // Total size of left and right tuples
    int left_tuple_size = findTupleSizeWithoutNullBytes(left_null_byte_count, leftAttrs, leftTuple);
    int right_tuple_size = findTupleSizeWithoutNullBytes(right_null_byte_count, rightAttrs, rightTuple);

    char* output = (char*)data + null_byte_count;
    char* left_seek = (char*)leftTuple + left_null_byte_count;
    char* right_seek = (char*)rightTuple + right_null_byte_count;

    memcpy(output, left_seek, left_tuple_size);
    output += left_tuple_size;
    memcpy(output, right_seek, right_tuple_size);
}
