#include "page.h"

Page::Page(unsigned page_id) {
	pid = page_id;

	// Initialize empty page metadata
	slot_count = 0;
	free_space_begin = 0;
	free_space_end = PAGE_SIZE - sizeof(slot_count) - sizeof(free_space_begin);
	free_space_amount = free_space_end - free_space_begin;
}

Page::Page(const Page& page) {
	pid = page.pid;
	slot_count = page.slot_count;
	free_space_amount = page.free_space_amount;
	free_space_begin = page.free_space_begin;
	free_space_end = page.free_space_end;
	record_descriptor = page.record_descriptor;

	// Copy slot data
	for (auto it = page.slots.begin(); it != page.slots.end(); it++) {
		slot s;
		s.offset = it->offset;
		s.length = it->length;
		slots.push_back(s);
	}

	// Copy records
	for (auto it = page.records.begin(); it != page.records.end(); it++) {
		records[it->first] = it->second;
	}
}

Page::~Page() {
	for (auto it = records.begin(); it != records.end(); it++) {
		delete[] it->second;
	}
}

// Start from the very end of the page, and read backwards
void Page::loadPage(char* data) {
	char* c = data;

	// Store free space offset
	c += PAGE_SIZE - sizeof(free_space_begin);
	free_space_begin = *(unsigned*)c;

	// Store slot count
	c -= sizeof(slot_count);
	slot_count = *(unsigned*)c;

	unsigned total_record_size = 0; // find out how much space all the records take up
	// Read in all slot data
	for (unsigned i = 0; i < slot_count; i++) {
		slot s;
		c -= sizeof(unsigned);
		s.length = *(unsigned*)c;

		c -= sizeof(unsigned);
		s.offset = *(unsigned*)c;

		slots.push_back(s);

		if (s.length > 0) { // skip tombstones
			total_record_size += s.length;
		}
	}

	// Update free space
	free_space_end = PAGE_SIZE - sizeof(free_space_begin) - sizeof(slot_count) - (2*sizeof(unsigned)*slots.size()) - 1;
	free_space_begin = total_record_size;
	free_space_amount = free_space_end - free_space_begin;

	// Store record data
	c = data;
	int slot_size = slots.size();
	for (int i = 0; i < slot_size; i++) {
		slot s = slots[i];

		if (s.length > 0) { // ignore tombstones
			records[i] = new char[s.length];
			memcpy(records[i], (void*)c, s.length);
			c += s.length;
		}
	}
}

void Page::setPID(unsigned new_id) {
	pid = new_id;
}

unsigned Page::getPID() {
	return pid;
}

slot Page::getSlot(int slot_id) {
	return slots[slot_id];
}

unsigned Page::getSlotCount() {
	return slots.size();
}

unsigned Page::getFreeSpace() {
	return free_space_amount;
}

RID Page::getLinkingRID(const RID& rid) {
	RID linking_rid;
	linking_rid.pageNum = abs(slots[rid.slotNum].offset);
	linking_rid.slotNum = abs(slots[rid.slotNum].length)-1;

	return linking_rid;
}

void Page::insertRecord(const vector<Attribute> &recordDescriptor, const void *data, unsigned record_size, RID& rid, void* attr_metadata) {
	// Save record descriptor
	record_descriptor = recordDescriptor;

	int attr_metadata_length = recordDescriptor.size() * 4;		   		// length of attribute metadata (one int per attribute)
	int attr_metadata_offset = record_size - attr_metadata_length; 		// offset where attribute metadata begins

	// Add slot
	slot s;
	s.offset = free_space_begin;
	s.length = record_size;
	slots.push_back(s);

	// save record data
	unsigned slot_id = slots.size() - 1;
	char* rec = new char[record_size];
	char* attr_metadata_begin = rec + attr_metadata_offset;
	memcpy((void*)rec, data, attr_metadata_offset); // copy null bytes and actual data
	memcpy((void*)attr_metadata_begin, attr_metadata, attr_metadata_length); // copy attribute metadata
	records[slot_id] = rec;

	// Update metadata
	free_space_begin += record_size;
	free_space_end -= sizeof(slot);
	free_space_amount = free_space_end - free_space_begin;
	slot_count++;

	// update record id
	rid.pageNum = pid;
	rid.slotNum = slot_id;
}

RC Page::deleteRecord(const RID& rid) {
	RC valid = checkValidSlot(rid.slotNum);
	if (valid != 0) {
		return valid;
	}

	delete[] records[rid.slotNum]; // free memory
	records.erase(rid.slotNum);	 // remove record from map

	unsigned record_length = slots[rid.slotNum].length; // record length

	// set tombstone
	slots[rid.slotNum].offset = -1;
	slots[rid.slotNum].length = 0;

	// Update metadata
	free_space_begin -= record_length;
	free_space_amount = free_space_end - free_space_begin;

	// shift slot offsets left according to length of deleted record
	shiftSlotOffsets(-(record_length), rid.slotNum + 1);

	return 0;
}

RC Page::updateRecord(const vector<Attribute> &recordDescriptor, const RID& rid, const void *data, unsigned record_size, void* attr_metadata) {
	delete[] records[rid.slotNum]; // free memory

	int attr_metadata_length = recordDescriptor.size() * 4;		   		// length of attribute metadata (one int per attribute)
	int attr_metadata_offset = record_size - attr_metadata_length; 		// offset where attribute metadata begins
	
	char* rec = new char[record_size];
	char* attr_metadata_begin = rec + attr_metadata_offset;
	memcpy((void*)rec, data, attr_metadata_offset); 						 // copy null bytes and actual data
	memcpy((void*)attr_metadata_begin, attr_metadata, attr_metadata_length); // copy attribute metadata

	// update records
	records[rid.slotNum] = rec;

	unsigned old_length = slots[rid.slotNum].length; 	// length before update
	slots[rid.slotNum].length = record_size;			// update changed slot
	int length_difference = record_size - old_length;   // change in record length

	// Update metadata
	free_space_begin += length_difference;
	free_space_amount = free_space_end - free_space_begin;

	// shift slot offsets left or right according to length of update
	shiftSlotOffsets(length_difference, rid.slotNum + 1);

	return 0;
}

RC Page::updateRecordByMoving(RID current_rid, RID move_rid) {
	deleteRecord(current_rid);
	// Set new tombstone
	slots[current_rid.slotNum].offset = -(move_rid.pageNum); // negative offset to indicate PID of new location
	slots[current_rid.slotNum].length = -(move_rid.slotNum+1); // negative length to indicate slot id of new location (starting at 1 because length 0 means deletion)
	return 0;
}

void Page::shiftSlotOffsets(int length, int starting_index) {
	int size = slots.size();

	// For every slot from starting index to end,
	// if it's not a tombstone,
	// shift slot offset according to the length
	// Negative length means shift left; positive means right
	for (int i = starting_index; i < size; i++) {
		if (slots[i].length > 0) {
			slots[i].offset += length;
		}
	}
}

bool Page::pageFull() {
	return (free_space_amount <= sizeof(slot)); // page is full if page cannot hold at least a slot
}

bool Page::hasSpace(unsigned record_size) {
	return free_space_amount > (record_size + sizeof(slot));
}

bool Page::hasSpaceForUpdate(unsigned new_size, unsigned slot_id) {
	int size_difference = new_size - slots[slot_id].length;
	return free_space_amount >= size_difference;
}

RC Page::checkValidSlot(unsigned slot_id) {
	if (slot_id >= slots.size()) { // if slot id out of range
		std::cout << "Slot id out of range. Record does not exist on this page." << std::endl;
		return -1;
	}
	else if (slots[slot_id].length == 0) { // if record already been deleted
		// std::cout << "The record you requested is deleted." << std::endl;
		// printDebug();
		return -1;
	}
	else if (slots[slot_id].length < 0 && slots[slot_id].offset < 0) { // if record exists on another page
		return 1; // signal that record exists on another page
	}
	else {
		return 0;
	}
}

void Page::flush(char* data) {
	char* c = data;

	// flush record data
	int slots_size = slots.size();
	for (int i = 0; i < slots_size; i++) {
		slot s = slots[i];
		if (s.length > 0) { // if slot points to actual data
			char* record = records[i];
			memcpy((void*)c, (void*)record, s.length);
			c += s.length;
		}

	}

	// seek to end of page
	// going backwards, write free space and record count metadata
	c = data + PAGE_SIZE - 4;
	*(unsigned*)c = free_space_begin;
	c -= sizeof(free_space_begin);
	*(unsigned*)c = slot_count;

	// write the slot data
	for (auto it = slots.begin(); it != slots.end(); it++) {
		slot s = *it;
		c -= sizeof(s.length);
		*(unsigned*)c = s.length;
		c -= sizeof(s.offset);
		*(unsigned*)c = s.offset;
	}
}

RC Page::readRecord(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor) {
	RC valid = checkValidSlot(slot_id);
	if (valid != 0) {
		return valid;
	}

	char* c = records[slot_id];
	int attr_metadata_length = recordDescriptor.size() * 4; // total length for attribute metadata at the end of record
	memcpy(data, (void*) c, slots[slot_id].length - attr_metadata_length); // copy only actual record data

	return 0;
}

RC Page::readRecordWithAttrData(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor) {
	RC valid = checkValidSlot(slot_id);
	if (valid != 0) {
		return valid;
	}

	char* c = records[slot_id];
	memcpy(data, (void*) c, slots[slot_id].length); // copy all record data, including attr metadata

	return 0;
}

RC Page::readAttribute(const string &attributeName, const RID& rid, void* data, const vector<Attribute> &recordDescriptor) {
	RC valid = checkValidSlot(rid.slotNum);
	if (valid != 0) {
		return valid;
	}

	int attr_count = recordDescriptor.size();

	// Find index of attribute
	int attr_index = -1;
	AttrType attr_type;
	for (int i = 0; i < attr_count; i++) {
		if (recordDescriptor[i].name == attributeName) {
			attr_index = i;
			attr_type = recordDescriptor[i].type;
			break;
		}
	}

	if (attr_index == -1) {
		std::cout << "attribute not in record descriptor" << std::endl;
		return -1;
	}

	char* rec = records[rid.slotNum];	// pointer to record
	int rec_length = slots[rid.slotNum].length;

	char* attr_metadata = rec + rec_length - (attr_count * 4) + (attr_index * 4); // seek to desired attr metadata to find offset
	int attr_offset = *(int*)attr_metadata; // offset of attribute in record

	if (attr_offset == -1) { // -1 offset means field is null
		char null_byte = (1 << (7-attr_offset)); // mark null bit corresponding to attribute
		memset(data, null_byte, 1); // copy 
		return 0;
	}
	else {
		char* c = rec + attr_offset;
		memset(data, 0, 1); // set null byte to 0 (since attribute is not null)
		char* after_null_byte = (char*)data + 1;

		switch(attr_type) {
			case TypeInt: case TypeReal: {
				memcpy(after_null_byte, (void*)c, 4);	// copy the int or float into data block after nullbyte
				break;
			}
			case TypeVarChar: {
				int varchar_length = *(int*)c;
				memcpy(after_null_byte, (void*)c, varchar_length + 4); // copy the varchar's size along with the actual string
				break;
			}
			default: {
				std::cout << "read attribute type unrecognized" << std::endl;
				return -1;
			}
		}
	}

	return 0;
	// // Find out if attribute is null
	// unsigned null_byte_count = div_ceil(attr_count, 8); // # of null bytes to represent data
	// char* null_byte_ptr = new char[null_byte_count]; 	// pointer to array of null bytes
	// memcpy((void*)null_byte_ptr, rec, null_byte_count); // store null byte data
	// bool nullattr = null_byte_ptr[attr_index/8] & (1 << (7-(attr_index%8))); // is attr null?
	// delete null_byte_ptr;
}

void Page::printDebug() {
	std::cout << "// ---- Debug info for page #" << pid << " ----- //" << std::endl;
	std::cout << "\tSlot count: " << slot_count << std::endl;
	std::cout << "\tFree space amount: " << free_space_amount << std::endl;
	std::cout << "\tFree space begin: " << free_space_begin << std::endl;
	std::cout << "\tFree space end: " << free_space_end << std::endl;
	for (auto it = slots.begin(); it != slots.end(); it++) {
		slot ps = *it;
		std::cout << "\t\tslot offset: " << ps.offset << ", slot length: " << ps.length << std::endl;
	}


}
