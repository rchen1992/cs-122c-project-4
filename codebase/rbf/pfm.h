#ifndef _pfm_h_
#define _pfm_h_

#include <string>
#include <climits>
#include <iostream>
#include <fstream>
#include "directory.h"
#include "types.h"
#include "page.h"
#include "readpage.h"

using namespace std;

class FileHandle;


class PagedFileManager
{
public:
    static PagedFileManager* instance();                     // Access to the _pf_manager instance

    RC createFile    (const string &fileName);                         // Create a new file
    RC destroyFile   (const string &fileName);                         // Destroy a file
    RC openFile      (const string &fileName, FileHandle &fileHandle); // Open a file
    RC closeFile     (FileHandle &fileHandle);                         // Close a file

protected:
    PagedFileManager();                                   // Constructor
    ~PagedFileManager();                                  // Destructor

private:
    static PagedFileManager *_pf_manager;
};


class FileHandle
{
public:
    // variables to keep counter for each operation
	unsigned readPageCounter;
	unsigned writePageCounter;
	unsigned appendPageCounter;

	std::fstream* file;						// pointer to file stream
	unsigned pageCount;						// number of pages in file (including directories)
	std::vector<Directory> directories;		// vector of all directories
	Page* current_page;						// last page in file
	
    FileHandle();                                                    // Default constructor
    ~FileHandle();                                                   // Destructor

    RC readPage(PageNum pageNum, void *data);                           // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                    // Write a specific page
    RC appendPage(const void *data);                                    // Append a specific page
    unsigned getNumberOfPages();                                        // Get the number of pages in the file
    RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);  // put the current counter values into variables

    // File setup
    RC attachFile(std::fstream* file); 							// Set up file handle with file stream
    void loadFileDirectory(unsigned pid); 						// Recursively loads all directories into file handle
    void loadCurrentPage();										// Loads last page in file as current page; if no page exists, do nothing

    // Page actions
    Page* getPage(unsigned pid); 								// Gets page from disk, unless desired page is current_page (which simply returns that page)
    ReadPage* getReadPage(unsigned pid);                        // Gets a page that is optimized for reads
    Page* newPage();										   	// Appends new page to end of file, updates directory, sets new page as current page

    // Directory actions
    void newDirectory(); 				  						// append new directory page to disk and directory vector
    int nextAvailablePID(unsigned record_size);                 // returns PID of next available page in all directories; -1 if not found
    Page* nextAvailablePage(unsigned record_size);			    // returns next available page in all directories; or new page if not found
    Directory& direcWithPage(unsigned pid);						// returns pid of directory containing target page
    void updateDirectory(Page* page);

    // Flushing
    void flushPage(Page* page);									// flush page to file and free its pointer
    void flushDirectory(Directory& d);							// flush directory to file
    void flushAll();											// flushes all directories and current page to file
    void smartFlush(Page* page);

    // Debug
    void printDebug();
    void printPage(int pid);
}; 

#endif
