#ifndef PAGE_H
#define PAGE_H

#include "types.h"

// General page class used for reading or writing
class Page {
public:
	Page(unsigned page_id);
	Page(const Page& page);
	~Page();

	void loadPage(char* data);		// transfers data from memory block to this page
	void setPID(unsigned new_id);	// sets PID

	// Getters
	unsigned getFreeSpace();
	unsigned getPID();
	std::vector<slot> getSlots();
	slot getSlot(int slot_id);
	unsigned getSlotCount();
	RID getLinkingRID(const RID& rid); 		// returns the RID for an updated record that has been moved to another page

	// Record Operations
	void insertRecord(const vector<Attribute> &recordDescriptor, const void *data, unsigned record_size, RID& rid, void* attr_metadata);
	RC readRecord(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor);
	RC readRecordWithAttrData(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor); // same as read record but include attribute metadata
	RC deleteRecord(const RID& rid); // returns 0 for success, -1 for error, and 1 if record is on other page
	RC updateRecord(const vector<Attribute> &recordDescriptor, const RID& rid, const void *data, unsigned record_size, void* attr_metadata); // assumes update is valid for this page
	RC updateRecordByMoving(RID current_rid, RID move_rid); // updates the page by deleting record and leaving tombstone to show new location
	void shiftSlotOffsets(int length, int starting_index); // shifts the slot offsets starting at start_index either left or right by given length
	RC readAttribute(const string &attributeName, const RID& rid, void* data, const vector<Attribute> &recordDescriptor);

	// Checking page
	bool pageFull(); 												// returns true if page is "full" or cannot reasonably insert any more records
	bool hasSpace(unsigned record_size); 							// returns true if page has space for record
	bool hasSpaceForUpdate(unsigned new_size, unsigned slot_id); 	// returns true if page has space for updated record
	RC checkValidSlot(unsigned slot_id); 							// returns 0 for success, -1 for error, and 1 if record is on other page

	// Flushing
	void flush(char* data); 				// returns page's data in a page block

	// Debug
	void printDebug();

private:
	unsigned pid; 								// this page's pid in the file
	unsigned slot_count;						// number of slots in page (NOT RECORDS); there may be more slots than records

	unsigned free_space_amount; 				// amount of free space in page
	unsigned free_space_begin; 					// offset to beginning of free space
	unsigned free_space_end; 					// offset to end of free space (the char right at the start of metadata)

	std::vector<slot> slots; 					// assumption: slots will never be removed, so slot id corresponds to record #
	std::vector<Attribute> record_descriptor;	// for storing the record descriptor that describes the records in this page
	std::unordered_map<unsigned, char*> records; 	// map from slot id to record data
};


#endif
