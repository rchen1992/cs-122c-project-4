#include "rbfm.h"


RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

RC RecordBasedFileManager::createFile(const string &fileName) {
	return PagedFileManager::instance()->createFile(fileName);
}

RC RecordBasedFileManager::destroyFile(const string &fileName) {
    return PagedFileManager::instance()->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) {
    RC opened = PagedFileManager::instance()->openFile(fileName, fileHandle);
    if (opened == 0) {	
        // if file has directory page, load directory
        if (fileHandle.getNumberOfPages() >= 1) {
        	fileHandle.loadFileDirectory(0);
        }
        else {
        	fileHandle.newDirectory();
        }
        fileHandle.loadCurrentPage(); // load last page in last directory as current page
        return 0;
    }

    return -1;
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
	fileHandle.flushAll();
    PagedFileManager::instance()->closeFile(fileHandle);
    return 0;
}



unsigned RecordBasedFileManager::recordSizeWithAttributeMetadata(const vector<Attribute> &recordDescriptor, const void *data, void* attr_metadata) {
	int rd_size = recordDescriptor.size();

	unsigned null_byte_count = div_ceil(rd_size, 8); 	 // # of null bytes to represent data
	char* null_byte_ptr = new char[null_byte_count]; 	 // pointer to array of null bytes
	memcpy((void*)null_byte_ptr, data, null_byte_count); // store null byte data

	int attr_metadata_length = rd_size * 4; 	// amount of space required for attribute metadata
	int current_attr_offset = null_byte_count;  // first attribute offset starts after null bytes
	int* attr_metadata_ptr = (int*)attr_metadata;


	char* c = (char*)data;
	c += null_byte_count;

	unsigned insert_size = 0;

	for (int i = 0; i < rd_size; i++) {
		Attribute a = recordDescriptor[i];
		int null_byte_index = i/8;
		bool nullbit = (null_byte_ptr[null_byte_index] & (1 << (7-(i%8)))); // is current field null?

		if (nullbit) { // null values don't take space
			*attr_metadata_ptr = -1; // -1 offset marks null attributes
		}
		else if (a.type == TypeVarChar) {
			int varchar_length = *(int*)c;
			c += varchar_length + 4;

			insert_size += sizeof(unsigned); // need an extra int to describe length of varchar
			insert_size += varchar_length;

			*attr_metadata_ptr = current_attr_offset; // save offset of varchar (including int describing varchar length)
			current_attr_offset += (4 + varchar_length); 
		}
		else {
			insert_size += a.length;
			c += 4; // assumes other types are only float or int

			*attr_metadata_ptr = current_attr_offset; // save offset of float or int attr
			current_attr_offset += 4;
		}

		attr_metadata_ptr++;
	}

	delete[] null_byte_ptr;
	return insert_size + null_byte_count + attr_metadata_length;
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) {
	// we are going to pass this to recordSizeWithAttributeMetadata to grab the attribute metadata along with record size
	char* attr_metadata = new char[recordDescriptor.size()*4]; 
	unsigned record_size = recordSizeWithAttributeMetadata(recordDescriptor, data, (void*)attr_metadata);
	Page* page; // the pointer to page we are going to insert into

	if (fileHandle.current_page == 0) {
		page = fileHandle.newPage();
	}
	// if current page has space
	else if (fileHandle.current_page->hasSpace(record_size)){
		page = fileHandle.current_page;
	}
	else {
		// check all directories for available page; or new page if unavailable
		page = fileHandle.nextAvailablePage(record_size);
	}

	// Do actual insert
	insertRecordIntoPage(page, fileHandle, recordDescriptor, data, record_size, rid, (void*)attr_metadata);
	delete[] attr_metadata;

	return 0;
}

RC RecordBasedFileManager::insertRecordIntoPage(Page* page, FileHandle& fileHandle,  const vector<Attribute> &recordDescriptor, const void *data, unsigned record_size, RID &rid, void* attr_metadata) {
	page->insertRecord(recordDescriptor, data, record_size, rid, attr_metadata);

	fileHandle.updateDirectory(page);

    // Automatically flush page if its not the current page (since we don't want to keep all pages in cache)
	fileHandle.smartFlush(page);

	return 0;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
	if (fileHandle.current_page->getPID() == rid.pageNum) { // if reading from current page
		RC valid = fileHandle.current_page->readRecord(rid.slotNum, data, recordDescriptor);
		if (valid == -1) { // error in read
			return -1;
		}
		if (valid == 1) { // record on another page
			RID linking_rid = fileHandle.current_page->getLinkingRID(rid);
			return readRecord(fileHandle, recordDescriptor, linking_rid, data);
		}
	}
	else { // reading from disk
		Page* page = fileHandle.getPage(rid.pageNum);
		RC valid = page->readRecord(rid.slotNum, data, recordDescriptor);
		if (valid == -1) { // error in read
			delete page;
			return -1;
		}
		if (valid == 1) { // record on another page
			RID linking_rid = page->getLinkingRID(rid);
			delete page;
			return readRecord(fileHandle, recordDescriptor, linking_rid, data);
		}
		delete page;
	}

    return 0;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) {
	int rd_size = recordDescriptor.size();

	unsigned null_byte_count = div_ceil(rd_size, 8); // # of null bytes to represent data
	char* null_byte_ptr = new char[null_byte_count]; // pointer to array of null bytes
	memcpy((void*)null_byte_ptr, data, null_byte_count); // store null byte data

	char* c = (char*) data;
	c += null_byte_count;

	for (int i = 0; i < rd_size; i++) {
		Attribute a = recordDescriptor[i];
		int null_byte_index = i/8;
		bool nullbit = null_byte_ptr[null_byte_index] & (1 << (7-(i%8)));

		std::cout << a.name << ": "; // print attribute name

		if (nullbit) {
			std::cout << "NULL      ";
		}
		else {
			switch(a.type) {
				case TypeInt:
					std::cout << *(int*)c << "      ";
					c += a.length;
					break;
				case TypeReal:
					std::cout << *(float*)c << "      ";
					c += a.length;
					break;
				case TypeVarChar:
					int varchar_length = *(int*)c;
					c += sizeof(int);
					for (int i = 0; i < varchar_length; i++) {
						std::cout << *c;
						c++;
					}
					std::cout << "      ";
					break;
			}
		}
	}
	std::cout << std::endl;

	delete[] null_byte_ptr;
    return 0;
}

RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid) {
	Page* page = fileHandle.getPage(rid.pageNum);
	RC deleted = page->deleteRecord(rid);

	if (deleted == -1) { // error in page deletion
		delete page;
		return -1;
	}

	if (deleted == 1) { // record exists on another page
		RID linking_rid = page->getLinkingRID(rid);
		delete page;

		return deleteRecord(fileHandle, recordDescriptor, linking_rid);
	}

	
	fileHandle.updateDirectory(page);
	fileHandle.smartFlush(page);

	return 0;
}

RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid) {
	int attr_metadata_length = recordDescriptor.size()*4;
	char* attr_metadata = new char[attr_metadata_length]; 
	unsigned record_size = recordSizeWithAttributeMetadata(recordDescriptor, data, (void*)attr_metadata);

	RC updated = updateRecordInPage(fileHandle, recordDescriptor, data, rid, record_size, (void*)attr_metadata);

	delete[] attr_metadata;

	return updated;
}

RC RecordBasedFileManager::updateRecordInPage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid, unsigned record_size, void* attr_metadata) {
	Page* page = fileHandle.getPage(rid.pageNum);

	// Checking if slot is valid (not a tombstone)
	RC valid = page->checkValidSlot(rid.slotNum);
	if (valid == -1) { // slot is not valid
		std::cout << "in update: invalid slot" << std::endl;
		delete page;
		return -1;
	}
	if (valid == 1) { // record is on another page
		std::cout << "in update: slot is tombstone" << std::endl;
		RID linking_rid = page->getLinkingRID(rid);
		delete page;

		// recursively call update by following linking rid
		return updateRecordInPage(fileHandle, recordDescriptor, data, linking_rid, record_size, attr_metadata);
	}

	// If we reach this point, slot id must be valid
	if (page->hasSpaceForUpdate(record_size, rid.slotNum)) { // page has space

		page->updateRecord(recordDescriptor, rid, data, record_size, attr_metadata);

		fileHandle.updateDirectory(page);
	}
	else { // must move record to new page
		Page* insert_page = fileHandle.nextAvailablePage(record_size);
		RID move_rid;
		insertRecordIntoPage(insert_page, fileHandle, recordDescriptor, data, record_size, move_rid, attr_metadata);
		page->updateRecordByMoving(rid, move_rid); 	// update record's old page

		fileHandle.updateDirectory(page);

	}

	fileHandle.smartFlush(page);

	return 0;

}

RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data) {
	Page* page = fileHandle.getPage(rid.pageNum);

	RC valid = page->readAttribute(attributeName, rid, data, recordDescriptor);
	if (valid == -1) { // error
		delete page;
		return -1;
	}
	if (valid == 1) { // slot was tombstone
		RID linking_rid = page->getLinkingRID(rid);
		delete page;

		// recurse by following linking rid to valid slot
		return readAttribute(fileHandle, recordDescriptor, linking_rid, attributeName, data);
	}

	return 0;
}

RC RecordBasedFileManager::scan(FileHandle &fileHandle,
								  const vector<Attribute> &recordDescriptor,
								  const string &conditionAttribute,
								  const CompOp compOp,                  // comparision type such as "<" and "="
								  const void *value,                    // used in the comparison
								  const vector<string> &attributeNames, // a list of projected attributes
								  RBFM_ScanIterator &rbfm_ScanIterator) {

	rbfm_ScanIterator.init(fileHandle, recordDescriptor, conditionAttribute, compOp, value, attributeNames);
	return 0;
}




