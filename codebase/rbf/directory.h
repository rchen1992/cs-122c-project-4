#ifndef DIRECTORY_H
#define DIRECTORY_H

#include "types.h"

// A slot inside directory that describes a page
struct page_slot {
	unsigned pid;	 	 // page number within file
	unsigned free_space; // free space within a page
};

class Directory {
public:
	Directory();
	~Directory();

	void loadDirectory(char* data, unsigned page_id); 	// takes a pointer to directory page data and loads it
	void flush(char* data); 							// flushes data to given char block; does not write to disk directly

	// Setters
	void setPID(unsigned page_id);
	void setPageCount(unsigned count);
	void setNextDirecPID(int pid);

	// Getters
	unsigned getPID();
	int getNextDirecPID();
	unsigned getLastPage();										// returns pid of last page in directory; if no pages in direc, return pid of direc
	unsigned getPageCount();
	std::map<int, page_slot> getSlots();

	// Get page info
	int nextAvailablePage(unsigned record_size); 			// returns the pid of next available page in directory
	bool pageHasSpace(unsigned record_size, unsigned pid); 		// returns true if page can fit record
	bool hasSpaceForPage();										// returns true if directory has space for insert
	bool hasPage(unsigned pid);									// returns true if this directory contains target page

	// Updating directory with page info
	void addPage(unsigned pid, unsigned free_space); 			// update directory with new page
	void updatePageSpace(unsigned pid, unsigned free_space); 	// update free space on a page

	// Debug
	void printDebug();



private:

	unsigned pid;						// page number of this directory in file
	unsigned page_count; 				// current number of pages in directory
	std::map<int, page_slot> slots; 	// ordered map of page slots
	int next_direc_pid;					// pid of next directory in file
	unsigned direc_free_space; 			// amount of free space left in directory


};

#endif
