#ifndef _rbfm_h_
#define _rbfm_h_


#include "../rbf/pfm.h"
#include "types.h"

using namespace std;




/****************************************************************************
The scan iterator is NOT required to be implemented for part 1 of the project 
*****************************************************************************/

# define RBFM_EOF (-1)  // end of a scan operator

// RBFM_ScanIterator is an iterator to go through records
// The way to use it is like the following:
//  RBFM_ScanIterator rbfmScanIterator;
//  rbfm.open(..., rbfmScanIterator);
//  while (rbfmScanIterator(rid, data) != RBFM_EOF) {
//    process the data;
//  }
//  rbfmScanIterator.close();


class RBFM_ScanIterator {
public:
  RBFM_ScanIterator();
  ~RBFM_ScanIterator();

  // "data" follows the same format as RecordBasedFileManager::insertRecord()
  void init(FileHandle &file_handle,
      const vector<Attribute> &record_descriptor,
      const string &condition_attribute,
      const CompOp comp_op,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attribute_names);

  // Filtering
  bool filterRecord(char* record, int rec_length);
  bool intPassesCondition(int value);
  bool floatPassesCondition(float value);
  bool varcharPassesCondition(char* str, int length);

  // Return projection of record
  void projectRecord(char* record, int rec_length, char* projection);

  // returns 1 if attribute is null; does not return nullbyte in data
  // returns -1 on error
  // returns 0 on success
  // if data is varchar, stores the varchar length in front
  RC getAttribute(char* rec, int rec_length, void* data, string attr_name, int attr_index, AttrType attr_type);
  RC getAttributeDescription(int& attr_index, AttrType& attr_type, string attr_name);

  RC getNextRecord(RID &rid, void *data);

  RC close();

private:
    FileHandle fileHandle;
    vector<Attribute> recordDescriptor;
    string conditionAttribute;      // ex) salary
    CompOp compOp;                  // comparision type such as "<" and "="
    const void *compVal;            // used in the comparison
    vector<string> attributeNames;  // project these attributes

    std::unordered_map<int,bool> direc_pids; // list of pids that correspond to directory pages
    Page* current_page;
    unsigned current_pid;                         // current page
    int current_slot_number;                 // current slot we are looking at in page
    int page_slot_count;                     // total # of slots for the current page

};


class RecordBasedFileManager
{
public:
  static RecordBasedFileManager* instance();

  RC createFile(const string &fileName);
  
  RC destroyFile(const string &fileName);
  
  // If empty file, it will append a directory page and then an empty page as the current page
  // Otherwise, it will load all directories and the last page as current page
  RC openFile(const string &fileName, FileHandle &fileHandle);
  
  // Flushes everything in memory to file
  RC closeFile(FileHandle &fileHandle);

  //  Format of the data passed into the function is the following:
  //  [n byte-null-indicators for y fields] [actual value for the first field] [actual value for the second field] ...
  //  1) For y fields, there is n-byte-null-indicators in the beginning of each record.
  //     The value n can be calculated as: ceil(y / 8). (e.g., 5 fields => ceil(5 / 8) = 1. 12 fields => ceil(12 / 8) = 2.)
  //     Each bit represents whether each field contains null value or not.
  //     If k-th bit from the left is set to 1, k-th field value is null. We do not include anything in the actual data.
  //     If k-th bit from the left is set to 0, k-th field contains non-null values.
  //     If thre are more than 8 fields, then you need to find the corresponding byte, then a bit inside that byte.
  //  2) actual data is a concatenation of values of the attributes
  //  3) For int and real: use 4 bytes to store the value;
  //     For varchar: use 4 bytes to store the length of characters, then store the actual characters.
  //  !!!The same format is used for updateRecord(), the returned data of readRecord(), and readAttribute()
  // For example, refer to the Q8 of Project 1 wiki page.
  RC insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid);

  // Does actual inserting of record
  RC insertRecordIntoPage(Page* page, FileHandle& fileHandle,  const vector<Attribute> &recordDescriptor, const void *data, unsigned record_size, RID &rid, void* attr_metadata);

  // Will recursively call itself and follow the link of RIDs to any updated records that have been moved to another page
  RC readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data);
  
  // This method will be mainly used for debugging/testing
  RC printRecord(const vector<Attribute> &recordDescriptor, const void *data);

  // Returns total amount of bytes record will take up
  // Takes into account null byte fields as well
  // Also saves the attribute metadata into memory block; since we parse through the record descriptor and fields,
  //    we might as well get the attribute metadata at the same time
  unsigned recordSizeWithAttributeMetadata(const vector<Attribute> &recordDescriptor, const void *data, void* attr_metadata);


/**************************************************************************************************************************************************************
IMPORTANT, PLEASE READ: All methods below this comment (other than the constructor and destructor) are NOT required to be implemented for part 1 of the project
***************************************************************************************************************************************************************/
  
  // Will recursively call itself and follow the link of RIDs to any updated records that have been moved to another page
  RC deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid);

  // Assume the rid does not change after update
  // Sets up a page for updating
  RC updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid);

  // Does actual update for a page
  RC updateRecordInPage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid, unsigned record_size, void* attr_metadata);

  RC readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data);

  // scan returns an iterator to allow the caller to go through the results one by one. 
  RC scan(FileHandle &fileHandle,
      const vector<Attribute> &recordDescriptor,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RBFM_ScanIterator &rbfm_ScanIterator);

public:

protected:
  RecordBasedFileManager();
  ~RecordBasedFileManager();

private:
  static RecordBasedFileManager *_rbf_manager;
};

#endif
