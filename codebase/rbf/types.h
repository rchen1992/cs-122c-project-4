#ifndef TYPES_H
#define TYPES_H

#include <string>
#include <cstring>
#include <vector>
#include <climits>
#include <unordered_map>
#include <map>
#include <iostream>
#include "helper.h"

typedef int RC;
typedef char byte;
typedef unsigned PageNum;

#define PAGE_SIZE 4096

using namespace std;

// Record ID
typedef struct
{
  unsigned pageNum;	// page number
  unsigned slotNum; // slot number in the page
} RID;


// Attribute
typedef enum { TypeInt = 0, TypeReal, TypeVarChar } AttrType;

typedef unsigned AttrLength;

struct Attribute {
    string   name;     // attribute name
    AttrType type;     // attribute type
    AttrLength length; // attribute length
};

struct TableTuple {
    int tableId;
    string tableName;
    string fileName;
};

struct ColumnTuple {
    int tableId;
    string columnName;
    int columnType;
    int columnLength;
    int columnPosition;
};

// Comparison Operator (NOT needed for part 1 of the project)
typedef enum { NO_OP = 0,  // no condition
		   EQ_OP,      // =
           LT_OP,      // <
           GT_OP,      // >
           LE_OP,      // <=
           GE_OP,      // >=
           NE_OP,      // !=
} CompOp;

// slot metadata that describes a record on a page
struct slot {
	int offset;
	int length;
};

#endif
