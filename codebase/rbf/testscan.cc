// #include <iostream>
// #include <string>
// #include <cassert>
// #include <sys/stat.h>
// #include <stdlib.h>
// #include <string.h>
// #include <stdexcept>
// #include <stdio.h>

// #include "pfm.h"
// #include "rbfm.h"
// #include "test_util.h"

// using namespace std;

// int RBFTest_8(RecordBasedFileManager *rbfm) {
//    // Functions tested
//    // 1. Create Record-Based File
//    // 2. Open Record-Based File
//    // 3. Insert Record
//    // 4. Read Record
//    // 5. Close Record-Based File
//    // 6. Destroy Record-Based File
//    cout << endl << "***** In RBF Test Case 8 *****" << endl;

//    RC rc;
//    string fileName = "test8";

//    // Create a file named "test8"
//    rc = rbfm->createFile(fileName);
//    assert(rc == success && "Creating the file should not fail.");

// 	rc = createFileShouldSucceed(fileName);
//    assert(rc == success && "Creating the file failed.");

//    // Open the file "test8"
//    FileHandle fileHandle;
//    rc = rbfm->openFile(fileName, fileHandle);
//    assert(rc == success && "Opening the file should not fail.");

//    RID rid;
//    int recordSize = 0;
//    void *record = malloc(100);
//    void *returnedData = malloc(100);

//    vector<Attribute> recordDescriptor;
//    createRecordDescriptor(recordDescriptor);

//    // Initialize a NULL field indicator
//    int nullFieldsIndicatorActualSize = getActualByteForNullsIndicator(recordDescriptor.size());
//    unsigned char *nullsIndicator = (unsigned char *) malloc(nullFieldsIndicatorActualSize);
// 	memset(nullsIndicator, 0, nullFieldsIndicatorActualSize);

//    // Insert a record into a file
//    prepareRecord(recordDescriptor.size(), nullsIndicator, 6, "Peters", 24, 170.1, 5000, record, &recordSize);
//    cout << "Insert Data:" << endl;
//    rbfm->printRecord(recordDescriptor, record);

//    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
//    assert(rc == success && "Inserting a record should not fail.");


//    // Given the rid, read the record from file
//    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
//    assert(rc == success && "Reading a record should not fail.");

//    cout << "Returned Data:" << endl;
//    rbfm->printRecord(recordDescriptor, returnedData);

//    // Compare whether the two memory blocks are the same
//    if(memcmp(record, returnedData, recordSize) != 0)
//    {
//        cout << "[FAIL] Test Case 8 Failed!" << endl << endl;
//        free(record);
//        free(returnedData);
//        return -1;
//    }




//    RID rid2;
//    RID test_rid2;
//    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid2);

//    // ----------- TEST SCAN ----------- //
//    vector<string> attributeNames;
//    for (auto it = recordDescriptor.begin(); it != recordDescriptor.end(); it++) {
//       attributeNames.push_back(it->name);
//    }
//    RBFM_ScanIterator scanner;
//    int compVal = 4;
//    rbfm->scan(fileHandle, recordDescriptor, "", NO_OP, (void*)&compVal, attributeNames, scanner);

//    RID test_rid;
//    void* scannedData = malloc(100);
//    scanner.getNextRecord(test_rid, scannedData);

//    std::cout << "Returning test RID -- Page num: " << test_rid.pageNum << ", Slot num: " << test_rid.slotNum << std::endl; 
//    // Compare whether the two memory blocks are the same
//    if(memcmp(record, scannedData, recordSize) != 0)
//    {
//        cout << "[FAIL] Test Case 8 Failed!" << endl << endl;
//        free(record);
//        free(returnedData);
//        free(scannedData);
//        return -1;
//    }

//    memset(scannedData, 0, 100);
//    scanner.getNextRecord(test_rid2, scannedData);
//    std::cout << "Returning test RID2 -- Page num: " << test_rid2.pageNum << ", Slot num: " << test_rid2.slotNum << std::endl; 
//    if(memcmp(record, scannedData, recordSize) != 0)
//    {
//        cout << "[FAIL] Test Case 8 Failed!" << endl << endl;
//        free(record);
//        free(returnedData);
//        free(scannedData);
//        return -1;
//    }

//    scanner.close();





//    // Close the file "test8"
//    rc = rbfm->closeFile(fileHandle);
//    assert(rc == success && "Closing the file should not fail.");

//    // Destroy File
//    rc = rbfm->destroyFile(fileName);
//    assert(rc == success && "Destroying the file should not fail.");

// 	rc = destroyFileShouldSucceed(fileName);
//    assert(rc == success  && "Destroying the file should not fail.");


//    free(record);
//    free(returnedData);
//    free(scannedData);

//    cout << "[PASS] Test Case 8 Passed!" << endl << endl;

//    return 0;
// }

// int main()
// {
// 	// To test the functionality of the paged file manager
//    // PagedFileManager *pfm = PagedFileManager::instance();

//    // To test the functionality of the record-based file manager
//    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

//    remove("test8");

//    RC rcmain = RBFTest_8(rbfm);
//    return rcmain;
// }






#include <fstream>
#include <iostream>
#include <string>
#include <cassert>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <stdexcept>
#include <stdio.h>

#include "pfm.h"
#include "rbfm.h"
#include "test_util.h"

using namespace std;

int RBFTest_9(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
   // Functions tested
   // 1. Create Record-Based File
   // 2. Open Record-Based File
   // 3. Insert Multiple Records
   // 4. Close Record-Based File
   cout << endl << "***** In RBF Test Case 9 *****" << endl;

   RC rc;
   string fileName = "test9";

   // Create a file named "test9"
   rc = rbfm->createFile(fileName);
   assert(rc == success && "Creating the file should not fail.");

   rc = createFileShouldSucceed(fileName);
   assert(rc == success && "Creating the file failed.");

   // Open the file "test9"
   FileHandle fileHandle;
   rc = rbfm->openFile(fileName, fileHandle);
   assert(rc == success && "Opening the file should not fail.");

   RID rid;
   void *record = malloc(1000);
   int numRecords = 2000;

   vector<Attribute> recordDescriptor;
   createLargeRecordDescriptor(recordDescriptor);

   for(unsigned i = 0; i < recordDescriptor.size(); i++)
   {
       cout << "Attr Name: " << recordDescriptor[i].name << " Attr Type: " << (AttrType)recordDescriptor[i].type << " Attr Len: " << recordDescriptor[i].length << endl;
   }
   cout << endl;

   // NULL field indicator
   int nullFieldsIndicatorActualSize = getActualByteForNullsIndicator(recordDescriptor.size());
   unsigned char *nullsIndicator = (unsigned char *) malloc(nullFieldsIndicatorActualSize);
   memset(nullsIndicator, 0, nullFieldsIndicatorActualSize);

   // Insert 2000 records into file
   for(int i = 0; i < numRecords; i++)
   {
       // Test insert Record
       int size = 0;
       memset(record, 0, 1000);
       prepareLargeRecord(recordDescriptor.size(), nullsIndicator, i, record, &size);

       rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
       assert(rc == success && "Inserting the file should not fail.");

       rids.push_back(rid);
       sizes.push_back(size);
   }


std::cout << "set up scan" << std::endl;
   // ----------- TEST SCAN ----------- //
   vector<string> attributeNames;
   // for (auto it = recordDescriptor.begin(); it != recordDescriptor.end(); it++) {
   //    attributeNames.push_back(it->name);
   // }
   attributeNames.push_back("attr27");
   attributeNames.push_back("attr28");
   attributeNames.push_back("attr29");
   RBFM_ScanIterator scanner;
   string compVal = "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm";
   rbfm->scan(fileHandle, recordDescriptor, "attr27", NE_OP, (void*)(compVal.c_str()), attributeNames, scanner);

   std::cout << "starting scan" << std::endl;
   RID test_rid;
   void* scannedData = malloc(1000);
   void* readData = malloc(1000);
   int rec_count = 0;
   while (scanner.getNextRecord(test_rid, scannedData) != RBFM_EOF) {
      char* c = (char*)scannedData+1;
      rec_count++;
      int varchar_length = *(int*)c;
      std::cout << "varchar_length: " << varchar_length << std::endl;
      c += 4;
      std::cout << "attr27: ";
      for (int i = 0; i < varchar_length; i++) {
         std::cout << *c;
         c++;
      }
      std::cout << endl;
      std::cout << "attr28: " << *(int*)c << std::endl;
      c += 4;
      std::cout << "attr29: " << *(float*)c << std::endl;
      // std::cout << "Returning test RID -- Page num: " << test_rid.pageNum << ", Slot num: " << test_rid.slotNum << std::endl; 

      // rbfm->readRecord(fileHandle, recordDescriptor, test_rid, readData);
      // Compare whether the two memory blocks are the same
      // if(memcmp(readData, scannedData, 134) != 0)
      // {
      //     cout << "[FAIL] Test Case 8 Failed!" << endl << endl;
      //     free(readData);
      //     free(scannedData);
      //     return -1;
      // }
      // rbfm->printRecord(recordDescriptor, readData);
   }
   std::cout << "final record count from scan: " << rec_count << std::endl;
   free(scannedData);
   free(readData);
   scanner.close();


   // Close the file "test9"
   rc = rbfm->closeFile(fileHandle);
   assert(rc == success && "Closing the file should not fail.");

   free(record);


   // Write RIDs to the disk. Do not use this code. This is not a page-based operation - for the test purpose only.
   ofstream ridsFile("test9rids", ios::out | ios::trunc | ios::binary);

   if (ridsFile.is_open()) {
      ridsFile.seekp(0, ios::beg);
      for (int i = 0; i < numRecords; i++) {
         ridsFile.write(reinterpret_cast<const char*>(&rids[i].pageNum),
               sizeof(unsigned));
         ridsFile.write(reinterpret_cast<const char*>(&rids[i].slotNum),
               sizeof(unsigned));
         if (i % 1000 == 0) {
            cout << "RID #" << i << ": " << rids[i].pageNum << ", "
                  << rids[i].slotNum << endl;
         }
      }
      ridsFile.close();
   }

   // Write sizes vector to the disk. Do not use this code. This is not a page-based operation - for the test purpose only.
   ofstream sizesFile("test9sizes", ios::out | ios::trunc | ios::binary);

   if (sizesFile.is_open()) {
      sizesFile.seekp(0, ios::beg);
      for (int i = 0; i < numRecords; i++) {
         sizesFile.write(reinterpret_cast<const char*>(&sizes[i]),sizeof(int));
         if (i % 1000 == 0) {
            cout << "Sizes #" << i << ": " << sizes[i] << endl;
         }
      }
      sizesFile.close();
   }

   cout << "[PASS] Test Case 9 Passed!" << endl << endl;

   return 0;
}

int main()
{
   // To test the functionality of the paged file manager
   // PagedFileManager *pfm = PagedFileManager::instance();

   // To test the functionality of the record-based file manager
   RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

   remove("test9");
   remove("test9rids");
   remove("test9sizes");

   vector<RID> rids;
   vector<int> sizes;
   RC rcmain = RBFTest_9(rbfm, rids, sizes);
   return rcmain;
}

