#ifndef _helper_h_
#define _helper_h_
#include <sys/stat.h>
// inline bool fileExists (const std::string& name) {
//     if (FILE *file = fopen(name.c_str(), "r")) {
//         fclose(file);
//         return true;
//     } else {
//         return false;
//     }
// }

// Check whether the given file exists
inline bool fileExists(const std::string &fileName)
{
    struct stat stFileInfo;

    if(stat(fileName.c_str(), &stFileInfo) == 0) return true;
    else return false;
}

// Divides two numbers and returns the ceiling
inline int div_ceil ( int numerator, int denominator )
{
    return numerator / denominator
             + (((numerator < 0) ^ (denominator > 0)) && (numerator%denominator));
}

inline void setBit(char& number, int bit_number) {
	number |= 1 << bit_number;
}

inline int getBit(char& number, int bit_number) {
	return number & (1 << (7-bit_number));
}

#endif
