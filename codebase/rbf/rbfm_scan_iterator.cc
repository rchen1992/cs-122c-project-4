#include "rbfm.h"
#include <cstring>

// -------- RBF ITERATOR --------- //
RBFM_ScanIterator::RBFM_ScanIterator() {
	compOp = NO_OP;
	conditionAttribute = "";
	compVal = 0;
}

RBFM_ScanIterator::~RBFM_ScanIterator() {
}

void RBFM_ScanIterator::init(FileHandle &file_handle,
      const vector<Attribute> &record_descriptor,
      const string &condition_attribute,
      const CompOp comp_op,                  // comparision type such as "<" and "="
      const void *value,                     // used in the comparison
      const vector<string> &attribute_names) // a list of projected attributes
{
	fileHandle         = file_handle;
	recordDescriptor   = record_descriptor;
	conditionAttribute = condition_attribute;
	compOp             = comp_op;
	compVal            = value;
	attributeNames     = attribute_names;

	// add directory pids to blacklist
	for (auto it = fileHandle.directories.begin(); it != fileHandle.directories.end(); it++) {
		direc_pids[it->getPID()] = true;
	}

	if (fileHandle.pageCount > 1) {
		current_page = fileHandle.getPage(1); // get first page
	}
	else {
		current_page = 0; // no pages in file yet
	}

	current_pid = 1;
	current_slot_number = 0;
	page_slot_count = current_page->getSlotCount();
}


RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data) { 
	// If there are no pages in file to begin with, 
	// or if we've exhausted all pages in file
	if (current_page == 0 || current_pid >= fileHandle.pageCount) {
		return RBFM_EOF;
	}

	// If page is directory, skip page
	if (direc_pids[current_pid]) {
		current_pid++;
		return getNextRecord(rid, data);
	}

	// If currently cached page is not the one we want,
	// grab from disk and reset counters
	if (current_page->getPID() != current_pid) {
		delete current_page;
		current_page = fileHandle.getPage(current_pid);
		current_slot_number = 0;
		page_slot_count = current_page->getSlotCount();
	}

	// Iff no more slots on page, go to next page
	if (current_slot_number >= page_slot_count) {
		current_pid++;
		return getNextRecord(rid, data);
	}

	slot s = current_page->getSlot(current_slot_number);

	// Check for tombstone; skip it
	if (s.length <= 0) { 
		current_slot_number++;
		return getNextRecord(rid, data);
	}

	char* record = new char[s.length];
	current_page->readRecordWithAttrData(current_slot_number, (void*)record, recordDescriptor);

	if (compOp == NO_OP || filterRecord(record, s.length)) { // if record passes conditions

		// Fill in RID
		rid.pageNum = current_page->getPID();
		rid.slotNum = current_slot_number;

		if (attributeNames.size() == recordDescriptor.size()) { // if we want all the attributes, no need for projection
			memcpy(data, (void*)record, s.length);
		}
		else {
			// Create projection of desired attributes
			char* projection = new char[s.length];
			projectRecord(record, s.length, projection); 

			// Copy data
			memcpy(data, (void*)projection, s.length);
			delete[] projection;
		}

		delete[] record;
		current_slot_number++;
		return 0;
	}
	else {
		// if record was not valid, go to next slot
		// and recurse
		current_slot_number++;
		return getNextRecord(rid, data);
	}
}

bool RBFM_ScanIterator::filterRecord(char* rec, int rec_length) {
	// Find index of attribute
	int attr_index = -1;
	AttrType attr_type;
	RC valid_attr = getAttributeDescription(attr_index, attr_type, conditionAttribute);

	// Check if valid attribute (attribute exists)
	if (valid_attr == -1) {
		std::cout << "attribute not in record descriptor" << std::endl;
		return false;
	}

	// Grab attribute data
	char* attr_data = new char[rec_length];
	RC result = getAttribute(rec, rec_length, (void*)attr_data, conditionAttribute, attr_index, attr_type);
	if (result != 0) {
		return false;
	}

	// Check attribute against condition depending on type
	bool pass = false;
	switch(attr_type) {
		case TypeInt: {
			int value = *(int*)attr_data;
			pass = intPassesCondition(value);
			break;
		}
		case TypeReal: {
			float value = *(float*)attr_data;
			pass = floatPassesCondition(value);
			break;
		}
		case TypeVarChar: {
			int varchar_length = *(int*)attr_data;
			char* str = attr_data + 4;
			pass = varcharPassesCondition(str, varchar_length);
			break;
		}
		default: {
			std::cout << "read attribute type unrecognized" << std::endl;
			delete[] attr_data;
			return false;
		}
	}

	delete[] attr_data;
	return pass;
}

bool RBFM_ScanIterator::intPassesCondition(int value) {
	int comp_val = *(int*)compVal;

	switch(compOp) {
		case EQ_OP: {
			return value == comp_val;
		}
		case LT_OP: {
			return value < comp_val;
		}
		case GT_OP: {
			return value > comp_val;
		}
		case LE_OP: {
			return value <= comp_val;
		}
		case GE_OP: {
			return value >= comp_val;
		}
		case NE_OP: {
			return value != comp_val;
		}
		default: {
			return false;
		}
	}
}

bool RBFM_ScanIterator::floatPassesCondition(float value) {
	float comp_val = *(float*)compVal;
	
	switch(compOp) {
		case EQ_OP: {
			return value == comp_val;
		}
		case LT_OP: {
			return value < comp_val;
		}
		case GT_OP: {
			return value > comp_val;
		}
		case LE_OP: {
			return value <= comp_val;
		}
		case GE_OP: {
			return value >= comp_val;
		}
		case NE_OP: {
			return value != comp_val;
		}
		default: {
			return false;
		}
	}
}

bool RBFM_ScanIterator::varcharPassesCondition(char* str, int length) {
	char* cval = (char*) compVal;
	char cstr[length + 1];
	memset(cstr, 0, sizeof(cstr));	// Zero out the junk (put a null at the end);
	strncpy(cstr, str, length);

//	cout << "PRINT: " << cstr << "\t LENGTH: " << length << endl;
//	cout << "PRINT COMP: " << (char*) compVal << endl;
	switch(compOp) {
		case EQ_OP: { return strcmp(cstr, cval) == 0; }
		case NE_OP: { return strcmp(cstr, cval) != 0; }
		case LT_OP: { return strcmp(cstr, cval) < 0;  }
		case GT_OP: { return strcmp(cstr, cval) > 0;  }
		case LE_OP: { return strcmp(cstr, cval) <= 0; }
		case GE_OP: { return strcmp(cstr, cval) >= 0; }
		default: {
			return false;
		}
	}
}

// bool RBFM_ScanIterator::varcharPassesCondition(char* str, int length) {
// 	std::string tuple_val(str, length);
// 	std::string comp_val((char*)compVal);

// 	switch(compOp) {
// 		case EQ_OP: { return tuple_val == comp_val; }
// 		case NE_OP: { return tuple_val != comp_val; }
// 		case LT_OP: { return tuple_val < comp_val; }
// 		case GT_OP: { return tuple_val > comp_val; }
// 		case LE_OP: { return tuple_val <= comp_val; }
// 		case GE_OP: { return tuple_val >= comp_val; }
// 		default: {
// 			return false;
// 		}
// 	}
// }

RC RBFM_ScanIterator::getAttribute(char* rec, int rec_length, void* data, string attr_name, int attr_index, AttrType attr_type) {
	int attr_count = recordDescriptor.size();

	// seek to desired attr metadata to find offset
	char* attr_metadata = rec + rec_length - (attr_count * 4) + (attr_index * 4);
	// offset of attribute in record
	int attr_offset = *(int*)attr_metadata;

	if (attr_offset == -1) { // offset of -1 means field is null
		return 1;
	}

	char* c = rec + attr_offset;
	switch(attr_type) {
		case TypeInt: case TypeReal: {
			memcpy(data, (void*)c, 4);
			return 0;
		}
		case TypeVarChar: {
			int varchar_length = *(int*)c;
			memcpy(data, (void*)c, 4 + varchar_length);
			return 0;
		}
		default:
			return -1;
	}
}

RC RBFM_ScanIterator::getAttributeDescription(int& attr_index, AttrType& attr_type, string attr_name) {
	int attr_count = recordDescriptor.size();

	// Find index of attribute
	for (int i = 0; i < attr_count; i++) {
		if (recordDescriptor[i].name == attr_name) {
			attr_index = i;
			attr_type = recordDescriptor[i].type;
			return 0;
		}
	}

	return -1;
}

void RBFM_ScanIterator::projectRecord(char* record, int rec_length, char* projection) {
	int attr_index = -1;
	AttrType attr_type;

	int null_byte_count = div_ceil(attributeNames.size(), 8); // # of null bytes needed for projection
	memset((void*)projection, 0, null_byte_count); // set the beginning of projection to null bytes

	char* c = projection + null_byte_count; // pointer after null bytes
	for (auto it = attributeNames.begin(); it != attributeNames.end(); it++) {
		getAttributeDescription(attr_index, attr_type, *it); // get attr type and index

		int null_byte_index = attr_index/8;	// which null byte
		char* attr_data = new char[rec_length];	

		RC result = getAttribute(record, rec_length, (void*)attr_data, *it, attr_index, attr_type);
		if (result == 1) { // attribute was null
			setBit(projection[null_byte_index], 7-(attr_index%8)); // mark corresponding attribute as null
		}
		else {
			switch(attr_type) {
				case TypeInt: case TypeReal: {
					memcpy((void*)c, attr_data, 4);
					c += 4;
					break;
				}
				case TypeVarChar: {
					int varchar_length = *(int*)attr_data;
					memcpy((void*)c, attr_data, 4 + varchar_length);
					c += (4 + varchar_length);
					break;
				}
			}
		}

		delete[] attr_data;
	}
}


RC RBFM_ScanIterator::close() {
	if (current_page != fileHandle.current_page) {
		delete current_page;
	}
	return 0;
}




