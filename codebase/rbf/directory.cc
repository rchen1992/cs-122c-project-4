#include "directory.h"

Directory::Directory() {
	pid = 0;
	page_count = 0;
	next_direc_pid = -1;
	direc_free_space = PAGE_SIZE - sizeof(next_direc_pid) - sizeof(page_count);
}

Directory::~Directory() {
}

void Directory::loadDirectory(char* data, unsigned page_id) {
	pid = page_id;

	char* c = data;
	page_count = *(unsigned*)c; // store page count
	c += sizeof(page_count); // seek to beginning of page slot data

	for (unsigned i = 0; i < page_count; i++) {
		page_slot ps;

		ps.pid = *(unsigned*)c; // store page's offset
		c += sizeof(ps.pid);

		ps.free_space = *(unsigned*)c; // store page's free space amount
		c += sizeof(ps.free_space);

		slots[ps.pid] = ps; // add slot to map of slots
	}

	next_direc_pid = *(int*)(data + PAGE_SIZE - sizeof(next_direc_pid)); // store pid of next directory
	direc_free_space = PAGE_SIZE - sizeof(next_direc_pid) - sizeof(page_count) - (2*sizeof(unsigned)*slots.size());
}


void Directory::flush(char* data) {
	char* c = data;
	*(unsigned*)c = page_count;
	c += sizeof(page_count);

	for (auto it = slots.begin(); it != slots.end(); it++) {
		std::pair<int, page_slot> p = *it;
		page_slot ps = p.second;
		*(unsigned*)c = ps.pid;
		c += sizeof(ps.pid);
		*(unsigned*)c = ps.free_space;
		c += sizeof(ps.free_space);
	}

	*(int*)(data + PAGE_SIZE - sizeof(next_direc_pid)) = next_direc_pid;
}

std::map<int, page_slot> Directory::getSlots() {
	return slots;
}

void Directory::setPageCount(unsigned count) {
	page_count = count;
}

void Directory::setPID(unsigned page_id) {
	pid = page_id;
}

void Directory::setNextDirecPID(int pid) {
	next_direc_pid = pid;
}

unsigned Directory::getPID() {
	return pid;
}

int Directory::getNextDirecPID() {
	return next_direc_pid;
}

unsigned Directory::getLastPage() {
	if (slots.empty()) {
		return pid; // indicates no pages in directory
	}
	else {
		return slots.rbegin()->first;
	}
}

unsigned Directory::getPageCount() {
	return page_count;
}

int Directory::nextAvailablePage(unsigned record_size) {
	for (auto it = slots.begin(); it != slots.end(); it++) {
		std::pair<int, page_slot> p = *it;
		page_slot ps = p.second;
		if (ps.free_space > (record_size + sizeof(slot))) {
			return ps.pid;
		}
	}

	return -1;
}

bool Directory::pageHasSpace(unsigned record_size, unsigned pid) {
	return slots[pid].free_space > (record_size + sizeof(slot) );
}

void Directory::addPage(unsigned pid, unsigned free_space) {
	page_slot ps;
	ps.pid = pid;
	ps.free_space = free_space;

	slots[pid] = ps;

	page_count++;
	direc_free_space -= sizeof(page_slot);
}

bool Directory::hasSpaceForPage() {
	return direc_free_space >= sizeof(page_slot);
}

bool Directory::hasPage(unsigned pid) {
	return (slots.find(pid) != slots.end());
}

void Directory::updatePageSpace(unsigned pid, unsigned free_space) {
	slots[pid].free_space = free_space;
}

void Directory::printDebug() {
	std::cout << "// ---- Debug info for directory pid#" << pid << " ----- //" << std::endl;
	std::cout << "\tPage count: " << page_count << std::endl;
	std::cout << "\tDirectory free space: " << direc_free_space << std::endl;
	std::cout << "\tNext directory pid: " << next_direc_pid << std::endl;
	for (auto it = slots.begin(); it != slots.end(); it++) {
		page_slot ps = it->second;
		std::cout << "\t\tslot pid: " << ps.pid << ", slot free space: " << ps.free_space << std::endl;
	}
}
