#include "readpage.h"

ReadPage::ReadPage(char* data) {
	page_data = data;
}


ReadPage::~ReadPage() {
	delete[] page_data;
}

slot ReadPage::getSlot(unsigned slot_id) {
	char* c = page_data + PAGE_SIZE - 8 - sizeof(slot) - (slot_id * sizeof(slot)); // seek to correct slot
	slot s;
	s.offset = *(int*)c;
	c += 4;
	s.length = *(int*)c;

	return s;
}

RC ReadPage::readRecord(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor) {
	RC valid = checkValidSlot(slot_id);
	if (valid != 0) {
		return valid;
	}
	
	slot s = getSlot(slot_id);
	int attr_metadata_length = recordDescriptor.size() * 4; // total length for attribute metadata at the end of record
	
	char* c = page_data + s.offset; // seek to front of record

	memcpy(data, (void*) c, s.length - attr_metadata_length); // copy only actual record data

	return 0;
}

RC ReadPage::checkValidSlot(unsigned slot_id) {
	char* c = page_data + PAGE_SIZE - 8;
	unsigned slot_count = *(unsigned*)c;

	if (slot_id >= slot_count) { // if slot id out of range
		std::cout << "Slot id out of range. Record does not exist on this page." << std::endl;
		return -1;
	}

	slot s = getSlot(slot_id);
	if (s.length == 0) { // if record already been deleted
		std::cout << "The record you requested is deleted." << std::endl;
		return -1;
	}
	else if (s.length < 0 && s.offset < 0) { // if record exists on another page
		return 1; // signal that record exists on another page
	}
	else {
		return 0;
	}
}

RID ReadPage::getLinkingRID(const RID& rid) {
	RID linking_rid;
	slot s = getSlot(rid.slotNum);

	linking_rid.pageNum = abs(s.offset);
	linking_rid.slotNum = abs(s.length);

	return linking_rid;
}


