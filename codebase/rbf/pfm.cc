#include "pfm.h"

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
    if(!_pf_manager)
        _pf_manager = new PagedFileManager();

    return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
}


PagedFileManager::~PagedFileManager()
{
}


RC PagedFileManager::createFile(const string &fileName)
{
	// Check for existing file
	if (fileExists(fileName)) {
		std::cout << "File already exists" << std::endl;
		return -1;
	}

	// Create empty file
	std::ofstream file(fileName);
	file << flush;
	file.close();
    return 0;
}


RC PagedFileManager::destroyFile(const string &fileName)
{
    return remove(fileName.c_str());
}


// If file exists, open it for read/write and attach to fileHandle
RC PagedFileManager::openFile(const string &fileName, FileHandle &fileHandle)
{
	if (fileExists(fileName)) {
		std::fstream* file = new fstream(fileName, ios::in | ios::out | ios::binary);
		return fileHandle.attachFile(file);
	}

	std::cout << "File:" << fileName << " does not exist." << std::endl;
	return -1;
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
	if (fileHandle.file->is_open()) {
		fileHandle.file->flush();
		fileHandle.file->close();
		std::fstream* temp = fileHandle.file;
		fileHandle.file = 0;
		delete temp;
		fileHandle.file = 0;

		fileHandle.directories.clear();

		return 0;
	}

	return -1;
}


FileHandle::FileHandle()
{
	readPageCounter = 0;
	writePageCounter = 0;
	appendPageCounter = 0;
	pageCount = 0;
	file = 0;
	current_page = 0;
}


FileHandle::~FileHandle()
{
}

RC FileHandle::attachFile(std::fstream* f) {
	if (file != 0) {
		std::cout << "File handle already has open file." << std::endl;
		f->close();
		return -1;
	}

	f->seekg(0, std::ios_base::end); // seek to end
	size_t size = f->tellg(); // size of file in bytes
	f->seekg(0, std::ios_base::beg); // seek to beginning

	pageCount = size/PAGE_SIZE; // calculate # of pages
	file = f;

	return 0;
}

RC FileHandle::readPage(PageNum pageNum, void *data)
{
	if (pageNum < pageCount) {
		file->seekg(pageNum * PAGE_SIZE); // seek to correct page
		file->read((char*)data, PAGE_SIZE); // read in full page and store in char array
		readPageCounter++;
		return 0;
	}

	std::cout << "Cannot read page -- does not exist." << std::endl;
    return -1;
}


RC FileHandle::writePage(PageNum pageNum, const void *data)
{
	if (pageNum < pageCount) {
		file->seekp(pageNum * PAGE_SIZE); // seek to correct page
		file->write((char*)data, PAGE_SIZE);
		writePageCounter++;

		return 0;
	}

	std::cout << "Cannot write page -- does not exist. Page count: " << pageCount << " Page num: " << pageNum << std::endl;
    return -1;
}


RC FileHandle::appendPage(const void *data)
{
	file->seekp(pageCount * PAGE_SIZE);
	file->write((char*)data, PAGE_SIZE);
	pageCount++;
	appendPageCounter++;

	return 0;
}


unsigned FileHandle::getNumberOfPages()
{
    return pageCount;
}


RC FileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
	readPageCount = readPageCounter;
	writePageCount = writePageCounter;
	appendPageCount = appendPageCounter;

	return 0;
}

void FileHandle::newDirectory() {
	char* data = new char[PAGE_SIZE];
	memset(data, '0', PAGE_SIZE);
	appendPage((void*) data); // append new directory page

	Directory new_direc;
	new_direc.setPID(pageCount-1);

	// if this new directory is the first directory,
	// set the last directory's "next directory PID" to refer to this new directory
	if (directories.size() > 0) {
		directories.back().setNextDirecPID(pageCount-1);
	}
	directories.push_back(new_direc);
	delete[] data;
}

Page* FileHandle::getPage(unsigned pid) {
	// if desired page is current page, simply return it
	if (current_page != 0 && current_page->getPID() == pid) {
		return current_page;
	}
	else {
		// pull page from disk
		char* data = new char[PAGE_SIZE];
		memset(data, '0', PAGE_SIZE);
		readPage(pid, (void*)data);
		Page* page = new Page(pid);
		page->loadPage(data);

		delete[] data;

		return page;
	}
}

ReadPage* FileHandle::getReadPage(unsigned pid) {
	// pull read page from disk
	char* data = new char[PAGE_SIZE];
	memset(data, '0', PAGE_SIZE);
	readPage(pid, (void*)data);
	ReadPage* page = new ReadPage(data);

	return page;
}

Page* FileHandle::newPage() {
	char* data = new char[PAGE_SIZE];
	memset(data, '0', PAGE_SIZE);
	appendPage((void*) data);

	unsigned pid = pageCount-1;
	Page* page = new Page(pid);
	directories.back().addPage(pid, page->getFreeSpace()); // update directory
	current_page = page; // set as file handle's current page

	delete[] data;

	return page;
}

void FileHandle::loadFileDirectory(unsigned pid) {
	char* page = new char[PAGE_SIZE];
	memset(page, '0', PAGE_SIZE);
	readPage(pid,(void*) page); // read first page
	Directory d;
	d.loadDirectory(page, pid); // load first page's data into directory
	directories.push_back(d);
	delete page;

	if (d.getNextDirecPID() > 0) {
		loadFileDirectory(d.getNextDirecPID());
	}
}

void FileHandle::loadCurrentPage() {
	int direc_size = directories.size();
	for (int i = direc_size-1; i >= 0; i--) {
		if (directories[i].getPageCount() == 0) {
			continue;
		}
		else {
			current_page = getPage(directories[i].getLastPage());
			return;
		}
	}
}

int FileHandle::nextAvailablePID(unsigned record_size) {
	for (auto d = directories.begin(); d != directories.end(); d++) {
		int available_pid = d->nextAvailablePage(record_size);
		if (available_pid != -1) {
			return available_pid;
		}
	}

	return -1;
}

Page* FileHandle::nextAvailablePage(unsigned record_size) {
	// check all directories for available page and get the id of that page
	int available_pid = nextAvailablePID(record_size);
	Page* page;

	if (available_pid == -1) { // if no existing page was available in directory
		flushPage(current_page); // flush current page

		if (!directories.back().hasSpaceForPage()) { // if there is no room in directory for new page
			newDirectory(); // create new direc
		}

		page = newPage();
	}
	else {
		// if available page found, get from disk
		page = getPage(available_pid);
	}

	return page;
}

Directory& FileHandle::direcWithPage(unsigned pid) {
	for (auto it = directories.begin(); it != directories.end(); it++) {
		if (it->hasPage(pid)) {
			return *it;
		}
	}
}

void FileHandle::updateDirectory(Page* page) {
	// Update directory with updated page's free space
	Directory& d = direcWithPage(page->getPID());
	d.updatePageSpace(page->getPID(), page->getFreeSpace());
	flushDirectory(d);
}

void FileHandle::flushPage(Page* page) {
	char* data = new char[PAGE_SIZE];
	memset(data, '0', PAGE_SIZE);
	page->flush(data);
	writePage(page->getPID(), data);
	delete[] data;
	delete page;
}

void FileHandle::flushDirectory(Directory& d) {
	char* data = new char[PAGE_SIZE];
	memset(data, '0', PAGE_SIZE);
	d.flush(data);
	writePage(d.getPID(), (void*)data);
	delete[] data;
}

void FileHandle::flushAll() {
	// Flush current page
	if (current_page != 0) {
		flushPage(current_page);
	}

	// Flush directories
	for (auto d = directories.begin(); d != directories.end(); d++) {
		flushDirectory(*d);
	}
}

void FileHandle::smartFlush(Page* page) {
	if (current_page != page) {
		flushPage(page);
	}
	// If current page is now full, flush and create new
	else {
		if (page->pageFull()) {
			// std::cout << "page full -- flushing now" << std::endl;
			flushPage(page);
			current_page = 0;
		}
	}
}


void FileHandle::printDebug() {

}

void FileHandle::printPage(int pid) {
   Page* page = getPage(pid);
   page->printDebug();
   if (current_page != page) {
   		delete page;
   }
}
