#ifndef READPAGE_H
#define READPAGE_H

#include "types.h"

// Page class optimized for reads
class ReadPage {
public:
    ReadPage(char* data);
    ~ReadPage();

    // Returns slot with given slot_id
    slot getSlot(unsigned slot_id);

    // Reading record from page
    RC readRecord(unsigned slot_id, void *data, const vector<Attribute> &recordDescriptor);

    // Returns -1 for error, 0 for success, and 1 if record is on another page
    RC checkValidSlot(unsigned slot_id);

    // Returns the RID that links to the record that has been moved to another page
    RID getLinkingRID(const RID& rid);

private:
    char* page_data;
};


#endif
