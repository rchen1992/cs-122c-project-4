#include "rm.h"

Attribute RelationManager::createAttribute(const string &name, AttrType type, unsigned length) {
    Attribute attr;
    attr.name = name;
    attr.type = type;
    attr.length = (AttrLength) length;
    return attr;
}

vector<Attribute> RelationManager::createTableAttributes() {
    vector <Attribute> attrs;
    attrs.push_back(createAttribute("table-id", TypeInt, 4));
    attrs.push_back(createAttribute("table-name", TypeVarChar, 50));
    attrs.push_back(createAttribute("file-name", TypeVarChar, 50));
    return attrs;
}

vector<Attribute> RelationManager::createColumnAttributes() {
    vector <Attribute> attrs;
    attrs.push_back(createAttribute("table-id", TypeInt, 4));
    attrs.push_back(createAttribute("column-name", TypeVarChar, 50));
    attrs.push_back(createAttribute("column-type", TypeInt, 4));
    attrs.push_back(createAttribute("column-length", TypeInt, 4));
    attrs.push_back(createAttribute("column-position", TypeInt, 4));
    return attrs;
}

bool RelationManager::isSystemTable(const string &tableName) {
    return (tableName == TABLE_SYSTEMTABLE || tableName == COLUMN_SYSTEMTABLE);
}

string RelationManager::getFileNameForTable(const string &tableName) {
    if (isSystemTable(tableName)) return tableName + CATALOG_FILE_EXT;
    return tableName + TABLE_FILE_EXT;
}

string RelationManager::getIndexName(const string &tableName, const string &attributeName) {
    return "idx_" + tableName + "_" + attributeName;
}

string RelationManager::getFileNameForIndex(const string &tableName, const string &attributeName) {
    return getIndexName(tableName, attributeName) + INDEX_FILE_EXT;
}

bool RelationManager::catalogsExist() {
    return fileExists(TABLE_CATALOGFILE) || fileExists(COLUMN_CATALOGFILE);
}

RC RelationManager::removeCachedTable(const string &tableName) {
    tableNameAndId.erase(tableName);
    tableNameAndAttrs.erase(tableName);
};

RC RelationManager::insertCachedTableId(const string &tableName, int tableId) {
    tableNameAndId.insert(pair<string, int>(tableName, tableId));
    return SUCCESS;
};

RC RelationManager::insertCachedAttributes(const string &tableName, const vector<Attribute>& attrs) {
    tableNameAndAttrs.insert(pair<string, vector<Attribute>>(tableName, attrs));
    return SUCCESS;
};

int RelationManager::getCachedTableId(const string &tableName) {
    return tableNameAndId.find(tableName)->second;
};

vector<Attribute> RelationManager::getCachedAttributes(const string &tableName) {
    return tableNameAndAttrs.find(tableName)->second;
}

// This will be big enough to hold data that conforms to attributes
int RelationManager::getDataSize(const vector <Attribute> &attrs) {
    int numFields = attrs.size();
    int nullHeaders = ceil((float) numFields / 8);
    int size = nullHeaders;
    for (int i = 0; i < numFields; i++) {
        Attribute attr = attrs[i];
        if (attr.type == TypeVarChar) size += sizeof(int);
        size += attr.length;
    }
    return size;
}

// Return data to be inserted into Table system catalog
void *RelationManager::buildTableTuple(const vector <Attribute> &attrs, int tableId, const string &tableName,
                                       const string &fileName) {
    // Tables (table-id:int, table-name:varchar(50), file-name:varchar(50))
    void *data = (void *) calloc(getDataSize(attrs), sizeof(byte));

    int numNullHeaders = ceil((float) attrs.size() / 8);
    int offset = 0;

    // Set null headers
    memset(((byte *) data + offset), 0, numNullHeaders);
    offset = numNullHeaders;

    // Set INT table-id
    memcpy(((byte *) data + offset), &tableId, sizeof(tableId));
    offset += sizeof(tableId);

    // Set VarChar table-name Length
    int tableNameLen = tableName.length();
    memcpy(((byte *) data + offset), &tableNameLen, sizeof(tableNameLen));
    offset += sizeof(tableNameLen);

    // Set VarChar table-name
    memcpy(((byte *) data + offset), tableName.c_str(), tableNameLen);
    offset += tableNameLen;

    // Set VarChar file-name Length
    int fileNameLen = fileName.length();
    memcpy(((byte *) data + offset), &fileNameLen, sizeof(fileNameLen));
    offset += sizeof(fileNameLen);

    // Set VarChar file-name
    memcpy(((byte *) data + offset), fileName.c_str(), fileNameLen);
    offset += fileNameLen;

//    printTuple(attrs, data);
    return data;
}

// Return data to be inserted into Column system catalog
void *RelationManager::buildColumnTuple(const vector <Attribute> &attrs, int tableId, const string &columnName,
                                        int columnType, int columnLength, int columnPosition) {
//  Columns(table-id:int, column-name:varchar(50), column-type:int, column-length:int, column-position:int)
    void *data = (void *) calloc(getDataSize(attrs), sizeof(byte));

    int numNullHeaders = ceil((float) attrs.size() / 8);
    int offset = 0;

    // Set null headers
    memset(((byte *) data + offset), 0, numNullHeaders);
    offset = numNullHeaders;

    // Set INT table-id
    memcpy(((byte *) data + offset), &tableId, sizeof(tableId));
    offset += sizeof(tableId);

    // Set VarChar column-name Length
    int columnNameLen = columnName.length();
    memcpy(((byte *) data + offset), &columnNameLen, sizeof(columnNameLen));
    offset += sizeof(columnNameLen);

    // Set VarChar column-name
    memcpy(((byte *) data + offset), columnName.c_str(), columnNameLen);
    offset += columnNameLen;

    // Set INT column-type
    memcpy(((byte *) data + offset), &columnType, sizeof(columnType));
    offset += sizeof(columnType);

    // Set INT column-length
    memcpy(((byte *) data + offset), &columnLength, sizeof(columnLength));
    offset += sizeof(columnLength);

    // Set INT column-position
    memcpy(((byte *) data + offset), &columnPosition, sizeof(columnPosition));
    offset += sizeof(columnPosition);

//    printTuple(attrs, data);
    return data;
}

// Have caller free the return.
void* RelationManager::getFieldValue(const string &attributeName, const vector<Attribute> &attrs, const void *data) {
    // Given tuple data and attributes, extract value (project) attributeName
    int offset = div_ceil(attrs.size(), 8); // number of null bytes is offset

    for (unsigned i = 0; i < attrs.size(); i++) {
        Attribute attr = attrs[i];

        // Go to next if attribute is NULL
        int nullbit = getBit(((char*)data)[i / 8], i % 8);
        if (nullbit == 1) {
            if (attr.name == attributeName) return NULL;
            continue;
        }

        if (attr.type == TypeInt) {
            int n;
            memcpy(&n, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);

//            cout << attr.name << " : " << n << "    ";
            if (attr.name == attributeName) {
                int* nx = new int;
                *nx = n;
                return (void*)nx;
            }

        } else if (attr.type == TypeReal) {
            float f;
            memcpy(&f, (byte *) data + offset, sizeof(float));
            offset += sizeof(float);

//            cout << attr.name << " : " << f << "    ";
            if (attr.name == attributeName) {
                float* fx = new float;
                *fx = f;
                return (void*)fx;
            }

        } else if (attr.type == TypeVarChar) {
            int nameLen;
            memcpy(&nameLen, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            byte *buffer = (byte *) calloc(nameLen + 1, sizeof(byte));
            memcpy(buffer, (byte *) data + offset, nameLen);
            offset += nameLen;

//            cout << attr.name << " : " << buffer << "    ";
            if (attr.name == attributeName) {
                byte* wBuffer = (byte*) calloc(4 + nameLen + 1, sizeof(byte));
                memcpy(wBuffer, &nameLen, sizeof(int));
                memcpy(wBuffer + 4, buffer, nameLen);
                return wBuffer; // This varcharLen and varchar recombined to use as key.
            }
            free(buffer);
            buffer = NULL;
        }
    }
    return NULL;
}

// Read tuple data from Table Catalog table
TableTuple RelationManager::readTableTuple(void *data) {
    TableTuple tuple;
    int offset = 1; // skip the one null byte. no values here should be null.
    for (unsigned i = 0; i < tableAttrs.size(); i++) {
        Attribute attr = tableAttrs[i];
        if (attr.type == TypeInt) {
            int n;
            memcpy(&n, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            if (attr.name == "table-id") tuple.tableId = n;
        } else if (attr.type == TypeReal) {
            float f;
            memcpy(&f, (byte *) data + offset, sizeof(float));
            offset += sizeof(float);
        } else if (attr.type == TypeVarChar) {
            int nameLen;
            memcpy(&nameLen, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            byte *buffer = (byte *) calloc(nameLen + 1, sizeof(byte));
            memcpy(buffer, (byte *) data + offset, nameLen);
            offset += nameLen;
            if (attr.name == "table-name") tuple.tableName = buffer;
            if (attr.name == "file-name") tuple.fileName = buffer;
            free(buffer);
            buffer = NULL;
        }
    }
    return tuple;
}

// Column Catalog contains attributes
Attribute RelationManager::readColumnTuple(void *data) {
    Attribute result;
    int offset = 1; // skip the one null byte. no values here should be null.
    for (unsigned i = 0; i < columnAttrs.size(); i++) {
        Attribute attr = columnAttrs[i];
        if (attr.type == TypeInt) {
            int n;
            memcpy(&n, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            if (attr.name == "column-type") result.type = (AttrType) n;
            if (attr.name == "column-length") result.length = (AttrLength) n;
        } else if (attr.type == TypeReal) {
            float f;
            memcpy(&f, (byte *) data + offset, sizeof(float));
            offset += sizeof(float);
        } else if (attr.type == TypeVarChar) {
            int nameLen;
            memcpy(&nameLen, (byte *) data + offset, sizeof(int));
            offset += sizeof(int);
            byte *buffer = (byte *) calloc(nameLen + 1, sizeof(byte));
            memcpy(buffer, (byte *) data + offset, nameLen);
            offset += nameLen;
            if (attr.name == "column-name") result.name = buffer;
            free(buffer);
            buffer = NULL;
        }
    }
    return result;
}

int RelationManager::getTableIdFromTableName(const string &tableName) {
    int tableId = -1;

    if (tableNameAndId.count(tableName) != 0)
        tableId = getCachedTableId(tableName);  // Read from Cache when available.
    else
        tableId = getTableByTableName(tableName).tableId;

    if (tableId < 1) {
        tableId = -1;
        cout << "[getTableIdFromTableName() Returned Invalid Value]" << endl;
    }
    return tableId;
}

TableTuple RelationManager::getTableByTableName(const string& tableName) {
    TableTuple tuple;
    RM_ScanIterator rmsi;
    RID rid;

    int len = tableName.length();
    void *value = (byte *) calloc(len, sizeof(byte));
    memcpy(value, tableName.c_str(), len);

    this->scan(TABLE_SYSTEMTABLE, "table-name", EQ_OP, value, tableAttrNames, rmsi);
    void *data = calloc(getDataSize(tableAttrs), sizeof(byte));
    if (rmsi.getNextTuple(rid, data) != RM_EOF) {
        tuple = readTableTuple(data);
    }
    rmsi.close();
    free(data);
    free(value);
    return tuple;
}

RC RelationManager::printTableCatalog() {
    cout << "=== BEGIN printTableCatalog() === " << endl;
    RM_ScanIterator rmsi;
    RID rid;
    this->scan(TABLE_SYSTEMTABLE, "", NO_OP, NULL, tableAttrNames, rmsi);
    void *data = calloc(getDataSize(tableAttrs), sizeof(byte));
    while (rmsi.getNextTuple(rid, data) != RM_EOF) {
//        cout << "RID: pageNum = " << rid.pageNum << "  slotNum = " << rid.slotNum << endl;
        printTuple(tableAttrs, data);
    }
    rmsi.close();
    free(data);
    cout << "=== END printTableCatalog() === " << endl;
    return 0;
}

RC RelationManager::printColumnCatalog() {
    cout << "=== BEGIN printColumnCatalog() === " << endl;
    RM_ScanIterator rmsi;
    RID rid;
    this->scan(COLUMN_SYSTEMTABLE, "", NO_OP, NULL, columnAttrNames, rmsi);
    void *data = calloc(getDataSize(columnAttrs), sizeof(byte));
    while (rmsi.getNextTuple(rid, data) != RM_EOF) {
//        cout << "RID: pageNum = " << rid.pageNum << "  slotNum = " << rid.slotNum << endl;
        printTuple(columnAttrs, data);
    }
    rmsi.close();
    free(data);
    cout << "=== END printColumnCatalog() === " << endl;
    return 0;
}

Attribute RelationManager::getAttributeByAttributeName(const string& tableName, const string& attributeName) {
    Attribute result;
    vector<Attribute> attrs;

    rc = getAttributes(tableName, attrs);
    for (auto attr : attrs) {
        if (attr.name == attributeName) {
            result = attr;
            break;
        }
    }
    return result;
}
