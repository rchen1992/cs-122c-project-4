#include "rm.h"

RelationManager *RelationManager::_rm = 0;

RelationManager *RelationManager::instance() {
    if (!_rm) _rm = new RelationManager();
    return _rm;
}

RelationManager::RelationManager()
    :   rc(0),
        tableIdCounter(1),

        TABLE_FILE_EXT(".table"),
        CATALOG_FILE_EXT(".catalog"),
        INDEX_FILE_EXT(".index"),

        TABLE_SYSTEMTABLE("Tables"), // test cases want these proper cased
        COLUMN_SYSTEMTABLE("Columns"), // test cases want these proper cased
        INDEX_SYSTEMTABLE("Indexes"),

        TABLE_CATALOGFILE(TABLE_SYSTEMTABLE + CATALOG_FILE_EXT),
        COLUMN_CATALOGFILE(COLUMN_SYSTEMTABLE + CATALOG_FILE_EXT),
        INDEX_CATALOGFILE(INDEX_SYSTEMTABLE + CATALOG_FILE_EXT),

        SYSTEM(0),
        USER(1)
{
    rbfm = RecordBasedFileManager::instance();
//    ixm = IndexManager::instance();

    tableAttrs = createTableAttributes();
    columnAttrs = createColumnAttributes();

    for (unsigned i = 0; i < tableAttrs.size(); i++)
        tableAttrNames.push_back(tableAttrs[i].name);

    for (unsigned i = 0; i < columnAttrs.size(); i++)
        columnAttrNames.push_back(columnAttrs[i].name);

    if (catalogsExist()) rc = readCatalog();
    // don't call create catalog, test cases do that (?)
}

RelationManager::~RelationManager() { }

RC RelationManager::createCatalog() {
//    cout << "[ CREATE CATALOG CALLED ]" << endl;
    if (catalogsExist()) {
        cout << "Create catalog called, catalogs already exist. returning" << endl;
        return -1;
    } else {
//        cout << "Creating the catalogs" << endl;
    }
    RID rid;
    void *data = NULL;
    FileHandle tableHandle, columnHandle;

    // Need these both created before writing
    rc = rbfm->createFile(TABLE_CATALOGFILE);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->createFile(COLUMN_CATALOGFILE);
    if (rc == FAILURE) return FAILURE;

    rc = rbfm->openFile(TABLE_CATALOGFILE, tableHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(COLUMN_CATALOGFILE, columnHandle);
    if (rc == FAILURE) return FAILURE;

    // BEGIN CREATE TABLE CATALOG
    int tableId = tableIdCounter;
    string tableName = TABLE_SYSTEMTABLE;
    string fileName = TABLE_CATALOGFILE;
    data = buildTableTuple(tableAttrs, tableId, tableName, fileName);
    rc = rbfm->insertRecord(tableHandle, tableAttrs, data, rid);
    free(data);
    if (rc == FAILURE) return FAILURE;

    // ADD ATTRIBUTES TO COLUMN CATALOG
    for (unsigned i = 0; i < tableAttrs.size(); i++) {
        int tableId = tableIdCounter;
        string columnName = tableAttrs[i].name;
        int columnType = tableAttrs[i].type;
        int columnLength = tableAttrs[i].length;
        int columnPosition = i + 1;
        data = buildColumnTuple(columnAttrs, tableId, columnName, columnType, columnLength, columnPosition);
        rc = rbfm->insertRecord(columnHandle, columnAttrs, data, rid);
        free(data);
        if (rc == FAILURE) return FAILURE;
    }
    //Cache the Table.catalog
    insertCachedTableId(TABLE_SYSTEMTABLE, tableIdCounter);
    insertCachedAttributes(TABLE_SYSTEMTABLE, tableAttrs);
    this->tableIdCounter++;
    // FINISHED CREATE TABLE CATALOG

    // BEGIN CREATE COLUMN CATALOG
    tableId = tableIdCounter;
    tableName = COLUMN_SYSTEMTABLE;
    fileName = COLUMN_CATALOGFILE;
    data = buildTableTuple(tableAttrs, tableId, tableName, fileName);
    rc = rbfm->insertRecord(tableHandle, tableAttrs, data, rid);
    free(data);
    if (rc == FAILURE) return FAILURE;

    // ADD ATTRIBUTES TO COLUMN CATALOG
    for (unsigned i = 0; i < columnAttrs.size(); i++) {
        int tableId = tableIdCounter;
        string columnName = columnAttrs[i].name;
        int columnType = columnAttrs[i].type;
        int columnLength = columnAttrs[i].length;
        int columnPosition = i + 1;
        data = buildColumnTuple(columnAttrs, tableId, columnName, columnType, columnLength, columnPosition);
        rc = rbfm->insertRecord(columnHandle, columnAttrs, data, rid);
        free(data);
        if (rc == FAILURE) return FAILURE;
    }

    // Cache the Column.catalog
    insertCachedTableId(COLUMN_SYSTEMTABLE, tableIdCounter);
    insertCachedAttributes(COLUMN_SYSTEMTABLE, columnAttrs);
    this->tableIdCounter++;
    // FINISHED CREATE COLUMN CATALOG

    rbfm->closeFile(tableHandle);
    rbfm->closeFile(columnHandle);
    return 0;
}

RC RelationManager::readCatalog() {
    if (!catalogsExist()) return -1;
    RM_ScanIterator rmsi;
    RID rid;
    void *data = calloc(getDataSize(tableAttrs), sizeof(byte));
    rc = this->scan(TABLE_SYSTEMTABLE, "", NO_OP, NULL, tableAttrNames, rmsi);
    if (rc == FAILURE) return FAILURE;

    int largestTableId = 0;
    while (rmsi.getNextTuple(rid, data) != RM_EOF) {
        TableTuple t = readTableTuple(data);
        if (t.tableId > largestTableId) largestTableId = t.tableId;

        // Cache <tableName, tableId>
        insertCachedTableId(t.tableName, t.tableId);

        // Cache <tableName, theTable'sAttributes>
        vector<Attribute> attrs;
        getAttributes(t.tableName, attrs);
        insertCachedAttributes(t.tableName, attrs);
    }
    rmsi.close();
    free(data);
    // tableIdCounter is the tableId that a newly created Table should take
    // (then tableIdCounter value is incremented by 1 for next Table to be created to use).
    this->tableIdCounter = largestTableId + 1;
    return 0;
}

RC RelationManager::deleteCatalog() {
    if (!catalogsExist()) return -1;
    rc = rbfm->destroyFile(TABLE_CATALOGFILE);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->destroyFile(COLUMN_CATALOGFILE);
    if (rc == FAILURE) return FAILURE;
    return 0;
}

RC RelationManager::createTable(const string &tableName, const vector <Attribute> &attrs) {
//    cout << "createTable called for: " << tableName << endl;
    if (isSystemTable(tableName)) {
        cout << "Table name '" + tableName + "' is reserved for system catalog." << endl;
        cout << "[createTable] User may not modify System Catalogs. Cannot create table: " << tableName << endl;
        return FAILURE;
    }
    return this->createTable(tableName, attrs, USER);
}

RC RelationManager::createTable(const string &tableName, const vector <Attribute> &attrs, int privilege) {
    if (fileExists(tableName)) {
        cout << "[createTable] Table named: " << tableName << " already exists. Cannot create table." << endl;
        return -1;
    }
    rc = rbfm->createFile(getFileNameForTable(tableName));
    // UPDATE TABLE SYSTEM CATALOG
    RID rid;
    void *data = buildTableTuple(tableAttrs, tableIdCounter, tableName, getFileNameForTable(tableName));
    rc = this->insertTuple(TABLE_SYSTEMTABLE, data, rid, SYSTEM);
    free(data);
    if (rc == FAILURE) return FAILURE;

    // UPDATE COLUMN SYSTEM CATALOG
    for (unsigned i = 0; i < attrs.size(); i++) {
        int tableId = tableIdCounter;
        string columnName = attrs[i].name;
        int columnType = attrs[i].type;
        int columnLength = attrs[i].length;
        int columnPosition = i + 1;

        data = buildColumnTuple(columnAttrs, tableId, columnName, columnType, columnLength, columnPosition);
        rc = this->insertTuple(COLUMN_SYSTEMTABLE, data, rid, SYSTEM);
        free(data);
        if (rc == FAILURE) return FAILURE;
    }
//    cout << "CREATED TABLE: " << tableName << " with table-id = " << tableIdCounter << endl;
    insertCachedTableId(tableName, tableIdCounter);
    insertCachedAttributes(tableName, attrs);
    this->tableIdCounter++;
    return 0;
}

RC RelationManager::deleteTable(const string &tableName) {
//    cout << "DELETE TABLE CALLED ON: " << tableName << endl;
    if (isSystemTable(tableName)) {
        cout << "[deleteTable] User may not modify System Catalogs. Cannot delete table: " << tableName << endl;
        return FAILURE;
    }
    return this->deleteTable(tableName, USER);
}

RC RelationManager::deleteTable(const string &tableName, int privilege) {
    // Scan TABLE CATALOG to find table-id of tableName.
    int tableId = -1;
    RID tableRID, rid;
    vector<RID> columnRIDs;
    RM_ScanIterator rmsi;

    int len = tableName.length();
    void *value = (byte *) calloc(len, sizeof(byte));
    memcpy(value, tableName.c_str(), len);
    void *data = calloc(getDataSize(tableAttrs), sizeof(byte));

    rc = this->scan(TABLE_SYSTEMTABLE, "table-name", EQ_OP, value, tableAttrNames, rmsi);
    if (rc == FAILURE) return FAILURE;
    if (rmsi.getNextTuple(rid, data) != RM_EOF) {
        // Need to call deleteTuple outside of Scan since scan opens the file to be deleted from.
        tableRID = rid;
        tableId = readTableTuple(data).tableId;
    }
    rmsi.close();
    free(data);
    free(value);
    if (rc == FAILURE) return FAILURE;

    if (tableId == -1) {
        cout << endl << "Table: " << tableName << " NOT FOUND" << endl << endl;
        printTableCatalog();
        return -1;
    }

    rc = this->deleteTuple(TABLE_SYSTEMTABLE, tableRID, SYSTEM); // delete entry from Table catalog
    if (rc == FAILURE) return FAILURE;

    // With table-id Scan COLUMN CATALOG to find entries to run deleteTuple() on
    // deleteTuple of TABLE CATALOG entry found (save RID)
    data = calloc(getDataSize(columnAttrs), sizeof(byte));
    rc = this->scan(COLUMN_SYSTEMTABLE, "table-id", EQ_OP, &tableId, columnAttrNames, rmsi);
    while (rmsi.getNextTuple(rid, data) != RM_EOF) {
        columnRIDs.push_back(rid);
    }
    rmsi.close();
    free(data);
    if (rc == FAILURE) return FAILURE;

    // Delete entries from column catalog
    for (unsigned i = 0; i < columnRIDs.size(); i++) {
        rc = this->deleteTuple(COLUMN_SYSTEMTABLE, columnRIDs[i], SYSTEM);
        if (rc == FAILURE) return FAILURE;
    }
    rc = removeCachedTable(tableName);
    rc = rbfm->destroyFile(getFileNameForTable(tableName)); // Delete table data (the table file)
    return rc;
}

RC RelationManager::getAttributes(const string &tableName, vector <Attribute> &attrs) {
    if (tableNameAndAttrs.count(tableName) != 0) {
        attrs = getCachedAttributes(tableName); // Read from Cache when available.
        return SUCCESS;
    }

    int tableId = getTableIdFromTableName(tableName);
    // Scan Columns to get all records with tableName's table-id
    RID rid;
    RM_ScanIterator rmsi;
    void *data = calloc(getDataSize(columnAttrs), sizeof(byte));
    rc = this->scan(COLUMN_SYSTEMTABLE, "table-id", EQ_OP, &tableId, columnAttrNames, rmsi);
    while (rmsi.getNextTuple(rid, data) != RM_EOF) {
        attrs.push_back(readColumnTuple(data));
    }
    rmsi.close();
    free(data);
    return SUCCESS;
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid) {
    if (isSystemTable(tableName)) {
        cout << "[insertTuple] User may not modify System Catalogs. Cannot insert into: " << tableName << endl;
        return FAILURE;
    }
    return this->insertTuple(tableName, data, rid, USER);
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid, int privilege) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    vector<Attribute> attrs;
    rc = getAttributes(tableName, attrs);

    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(fileName, fileHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->insertRecord(fileHandle, attrs, data, rid);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->closeFile(fileHandle);
    if (rc == FAILURE) return FAILURE;

//    cout << "Insert Tuple: Index Insert" << endl;
    for (auto a : attrs) {
        // Given entire tuple
        string ixFileName = getFileNameForIndex(tableName, a.name);

        if (fileExists(ixFileName)) {
//            cout << "The file (index) exists: " << ixFileName << endl;
            // The Index exists for this <Table, Attribute>
            // So, insert it into the index

            Attribute attr = getAttributeByAttributeName(tableName, a.name);
            void* key = getFieldValue(a.name, attrs, data); // Get key data for the column
            if (key == NULL) { free(key); break; }

            IXFileHandle ixFileHandle;
            rc = ixm->openFile(ixFileName, ixFileHandle);
            if (rc == FAILURE) return rc;
            rc = ixm->insertEntry(ixFileHandle, attr, key, rid);
            if (rc == FAILURE) return rc;
            rc = ixm->closeFile(ixFileHandle);
            if (rc == FAILURE) return rc;

            free(key);
        }
    }
    return rc;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid) {
    if (isSystemTable(tableName)) {
        cout << "[deleteTuple] User may not modify System Catalogs. Cannot delete from: " << tableName << endl;
        return FAILURE;
    }
    return this->deleteTuple(tableName, rid, USER);
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid, int privilege) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    vector <Attribute> attrs;

    if (privilege == SYSTEM) {
        if (tableName == TABLE_SYSTEMTABLE)
            attrs = tableAttrs;
        else if (tableName == COLUMN_SYSTEMTABLE) {
            attrs = columnAttrs;
        }
        rc = SUCCESS;
    } else {
        rc = getAttributes(tableName, attrs);
    }

    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(fileName, fileHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->deleteRecord(fileHandle, attrs, rid);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->closeFile(fileHandle);
    if (rc == FAILURE) return FAILURE;

    // Delete Tuple from Index
    // For delete, For delete... delete tuple, and delete from index.
    for (auto a : attrs) {
        string ixFileName = getFileNameForIndex(tableName, a.name);

        if (fileExists(ixFileName)) {
            cout << "The file (index) exists: " << ixFileName << endl;
            // The Index exists for this <Table, Attribute>
            // So, insert it into the index
            Attribute attr = getAttributeByAttributeName(tableName, a.name);

            byte data[PAGE_SIZE];
            readTuple(tableName, rid, data);
            void* key = getFieldValue(a.name, attrs, data);
            if (key == NULL) { free(key); break; }

            IXFileHandle ixFileHandle;
            rc = ixm->openFile(ixFileName, ixFileHandle);
            if (rc == FAILURE) return rc;
            rc = ixm->deleteEntry(ixFileHandle, attr, key, rid);
            if (rc == FAILURE) return rc;
            rc = ixm->closeFile(ixFileHandle);
            if (rc == FAILURE) return rc;

            free(key);
        }
    }
    return rc;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid) {
    if (isSystemTable(tableName)) {
        cout << "[updateTuple] User may not modify System Catalogs. Cannot update: " << tableName << endl;
        return FAILURE;
    }
    return this->updateTuple(tableName, data, rid, USER);
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid, int privilege) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    vector <Attribute> attrs;
    rc = getAttributes(tableName, attrs);

    // On Tuple update
    // Delete <Key, RID> from Index
    // Update Record
    // Insert <Key, RID> into Index

    // Delete <Key, RID> from Index
    for (auto a : attrs) {
        string ixFileName = getFileNameForIndex(tableName, a.name);

        if (fileExists(ixFileName)) {
            Attribute attr = getAttributeByAttributeName(tableName, a.name);

            byte eData[PAGE_SIZE];
            readTuple(tableName, rid, eData);
            void* key = getFieldValue(a.name, attrs, eData);
            if (key == NULL) { free(key); break; }

            IXFileHandle ixFileHandle;
            rc = ixm->openFile(ixFileName, ixFileHandle);
            if (rc == FAILURE) return rc;
            rc = ixm->deleteEntry(ixFileHandle, attr, key, rid);
            if (rc == FAILURE) return rc;
            rc = ixm->closeFile(ixFileHandle);
            if (rc == FAILURE) return rc;

            free(key);
        }
    }

    // Update Record
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(fileName, fileHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->updateRecord(fileHandle, attrs, data, rid);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->closeFile(fileHandle);
    if (rc == FAILURE) return FAILURE;

    // Insert <Key, RID> into Index
    for (auto a : attrs) {
        string ixFileName = getFileNameForIndex(tableName, a.name);

        if (fileExists(ixFileName)) {
            Attribute attr = getAttributeByAttributeName(tableName, a.name);
            void* key = getFieldValue(a.name, attrs, data); // Get key data for the column
            if (key == NULL) { free(key); break; }

            IXFileHandle ixFileHandle;
            rc = ixm->openFile(ixFileName, ixFileHandle);
            if (rc == FAILURE) return rc;
            rc = ixm->insertEntry(ixFileHandle, attr, key, rid);
            if (rc == FAILURE) return rc;
            rc = ixm->closeFile(ixFileHandle);
            if (rc == FAILURE) return rc;

            free(key);
        }
    }
    return rc;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    vector <Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(fileName, fileHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->readRecord(fileHandle, attrs, rid, data);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->closeFile(fileHandle);
    if (rc == FAILURE) return FAILURE;
    return rc;
}

RC RelationManager::printTuple(const vector <Attribute> &attrs, const void *data) {
    return rbfm->printRecord(attrs, data);
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    vector <Attribute> attrs;
    rc = getAttributes(tableName, attrs);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->openFile(fileName, fileHandle);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->readAttribute(fileHandle, attrs, rid, attributeName, data);
    if (rc == FAILURE) return FAILURE;
    rc = rbfm->closeFile(fileHandle);
    if (rc == FAILURE) return FAILURE;
    return rc;
}

RC RelationManager::scan(const string &tableName,
                         const string &conditionAttribute,
                         const CompOp compOp,
                         const void *value,
                         const vector<string> &attributeNames,
                         RM_ScanIterator &rm_ScanIterator) {
    FileHandle fileHandle;
    string fileName = getFileNameForTable(tableName);
    rc = rbfm->openFile(fileName, fileHandle);

    if (isSystemTable(tableName)) {
        if (tableName == TABLE_SYSTEMTABLE) {
            return rm_ScanIterator.init(fileHandle, tableAttrs, conditionAttribute, compOp, value, attributeNames);
        } else if (tableName == COLUMN_SYSTEMTABLE) {
            return rm_ScanIterator.init(fileHandle, columnAttrs, conditionAttribute, compOp, value, attributeNames);
        }
    } else {
        vector <Attribute> attrs;
        rc = getAttributes(tableName, attrs);
        return rm_ScanIterator.init(fileHandle, attrs, conditionAttribute, compOp, value, attributeNames);
    }
    return -1;
}

// Extra credit work
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr) {
    return -1;
}

// Extra credit work
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName) {
    return -1;
}


int BUGGER = 0;

RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
//    cout << "Create Index - Table: " << tableName << "\t Attribute: " << attributeName << endl;
    string ixFileName = getFileNameForIndex(tableName, attributeName);
//    cout << "Index filename: " << ixFileName << endl;

    if (fileExists(ixFileName)) {
        cout << "The file: " << ixFileName << " already exists." << endl;
        return FAILURE;
    }

    rc = ixm->createFile(ixFileName);
    if (rc == FAILURE) return rc;

    IXFileHandle ixFileHandle;
    rc = ixm->openFile(ixFileName, ixFileHandle);
    if (rc == FAILURE) return rc;

    // When creating an index on a table that already has content.
    // Scan the attributes in the table.
    // Add the things into the Index
    RID rid;
    byte data[PAGE_SIZE];

//    cout << "Scanning for items that already exist. " << endl;
    RM_ScanIterator rmsi;

    if (BUGGER == 0) {
        BUGGER++;
        // Scan Crash/SEGFAULT on empty Table
    } else {
//        cout << "NOT BUGGER" << endl;
        vector<Attribute> attrs;
        getAttributes(tableName, attrs);

        vector<string> cols;
        for (auto a : attrs) cols.push_back(a.name);
        rc = scan(tableName, "", NO_OP, NULL, cols, rmsi);
        if (rc == FAILURE) return rc;

        while (rmsi.getNextTuple(rid, data) != RM_EOF) {
//            printTuple(attrs, data);
            void* key = getFieldValue(attributeName, attrs, data);
            if (key == NULL) continue;
            Attribute attr = getAttributeByAttributeName(tableName, attributeName);

//            cout << endl;
//            cout << "AttrName: " << attr.name << endl;
//            cout << "AttrLen: " << attr.length << endl;
//            cout << "AttrType: " << attr.type << endl;

//            if (attr.type == TypeInt) {
//                cout << "int " << *(int*)key << endl;
//            } else if (attr.type == TypeReal) {
//                cout << "float " << *(float*)key << endl;
//            } else if (attr.type == TypeVarChar) {
//                cout << "varchar     len: " << *(int*)key << endl;
//                cout << "\t\tchars: " << (char*)key + 4 << endl;
//            }
            rc = ixm->insertEntry(ixFileHandle, attr, key, rid);
            if (rc == FAILURE) return rc;
            free(key);
        }
        rmsi.close();
    }
    rc = ixm->closeFile(ixFileHandle);
//    cout << ixFileName << " closed." << endl;

//    ADD INDEX TO TABLE CATALOG
//    printTableCatalog();
    RID tRid;
    void *tData = buildTableTuple(tableAttrs, tableIdCounter, getIndexName(tableName, attributeName), getFileNameForIndex(tableName, attributeName));
    rc = this->insertTuple(TABLE_SYSTEMTABLE, tData, tRid, SYSTEM);
    free(tData);
    if (rc == FAILURE) return FAILURE;
    insertCachedTableId(tableName, tableIdCounter);
    this->tableIdCounter++;
    return 0;
}

RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
    string ixTableName = getIndexName(tableName, attributeName);
    // Delete entries from Table catalog with ixTableName

    int tableId = -1;
    RID tableRID, rid;
    RM_ScanIterator rmsi;

    int len = ixTableName.length();
    void *value = (byte*) calloc(len, sizeof(byte));
    memcpy(value, ixTableName.c_str(), len);
    void *data = calloc(getDataSize(tableAttrs), sizeof(byte));

    rc = this->scan(TABLE_SYSTEMTABLE, "table-name", EQ_OP, value, tableAttrNames, rmsi);
    if (rc == FAILURE) return rc;
    if (rmsi.getNextTuple(rid, data) != RM_EOF) {
        tableRID = rid;
        tableId = readTableTuple(data).tableId;
    }
    rmsi.close();
    free(data);
    free(value);
    if (rc == FAILURE) return rc;

    if (tableId == -1) {
        cout << "Table Index: " << ixTableName << " NOT FOUND" << endl << endl;
        printTableCatalog();
        return FAILURE;
    }
    // Delete the Table Index entry from TABLE CATALOG
    rc = this->deleteTuple(TABLE_SYSTEMTABLE, tableRID, SYSTEM);
    if (rc == FAILURE) return rc;

    rc = removeCachedTable(ixTableName);
    if (rc == FAILURE) return rc;

    rc = ixm->destroyFile(getFileNameForIndex(tableName, attributeName));
    return rc;
}

RC RelationManager::indexScan(const string &tableName,
                              const string &attributeName,
                              const void *lowKey,
                              const void *highKey,
                              bool lowKeyInclusive, bool highKeyInclusive,
                              RM_IndexScanIterator &rm_IndexScanIterator)
{
    IXFileHandle ixFileHandle;
    string ixFileName = getFileNameForIndex(tableName, attributeName);

    rc = ixm->openFile(ixFileName, ixFileHandle);
    if (rc == FAILURE) return rc;

    Attribute attribute = getAttributeByAttributeName(tableName, attributeName);
    return rm_IndexScanIterator.init(ixFileHandle, attribute, lowKey, highKey, lowKeyInclusive, highKeyInclusive);
}
