#ifndef _rm_h_
#define _rm_h_

#include "../rbf/rbfm.h"
#include "../ix/ix.h"
#include "../rbf/types.h"

#include <string>
#include <vector>
#include <cmath>

using namespace std;

#define RM_EOF (-1)  // end of a scan operator
#define SUCCESS (0)
#define FAILURE (-1)

// RM_ScanIterator is an iteratr to go through tuples
class RM_ScanIterator {
public:
    RM_ScanIterator() { }
    ~RM_ScanIterator() { };

    RC init(
            FileHandle &fileHandle,
            const vector <Attribute> &recordDescriptor,
            const string &conditionAttribute,
            const CompOp compOp,                    // comparision type such as "<" and "="
            const void *value,                      // used in the comparison
            const vector <string> &attributeNames    // a list of projected attributes
    ) {
        rbfm = RecordBasedFileManager::instance();
        this->fileHandle = fileHandle;
        return rbfm->scan(fileHandle, recordDescriptor, conditionAttribute, compOp, value, attributeNames, rbfsi);
    }

    // "data" follows the same format as RelationManager::insertTuple()
    RC getNextTuple(RID &rid, void *data) {
        return rbfsi.getNextRecord(rid, data);
    };

    RC close() {
        rbfsi.close();
        return rbfm->closeFile(this->fileHandle);
    };
private:
    RecordBasedFileManager *rbfm;
    RBFM_ScanIterator rbfsi;
    FileHandle fileHandle;
};

/* Added for Project 4 */
class RM_IndexScanIterator {
public:
    RM_IndexScanIterator() {};  	// Constructor
    ~RM_IndexScanIterator() {}; 	// Destructor

    RC init(IXFileHandle &ixfh,
            const Attribute &attr,
            const void      *lkey,
            const void      *hkey,
            bool            lkeyInc,
            bool            hkeyInc) {
        ixm = IndexManager::instance();
        this->ixFileHandle = ixfh;
        return ixm->scan(ixfh, attr, lkey, hkey, lkeyInc, hkeyInc, ixsi);
    }

    // "key" follows the same format as in IndexManager::insertEntry()
    RC getNextEntry(RID &rid, void *key) { // Get next matching entry
        return ixsi.getNextEntry(rid, key);
    };

    RC close() { // Terminate index scan
        ixsi.close();
        return ixm->closeFile(ixFileHandle);
    };
private:
    IndexManager *ixm;
    IX_ScanIterator ixsi;
    IXFileHandle ixFileHandle;
};

// Relation Manager
class RelationManager {
public:
    static RelationManager *instance();

    RC createCatalog();
    RC deleteCatalog();

    RC createTable(const string &tableName, const vector <Attribute> &attrs);
    RC deleteTable(const string &tableName);

    RC getAttributes(const string &tableName, vector <Attribute> &attrs);

    RC readTuple(const string &tableName, const RID &rid, void *data);
    RC insertTuple(const string &tableName, const void *data, RID &rid);
    RC deleteTuple(const string &tableName, const RID &rid);
    RC updateTuple(const string &tableName, const void *data, const RID &rid);

    // mainly for debugging
    // Print a tuple that is passed to this utility method.
    RC printTuple(const vector <Attribute> &attrs, const void *data);

    // mainly for debugging
    RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

    // scan returns an iterator to allow the caller to go through the results one by one.
    RC scan(const string &tableName,
            const string &conditionAttribute,
            const CompOp compOp,                  // comparison type such as "<" and "="
            const void *value,                    // used in the comparison
            const vector <string> &attributeNames, // a list of projected attributes
            RM_ScanIterator &rm_ScanIterator);


    /* Added Index functions for Project 4 */
    RC createIndex(const string &tableName, const string &attributeName);
    RC destroyIndex(const string &tableName, const string &attributeName);

    // indexScan returns an iterator to allow the caller to go through qualified entries in index
    RC indexScan(const string &tableName,
                 const string &attributeName,
                 const void *lowKey,
                 const void *highKey,
                 bool lowKeyInclusive,
                 bool highKeyInclusive,
                 RM_IndexScanIterator &rm_IndexScanIterator);

// Extra credit work (10 points)
public:
    RC dropAttribute(const string &tableName, const string &attributeName);
    RC addAttribute(const string &tableName, const Attribute &attr);

protected:
    RelationManager();
    ~RelationManager();

    // Below are additions to base code
private:
    /* Wrappers around base functions for differentiating SYSTEM and USER requests. */
    RC deleteTable(const string &tableName, int privilege);
    RC createTable(const string &tableName, const vector <Attribute> &attrs, int privilege);
    RC insertTuple(const string &tableName, const void *data, RID &rid, int privilege);
    RC deleteTuple(const string &tableName, const RID &rid, int privilege);
    RC updateTuple(const string &tableName, const void *data, const RID &rid, int privilege);

    static RelationManager *_rm;
    RecordBasedFileManager *rbfm;
    IndexManager *ixm;
    FileHandle tableHandle, columnHandle;

    // Init these in constructor init list
    RC rc;
    int tableIdCounter; // table-id starts at 1.

    /* BEGIN ADDED CONSTANTS */
    // (Initialized in constructor initailizer list in source file)
    const string TABLE_FILE_EXT;
    const string CATALOG_FILE_EXT;
    const string INDEX_FILE_EXT;

    const string TABLE_SYSTEMTABLE;
    const string COLUMN_SYSTEMTABLE;
    const string INDEX_SYSTEMTABLE;

    const string TABLE_CATALOGFILE;
    const string COLUMN_CATALOGFILE;
    const string INDEX_CATALOGFILE;

    const int SYSTEM;
    const int USER;
    /* END ADDED CONSTANTS */


    vector<Attribute> tableAttrs, columnAttrs;
    vector<string> tableAttrNames, columnAttrNames;

    map<string, int> tableNameAndId;          // <table-name, table-id>
    map<string, vector<Attribute>> tableNameAndAttrs;   // <table-name, attributes[]>

    vector<Attribute> createTableAttributes();
    vector<Attribute> createColumnAttributes();
    Attribute createAttribute(const string &name, AttrType type, unsigned length);

    string getFileNameForTable(const string &tableName);
    string getFileNameForIndex(const string &tableName, const string &attributeName);
    string getIndexName(const string &tableName, const string &attributeName);

    void *buildTableTuple(const vector <Attribute> &attrs, int tableId, const string &tableName,
                          const string &fileName);
    void *buildColumnTuple(const vector <Attribute> &attrs, int tableId, const string &columnName, int columnType,
                           int columnLength, int columnPosition);

    RC readCatalog();
    bool catalogsExist();
    bool isSystemTable(const string &tableName);
    int getDataSize(const vector <Attribute> &attrs);
    TableTuple readTableTuple(void *data);
    Attribute readColumnTuple(void *data);
    int getTableIdFromTableName(const string &tableName);
    TableTuple getTableByTableName(const string& tableName);

    RC removeCachedTable(const string &tableName);
    RC insertCachedTableId(const string &tableName, int tableId);
    RC insertCachedAttributes(const string &tableName, const vector<Attribute>& attrs);
    int getCachedTableId(const string &tableName);
    vector<Attribute> getCachedAttributes(const string &tableName);

    RC printTableCatalog();
    RC printColumnCatalog();

    void* getFieldValue(const string& attributeName, const vector<Attribute>& attrs, const void *data);

    ///
    Attribute getAttributeByAttributeName(const string& tableName, const string& attributeName);
};

#endif




